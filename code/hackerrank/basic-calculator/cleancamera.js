/**
 * No code problems detected
 * npx eslint guzmanandrew.js
 */

/* eslint no-console: ["error", { allow: ["log"] }] */

const fileString = require('fs');

function sumOne(paramOne, paramTwo) {
  const sum = paramOne + paramTwo;
  console.log(sum.toFixed(2));
  return 0;
}

function resOne(paramOne, paramTwo) {
  const res = paramOne - paramTwo;
  console.log(res.toFixed(2));
  return 0;
}

function mulOne(paramOne, paramTwo) {
  const mul = paramOne * paramTwo;
  console.log(mul.toFixed(2));
  return 0;
}

function divOne(paramOne, paramTwo) {
  const div = paramOne / paramTwo;
  console.log(div.toFixed(2));
  return 0;
}

fileString.readFile('DATA.lst', 'utf8', (fail, inputData) => {
  const datafile = inputData;
  const dataresult = datafile.split(' ');
  const [ dataOne, dataTwo ] = dataresult;
  const dataThree = dataTwo;
  const dataNumOne = parseFloat(dataOne);
  const dataNumTwo = parseFloat(dataThree);

  sumOne(dataNumOne, dataNumTwo);

  resOne(dataNumOne, dataNumTwo);

  mulOne(dataNumOne, dataNumTwo);

  divOne(dataNumOne, dataNumTwo);

  return 0;
});

/*
$ node guzmanandrew.js
output:
6.30
-1.70
9.20
0.57
*/
