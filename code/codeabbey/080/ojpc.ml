let res pa pb =
        let cal=((float_of_int pa/.100.0)/.(1.0-.((1.0-.(float_of_int pa/.100.0))*.(1.0-.(float_of_int pb/.100.0)))))*.100.0 in
               int_of_float (floor(cal +. 0.5));;
let rec insert l i =
      match l with
        [] -> [i]
      | h :: t -> h :: (insert t i);;
let printArray arr = Array.iter (Printf.printf "%i ") arr;;
let inp=[|[|36; 21|];[|90; 77|];[|58; 47|];[|37 ;69|];[|83 ;34|];[|23 ;18|];[|21 ;57|];[|76 ;16|];[|11 ;60|];[|55 ;59|];[|68 ;59|];[|66 ;70|];[|53 ;49|];[|78 ;15|];[|16 ;38|];[|62 ;43|];[|50 ;62|];[|29 ;17|];[|18 ;57|];[|77 ;11|];[|82 ;90|];[|20 ;12|];[|56 ;87|];[|19 ;57|];[|56 ;64|];[|26 ;34|]|] in    
      let out= [| 0 |]    in        
            for i=0 to (Array.length inp) -1     do            
                    let act=inp.(i) in
                    let f=res act.(0) act.(1) in
                    out.(0) <- f;
                    printArray out
            done
