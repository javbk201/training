/*
 * $ cargo fmt   --all     -- --check
 * $ cargo build --release
 *   Finished release [optimized] target(s) in 0.01s
 */

#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use std::io::BufRead;

// struct to represent an object in a given coordinate system
struct cs {
  x: f64,
  y: f64,
  h: f64,
}

// struct to represent an object in the three possible coordinate systems
struct object {
  map: cs, // game map
  vrt: cs, // virtual screen
  scr: cs, // real screen
}

// constructors
impl cs {
  fn new_from_default() -> cs {
    cs {
      x: 0.0,
      y: 0.0,
      h: 0.0,
    }
  }
  fn new_from_points(x: f64, y: f64, h: f64) -> cs {
    cs { x: x, y: y, h: h }
  }
}

fn main() {
  // problem data
  let mut n: usize;
  let mut objects: Vec<object> = std::vec::Vec::new();

  // load problem data from "DATA.lst"
  let mut i: usize = 0;
  let file = std::fs::File::open("DATA.lst").unwrap();
  for it_line in std::io::BufReader::new(file).lines() {
    let line = &it_line.unwrap();
    let params: Vec<&str> = line.split(' ').collect();

    match i {
      0 => {
        n = params[0].parse::<usize>().unwrap();
        objects.reserve(n);
      }
      _ => {
        let x: f64 = params[0].parse::<f64>().unwrap();
        let y: f64 = params[1].parse::<f64>().unwrap();
        let h: f64 = params[2].parse::<f64>().unwrap();
        objects.push(object {
          map: cs::new_from_points(x, y, h),
          vrt: cs::new_from_default(),
          scr: cs::new_from_default(),
        });
      }
    }
    i += 1;
  }

  // transform objects from game world map to virtual screen
  let vrt_d: f64 = 0.5;
  let vrt_w: f64 = 0.4;
  let vrt_h: f64 = 0.3;
  let eye_h: f64 = 1.0;
  for mut o in &mut objects {
    let k: f64 = vrt_d / o.map.x;
    o.vrt.x = -o.map.y * k;
    o.vrt.y = -eye_h * k;
    o.vrt.h = o.map.h * k;
  }

  // transform objects from virtual screen to real screen
  let scr_w: f64 = 480.0;
  let scr_h: f64 = 360.0;
  for mut o in &mut objects {
    o.scr.x = o.vrt.x * (scr_w / vrt_w) + (scr_w / 2.0);
    o.scr.y = -o.vrt.y * (scr_h / vrt_h) + (scr_h / 2.0);
    o.scr.h = o.vrt.h * (scr_h / vrt_h);
  }

  // print results
  for o in &objects {
    print!(
      "{} {} {} ",
      o.scr.x.round(),
      o.scr.y.round(),
      (o.scr.y - o.scr.h).round(),
    );
  }

  println!("");
}

/*
 * $ cargo run --release
 *   240 199 143 390 195 156 240 300 132 307 213 147 7 192 180 118 192 175 40
 *   280 180 469 191 171 120 300 24 0 220 180 28 215 152
 */
