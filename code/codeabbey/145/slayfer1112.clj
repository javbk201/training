;; $ clj-kondo --lint slayfer1112.clj
;; linting took 14ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn modpow
  "In this case I use the InterOp with Java to simplify the exercise"
  [A B M]
  (.modPow (biginteger A) (biginteger B) (biginteger M)))

(defn solution
  "In this func I try to do all the logic for the challenge"
  [_ body]
  (doseq [x body]
    (let [vals (str/split x #" ")
          A (bigdec (read-string (vals 0)))
          B (bigdec (read-string (vals 1)))
          M (bigdec (read-string (vals 2)))
          ]
      (print (str (modpow A B M) " ")))))

(defn main []
  (let [[head body] (get-data "DATA.lst")]
    (solution head body)))

(main)

;; 198070683 109313054 277087692 12933586 198256003 236560725 294675307
;; 92481255 197854209 155631952 118366715 191058174 83470304 109786937
;; 229881269 152685176 279898540 180994460 100955905
