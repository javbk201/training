% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Values = [get_value_as_integer(X)||X<-Data],
  Max = lists:max(Values),
  Min = lists:min(Values),
  io:format("~B ~B\n", [Max,Min]).

get_value_as_integer(String) ->
  {A, _} = string:to_integer(String),
  A.

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:lexemes(erlang:binary_to_list(Binary), " ").

% $ erl -noshell -s andresclm -s init stop
% 79241 -78703
