/*$ cppcheck --enable=all --suppress=missingIncludeSystem dfuribez.c
Checking dfuribez.c ...
$ gcc dfuribez.c -o dfuribez
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned char count_set_bits(unsigned char *byte) {
    char bit_count = 0;
    for (char i=0; i<8; i++) {
        if (*byte & 1<<i) bit_count++;
    }
    return bit_count;
}

void main() {
    char line[1000];
    unsigned char byte;

    if (scanf("%1000[^\n]", line)) {
        char c[10];
        char * splitted;
        splitted = strtok(line, " ");
        while (splitted != NULL) {
            (void)snprintf(c, 6, "%s", splitted);
            byte = atoi(c);
            splitted = strtok(NULL, " ");
            unsigned char len = count_set_bits(&byte);
            if (!(len % 2)) {
                printf("%c", (byte & 127));
            }
        }
    }
}

/* $ chmod +x dfuribez && cat DATA.lst | ./dfuribez
EuhmXnJ1yGrFyWP3w00zjD tLuLdu q txPS t 3iRPosG xEc5OYvKgJGk  t.
*/
