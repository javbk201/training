#include <iostream>
#include <math.h>

#define PI 3.14159265

using namespace std;

int round_int(double val){
    int val_int;
    if(val>=0){
        val_int=(int)(val+0.5);
    }
    else{
        val_int=(int)(val-0.5);
    }
    return val_int;
}

void rotation2dspace(int num, int rot,int *X, int *Y){
    for(int i=0;i<num;i++){
        int X_aux=*(X+i), Y_aux=*(Y+i);
        *(X+i)=Y_aux*sin(-rot*PI/180)+X_aux*cos(-rot*PI/180);
        *(Y+i)=Y_aux*cos(-rot*PI/180)-X_aux*sin(-rot*PI/180);
    }
}

void sort(int num, string *names, int *X, int *Y){
    for(int i=0;i<num;i++){
        if(i==num-1){
            cout<<*(names+i);
            break;
        }
        int Y_aux=*(Y+i),X_aux=*(X+i);
        int min=Y_aux, index=i;
        string name_aux=*(names+i);
        for(int j=i+1;j<num;j++){
            if(*(Y+j)<=min){
                if(*(Y+j)==min){
                    if(*(X+j)<*(X+index)){
                        index=j;
                        continue;
                    }
                    else{
                        continue;
                    }
                }
                min=*(Y+j);
                index=j;
            }
        }
        *(X+i)=*(X+index); *(X+index)=X_aux;
        *(Y+i)=*(Y+index); *(Y+index)=Y_aux;
        *(names+i)=*(names+index); *(names+index)=name_aux; 
        cout<<*(names+i)<<' ';    
    }
}

int main(void){
    int num, rot;
    cin>>num>>rot;
    string names[num];
    int X[num], Y[num];
    for(int i=0;i<num;i++){
        cin>>names[i]>>X[i]>>Y[i];
    }
    rotation2dspace(num,rot,&X[0],&Y[0]);
    sort(num,&names[0],&X[0],&Y[0]);
    return 0;
}
