{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let numbers = map read $ words line :: [Int]
        let width = head numbers
        let height = numbers!!1
        let len = numbers!!2
        let x = -1;
        let y = -1;
        let w = 1;
        let h = 1;
        let iter i width height len x y w h = if i >= 0
            then do
                let xx = x + w
                let yy = y + h
                let www = if xx + len -1  >= width || xx < 0
                    then w * (-1)
                    else w
                let xxx = if xx + len -1  >= width || xx < 0
                    then xx + 2 * www
                    else xx
                let hhh = if yy >= height || yy < 0
                    then h * (-1)
                    else h
                let yyy = if yy >= height || yy < 0
                    then yy + 2 * hhh
                    else yy
                let res = iter (i-1) width height len xxx yyy www hhh
                show xxx ++ " " ++ show yyy ++ " " ++ res
            else ""
        let output = iter 100 width height len x y w h
        print output
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15
 16 16 17 17 18 16 19 15 20 14 21 13 22 12 23 11 24 10 25 9 26 8 27 7 28 6 29
 5 30 4 31 3 32 2 33 1 34 0 35 1 34 2 33 3 32 4 31 5 30 6 29 7 28 8 27 9 26
 10 25 11 24 12 23 13 22 14 21 15 20 16 19 17 18 16 17 15 16 14 15 13 14 12 13
 11 12 10 11 9 10 8 9 7 8 6 7 5 6 4 5 3 4 2 3 1 2 0 1 1 0 2 1 3 2 4 3 5 4 6 5 7
 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13 15 14 16 15 17 16 16 17 15 18 14 19 13
 20 12 21 11 22 10 23 9 24 8 25 7 26 6 27 5 28 4 29 3 30 2
-}
