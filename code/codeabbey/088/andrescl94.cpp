#include <iostream>
#include <math.h>
#include <string>
#include <map>

#define base_freq 440
#define A4 4

using namespace std;

map<string,int> init(void){
    map<string,int> octave;
        octave["C"]=0;      octave["F#"]=6;
        octave["C#"]=1;     octave["G"]=7;
        octave["D"]=2;      octave["G#"]=8;
        octave["D#"]=3;     octave["A"]=9;
        octave["E"]=4;      octave["A#"]=10;
        octave["F"]=5;      octave["B"]=11;
    return octave;
}

int count_notes(string note){
    map<string,int> octave=init();
    int n=((int)note[note.length()-1]-48-A4)*12;
    int note_aux=octave[note.erase(note.length()-1)];
    while(note_aux!=octave["A"]){
        if(note_aux>octave["A"]){
            n++;
            note_aux--;
        }
        else{
            n--;
            note_aux++;
        }
    }
    return n;
}

int main(void){
    int num;
    cin>>num;
    string notes[num];
    for(int i=0;i<num;i++){
        cin>>notes[i];
    }
    for(int i=0;i<num;i++){
        int n=count_notes(notes[i]);
        cout<<(int)(base_freq*pow(2,(double)n/12)+0.5)<<' ';
    }
    return 0;
}
