/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';
import 'dart:math';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    for (var i = 1; i <= c; i++) {
      var fibo =  2.078087 * log(double.parse(data[i])) + 1.672276;
      print(fibo.round());
    }
  }
  );
}

/* $ dart mrsossa.dart
 * 977 442 846 557 800 683 943 360
630 385 702 322 549 693
*/
