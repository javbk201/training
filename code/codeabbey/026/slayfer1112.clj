;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn lcm-calculator [val1 val2 gcd]
  (let [lcm (/ (* val1 val2) gcd)]
    lcm))

(defn gcd-calculator [val1 val2]
  (if (= val1 val2)
    val1
    (if (< val1 val2)
      (let [res (- val2 val1)]
        (gcd-calculator val1 res))
      (let [res (- val1 val2)]
        (gcd-calculator res val2)))))

(defn get-vals [line]
  (let [values (str/split line #" ")
        val1 (read-string (values 0))
        val2 (read-string (values 1))]
    [val1 val2]))

(defn solution [_ body]
  (doseq [x body]
    (let [[val1 val2] (get-vals x)
          gcd (gcd-calculator val1 val2)
          lcm (lcm-calculator val1 val2 gcd)]
      (print (str "(" gcd " " lcm ") ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; (1 23751) (2 47286) (1860 37200) (52 102492) (11 13728) (71 191913)
;; (2 1786) (78 323544) (28 100492) (108 19656) (52 146224) (275 22000)
;; (2 1012) (5 190) (1 602) (8 824) (62 335916) (44 11704) (4 12) (1 2577120)
