/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build680751478
*/

package main

import (
  "bufio"
  "fmt"
  "io"
  "os"
  "path/filepath"
  "strconv"
  "strings"
)

func main() {

  absPath, _ := filepath.Abs("DATA.lst")
  arr, _ := readFileToStr(absPath)
  n, _ := strconv.Atoi(arr[0])
  arr = arr[1:]
  res := ""
  for i := 0; i < n; i++ {

    moves := strings.Split(arr[i], " ")
    b := newBoard()

    err := 0
    j := 0
    for err == 0 && j < len(moves) {
      if b.makeMove(moves[j]) == 0 {
        err = (j + 1)
      }
      j++
    }

    res += (" " + strconv.Itoa(err))

  }

  fmt.Println(res)
}

var mvChrt = map[string]int{
  "a": 0,
  "b": 1,
  "c": 2,
  "d": 3,
  "e": 4,
  "f": 5,
  "g": 6,
  "h": 7,
}

type board struct {
  grid [][]rune
}

func newBoard() (b *board) {
  b = new(board)
  b.grid = [][]rune{
    {'R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'},
    {'P', 'P', 'P', 'P', 'P', 'P', 'P', 'P'},
    {'-', '-', '-', '-', '-', '-', '-', '-'},
    {'-', '-', '-', '-', '-', '-', '-', '-'},
    {'-', '-', '-', '-', '-', '-', '-', '-'},
    {'-', '-', '-', '-', '-', '-', '-', '-'},
    {'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'},
    {'r', 'n', 'b', 'q', 'k', 'b', 'n', 'r'},
  }
  return b
}

func (b board) printBoard() {
  fmt.Printf("---------------------------------\n")
  for i := 7; i >= 0; i-- {
    fmt.Printf("|")
    for j := 0; j < 8; j++ {
      fmt.Printf(" %c |", b.grid[i][j])
    }
    fmt.Printf("\n")
    fmt.Printf("---------------------------------\n")
  }

}

func (b board) makeMove(m string) int {

  xI := mvChrt[m[0:1]]
  yI, _ := strconv.Atoi(m[1:2])
  yI--

  xF := mvChrt[m[2:3]]
  yF, _ := strconv.Atoi(m[3:4])
  yF--

  p := b.getPiece(xI, yI)

  switch p {
  case 'X':
    fmt.Println("ERROR: WRONG MOVE LOCATION")
    return 0
  case 'p':
    if b.pawnCheck(xI, yI, xF, yF, 1) {
      b.setPiece(xI, yI, '-')
      b.setPiece(xF, yF, p)
      return 1
    }
    return 0
  case 'P':
    if b.pawnCheck(xI, yI, xF, yF, 0) {
      b.setPiece(xI, yI, '-')
      b.setPiece(xF, yF, p)
      return 1
    }
    return 0
  default:
    b.setPiece(xI, yI, '-')
    b.setPiece(xF, yF, p)
  }

  return 1
}

func (b board) getPiece(x int, y int) (p rune) {

  if x > 7 || x < 0 || y > 7 || y < 0 {
    return 'X'
  }
  p = b.grid[y][x]
  return p

}

func (b board) setPiece(x int, y int, p rune) {

  if x > 7 || x < 0 || y > 7 || y < 0 {
    return
  }
  b.grid[y][x] = p

}

func (b board) getColor(p rune) int {

  switch {
  case 'a' <= p && p <= 'z':
    return 1
  case 'A' <= p && p <= 'Z':
    return 0
  }
  return -1
}

func (b board) pawnCheck(xI int, yI int, xF int, yF int, c int) bool {

  //Not eating piece
  if xI == xF {
    return b.pawnMove(xI,yI,xF,yF,c)
  }

  //Eating piece
  return b.pawnEat(xI,yI,xF,yF,c)

}

func (b board) pawnMove (xI int, yI int, xF int, yF int, c int) bool {
  //Moving white piece
  if c == 0 {
    p1 := b.getPiece(xI, yI+1)
    if p1 != '-' {
      return false
    }
    p2 := b.getPiece(xI, yI+2)

    if yI == 1 {

      if ((yF-yI) == 2 || (yF-yI) == 1) && p2 == '-' {
        return true
      }
      return false
    }

    if (yF - yI) == 1 {
      return true
    }
    return false
  }

  //Moving black piece
  p1 := b.getPiece(xI, yI-1)
  if p1 != '-' {
    return false
  }
  p2 := b.getPiece(xI, yI-2)

  if yI == 6 {

    if (yI-yF) == 2 || (yI-yF) == 1 && p2 == '-' {
      return true
    }
    return false
  }

  if (yI - yF) == 1 {
    return true
  }
  return false
}

func (b board) pawnEat(xI int, yI int, xF int, yF int, c int) bool {
  p1 := b.getPiece(xF, yF)
  //Moving white piece
  if c == 0 {
    if (p1 != '-' && p1 != 'X' && b.getColor(p1) == 1) &&
      (xI-xF == 1 && (yF-yI)*(yF-yI) == 1) {
      return true
    }
    return false
  }

  //Moving black piece
  if (p1 != '-' && p1 != 'X' && b.getColor(p1) == 0) &&
    (xF-xI == 1 && (yI-yF)*(yI-yF) == 1) {
    return true
  }
  return false
}

func readFileToStr(fn string) (arr []string, err error) {

  //Save file to buffer and close
  file, err := os.Open(fn)
  defer file.Close()

  if err != nil {
    return nil, err
  }

  //Cread reader buffer from file
  reader := bufio.NewReader(file)

  //Start reading buffer
  var line string
  for {
    line, err = reader.ReadString('\n')
    //Delete \n from last character of string
    if last := len(line) - 1; last >= 0 && line[last] == '\n' {
      line = line[:last]
    }
    if line != "" {
      arr = append(arr, line)
    }
    if err != nil {
      break
    }
  }

  if err != io.EOF {
    fmt.Printf("ERROR: %v\n", err)
  }

  return arr, nil
}

/*
./lope391
6 4 0 0 6 0 6 4 4 3 4 4 2
*/
