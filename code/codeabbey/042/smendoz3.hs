-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (checkAll inputs))

checkAll :: [String] -> [String]
checkAll = map function

function :: String -> String
function x = countCard (checkString x) 0 0

checkString :: String -> String
checkString str = concat (words str)

evalNum :: Char -> Int
evalNum '2' = 2
evalNum '3' = 3
evalNum '4' = 4
evalNum '5' = 5
evalNum '6' = 6
evalNum '7' = 7
evalNum '8' = 8
evalNum '9' = 9
evalNum 'T' = 10
evalNum 'J' = 10
evalNum 'Q' = 10
evalNum 'K' = 10
evalNum 'A' = 1

countCard :: String -> Int -> Int -> String
countCard [x] result a
  | (evalNum x + result) > 21 = "Bust"
  | a == 0 && x /= 'A' = value
  | (evalNum x + result) < 12 = show (result + evalNum x + 10)
  | otherwise = value
  where value = show (result + evalNum x)
countCard (x:xs) result a = if x == 'A' then ace else noAce
  where ace = countCard xs (result + evalNum x) (a + 1)
        noAce = countCard xs (result + evalNum x) a

-- $ ./smendoz3
--   "Bust 21 17 21 Bust 18 18 19 16 21 Bust 21 17 Bust 21 16 18 18 21 20"
