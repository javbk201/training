;; $ clj-kondo --lint slayfer1112.clj
;; linting took 40ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn get-vals [line]
  (let [values (vec (sort #(compare %2 %1)
                          (str/split (str/replace line #"[KQJT]" "10") #" ")))]
    values))

(defn points [values aces]
  (if (= -1 (.indexOf values "A"))
    [aces (reduce + (vec (map read-string values)))]
    (points (subvec values 1 (count values)) (inc aces))))

(defn possible-score [ace counter p-counter]
  (if (= counter ace)
    p-counter
    (let [p-score (+ (* (- ace counter) 11) p-counter)]
      (if (> p-score 21)
        (possible-score ace (inc counter) (+ 1 p-counter))
        (possible-score ace (inc counter) (+ 11 p-counter))))))

(defn solution [_ body]
  (doseq [x body]
    (let [values (get-vals x)
          [aces points-wa] (points values 0)
          total-score (possible-score aces 0 points-wa)
          ]
      (if (> total-score 21)
        (print "Bust ")
        (print (str total-score " "))))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 20 16 21 16 20 16 21 16 17 Bust 17 Bust 21
;; 18 17 Bust 18 18 Bust 16 16 21 16 17 16
