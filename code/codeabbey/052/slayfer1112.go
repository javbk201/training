/*
> go fmt
*/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {
  dataIn, _ := os.Open("DATA.lst")
  dataScan := bufio.NewScanner(dataIn)
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      a, _ := strconv.Atoi(line[0])
      b, _ := strconv.Atoi(line[1])
      c, _ := strconv.Atoi(line[2])

      a2 := a * a
      b2 := b * b
      c2 := c * c
      d := a2 + b2
      if (d - c2) == 0 {
        fmt.Print("R ")
      }
      if (d - c2) > 0 {
        fmt.Print("A ")
      }
      if (d - c2) < 0 {
        fmt.Print("O ")
      }
    }
  }
}

/*
> go run slayfer1112.go
O A A R R R R R O O A A O A R R R R O A
*/
