#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.81 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(3).each do |x|
  value = Math.sqrt(args[x] ** 2 + args[x + 1] ** 2)
  print "A " if value > args[x + 2]
  print "O " if value < args[x + 2]
  print "R " if value == args[x + 2]
end

puts

# $ ./richardalmanza.cr
# O A A R R R R R O O A A O A R R R R O A
