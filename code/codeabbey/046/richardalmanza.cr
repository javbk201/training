#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 5.91 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def tictactoe(matrix, symbol)
  counters = (0...8).to_a.map {0}

  (0...9).each do |x|
    pos = {"col" => x % 3, "row" => x // 3}

    # Vertical
    (0...3).each do |y|
      counters[y] += 1 if matrix[x] == symbol && pos["col"] == y
    end

    # Horizontal
    (0...3).each do |y|
      counters[y + 3] += 1 if matrix[x] == symbol && pos["row"] == y
    end

    # Diagonal, negative
    if pos["row"] == pos["col"]
      counters[6] += 1 if matrix[x] == symbol
    end

    # Diagonal, positive
    if (pos["row"] - pos["col"]).abs == 2 || x == 4
      counters[7] += 1 if matrix[x] == symbol
    end
  end

  3.in?(counters)
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i - 1}

(0...args.size).step(9).each do |x|
  game = (0...9).to_a.map {9}
  move = 0

  (0...9).each do |y|
    game[args[x + y]] = y % 2

    break if tictactoe(game, y % 2)
    move += 1
  end

  print "#{move + 1} " if move < 9
  print "0 " unless move < 9
end

puts

# $ ./richardalmanza.cr
# 0 9 7 5 9 8 9 0 8 9 7 7 7 7 7 0 7
