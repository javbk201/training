--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local d = -1
for line in data do
    if d ~= -1 then
        for token in string.gmatch(line, "[^%s]+") do
            local c = 0
            local num = tonumber(token)
            while num ~= 1 do
                if num%2==0 then
                    num = num/2
                else
                    num = 3 * num + 1
                end
                c = c + 1
            end
            print(c)
        end
    end
    d = d +1
end

--[[
$ lua mrsossa.lua
106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
]]
