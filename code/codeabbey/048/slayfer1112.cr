#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.2 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  sol = [] of Int32
  counter = 0
  array.each do |x|
    while x != 1
      counter += 1
      mod = x.divmod(2)
      if mod[1] == 0
        x = x / 2
      else
        x = 3 * x + 1
      end
      if x == 1
        sol << counter
        counter = 0
      end
    end
  end
  sol.each do |x|
    print "#{x} "
  end
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 34 91 16 18 37 18 33 30 55 6 45 128 110 15 37 16 18 102 109 18 29 9 19 106 14
