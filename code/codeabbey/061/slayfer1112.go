/*
$ golint slayfer1112.go

*/

package main

import (
  "bufio"
  "fmt"
  "math"
  "os"
  "strconv"
  "strings"
)

//Function to scan data from DATA.lst
func dataEntry() []string {

  dataIn, _ := os.Open("Data.lst")
  dataScan := bufio.NewScanner(dataIn)
  var data []string
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      data = line
    }
  }

  return data

}

//Prime eval if a number is prime or not
func Prime(number int) bool {

  maxDivisor := math.Sqrt(float64(number))

  //1 isn't a prime number
  if number == 1 {
    return false
  }

  //2 is the first prime number
  if number == 2 {
    return true
  }

  //An even number can't be prime
  if number%2 == 0 {
    return false
  }

  /*
     This eval all numbers starting in 3 until the last divisor
     and avoid to eval the even numbers because we eval even numbers
     with the previous lines
  */
  lastDivisor := int(math.Ceil(maxDivisor))
  for i := 3; i <= lastDivisor; i += 2 {
    if number%i == 0 {
      return false
    }
  }

  return true

}

func main() {

  data := dataEntry()

  var Pnumber []int

  //Len of the array recommended in code Abbey
  max := 200000

  //Make the array with the prime numbers in the range
  //This can take some seconds creating all entries of the array
  for i := 0; len(Pnumber) <= max; i++ {
    if Prime(i) {
      Pnumber = append(Pnumber, i)
    }
  }

  //Print the prime number in the selected position
  for i := 0; i < len(data); i++ {
    x, _ := strconv.Atoi(data[i])
    fmt.Print(Pnumber[x-1], " ")
  }

}

/*
$ go run slayfer1112.go
2529089 2018167 2543621 1800619 1955113 2491117
2684933 1422089 2464349 1370053 2224753 1761883
*/
