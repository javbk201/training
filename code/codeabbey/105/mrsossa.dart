/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    var sum = 0;
    for (var i = 1; i < c; i++) {
      var subI = i + 1;
      if (i == data.length - 2) {
        subI = 1;
      }
      var token = data[i].split(' ');
      var token2 = data[subI].split(' ');
      var sumA = int.parse(token[0]) * int.parse(token2[1]);
      var sumB = int.parse(token[1]) * int.parse(token2[0]);
      sum = sum + (sumA - sumB);
    }
    print(0.5 * sum);
  });
}

/* $ dart mrsossa.dart
 * 60992863
*/
