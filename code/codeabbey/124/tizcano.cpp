/*
  $ g++ tizcano.cpp -o tizcano.out
  $
*/
#include<iostream>
#include<algorithm>
#include<fstream>
#include<vector>

using namespace std;

void solver(int w[], int val[], int N, int W) {
  int grid[N+1][W+1];
  for (int i = 0; i <= N; ++i) {
    for (int j = 0; j <= W; ++j) {
      if (i==0 || j==0) {
        grid[i][j] = 0;
      }
      else if (w[i-1] <= j) {
        grid[i][j] = max(val[i-1] + grid[i-1][j-w[i-1]],  grid[i-1][j]);
      }
      else {
        grid[i][j] = grid[i-1][j];
      }
    }
  }
  int maximum = grid[N][W];
  for (int i = N; i > 0 && maximum > 0; i--) {
    if (maximum == grid[i - 1][W]) {
      continue;
    }
    else {
      cout<<i - 1<<" ";
      maximum = maximum - val[i - 1];
      W = W - w[i - 1];
    }
  }
}

int main() {
  ifstream input("DATA.lst");
  cin.rdbuf(input.rdbuf());
  int N, Wmax;
  cin>>N>>Wmax;
  int w[N];
  int v[N];
  for (int i = 0; i < N; ++i) {
    cin>>w[i]>>v[i];
  }
  solver(w, v, N, Wmax);
  return 0;
}
/*
  $ ./tizcano.out
  output:
  144 141 140 139 137 136 135 134 132 131 126 124 121 120 119 118 116 115 114
  113 111 110 109 107 106 105 104 103 100 98 97 96 95 94 93 91 90 89 88 87 86
  85 84 83 81 80 79 77 76 75 73 71 70 69 68 67 66 65 62 61 60 59 58 57 56 55 54
  53 52 47 46 45 44 43 42 41 40 39 38 37 36 34 31 29 28 27 24 22 21 20 19 18 16
  15 14 13 12 10 8 7 6 5 4 3 2 1 0
*/
