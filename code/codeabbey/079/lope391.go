/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build993822926
*/

package main

import (
  "bufio"
  "fmt"
  "io"
  "os"
  "path/filepath"
  "strconv"
  "strings"
)

type graph struct {
  vertex []rune
  vN     int
  edges  []string
}

func (g graph) checkCycles(vertex []rune, edges []string) bool {

  visited := make([]bool, len(vertex))

  for i := 0; i < len(vertex); i++ {
    if !visited[i] {
      if g.recurseCycles(i, visited, -1) {
        return true
      }
    }
  }

  return false
}

func (g graph) recurseCycles(v int, visited []bool, p int) bool {

  visited[v] = true
  adjV := g.getAdjVertx(g.vertex[v])

  for i := 0; i < len(adjV); i++ {
    tmpIndx := sliceIndex(g.vertex, adjV[i])

    if !visited[tmpIndx] {
      if g.recurseCycles(tmpIndx, visited, v) {
        return true
      }
    } else if g.vertex[tmpIndx] != g.vertex[p] {
      return true
    }

  }

  return false
}

func (g graph) getAdjVertx(v rune) (adj []rune) {

  for i := 0; i < len(g.edges); i++ {
    tmpE := []rune(g.edges[i])
    if sliceContainsRune(tmpE, v) {
      if tmpE[0] == v {
        adj = append(adj, tmpE[2])
      } else {
        adj = append(adj, tmpE[0])
      }
    }
  }

  return adj
}

func main() {

  absPath, _ := filepath.Abs("DATA.lst")
  arr, _ := readFileToStr(absPath)
  n, _ := strconv.Atoi(arr[0])
  arr = arr[1:]
  res := ""

  for i := 0; i < n; i++ {

    edges := strings.Split(arr[i], " ")
    nE, _ := strconv.Atoi(edges[0])
    edges = edges[1:]
    var vertex []rune

    for j := 0; j < nE; j++ {
      tmp := []rune(edges[j])
      if !sliceContainsRune(vertex, tmp[0]) {
        vertex = append(vertex, tmp[0])
      }
      if !sliceContainsRune(vertex, tmp[2]) {
        vertex = append(vertex, tmp[2])
      }
    }

    gr := graph{
      vertex: vertex,
      vN:     len(vertex),
      edges:  edges,
    }

    if gr.checkCycles(vertex, edges) {
      res += "1 "
    } else {
      res += "0 "
    }

  }

  fmt.Println(res)
}

func sliceIndex(s []rune, r rune) int {
  for i, v := range s {
    if v == r {
      return i
    }
  }
  return -1
}

func sliceContainsRune(s []rune, c rune) bool {

  for i := 0; i < len(s); i++ {
    if s[i] == c {
      return true
    }
  }

  return false
}

func readFileToStr(fn string) (arr []string, err error) {

  //Save file to buffer and close
  file, err := os.Open(fn)
  defer file.Close()

  if err != nil {
    return nil, err
  }

  //Cread reader buffer from file
  reader := bufio.NewReader(file)

  //Start reading buffer
  var line string
  for {
    line, err = reader.ReadString('\n')
    //Delete \n from last character of string
    if last := len(line) - 1; last >= 0 && line[last] == '\n' {
      line = line[:last]
    }
    if line != "" {
      arr = append(arr, line)
    }
    if err != nil {
      break
    }
  }

  if err != io.EOF {
    fmt.Printf("ERROR: %v\n", err)
  }

  return arr, nil
}

/*
./lope391
0 1 0 0 0 1 1 0 0 1 0 1 1 0 1 0 1 0 1 0 0 1 1 0 0 1 1 1 1
*/
