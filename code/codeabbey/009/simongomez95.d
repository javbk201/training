/*
$ dub --quiet run dscanner -- -s simongomez95.d
$ dub --quiet run dscanner -- -S simongomez95.d
$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

void main() {
  File file = File("DATA.lst", "r");
  string countstring = file.readln();
  countstring = stripws(countstring);
  const int count = to!int(countstring);
  string answer;
  int a, b, c;
  for(int i=0; i<count; i++) {
    string[] numberstring = file.readln().split(" ");
    a = to!int(stripws(numberstring[0]));
    b = to!int(stripws(numberstring[1]));
    c = to!int(stripws(numberstring[2]));
    if((a+b > c) && (a+c > b) && (b+c > a)) answer = answer ~ " 1";
      else answer = answer ~ " 0";
  }
  writeln(answer);
  file.close();
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
$ ./simongomez95
 0 0 1 0 0 1 0 1 0 0 1 0 1 1 1 0 1 0 0 0 1 1
*/
