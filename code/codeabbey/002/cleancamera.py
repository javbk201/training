#!/usr/bin/env python3

"""
$ pylint cleancamera.py.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

from typing import List


def sum_function(file: List[int]) -> None:
    sum_one = 0
    for i in file:
        sum_one = sum_one + i

    print(sum_one)


def divide_array() -> None:
    """
    get data file
    """
    file_open = open('DATA.lst', "r")
    data_file = file_open.read()
    one_data = data_file[3:163]
    data_convert = one_data.split()
    convert_int = list(map(int, data_convert))
    sum_function(convert_int)


divide_array()

# $ python cleancamera.py
# 27098
