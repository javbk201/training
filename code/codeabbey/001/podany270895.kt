/*
$ ktlint \
  --experimental \
  --verbose \
  --disabled_rules=experimental:indent,indent \
  podany270895.kt
*/

fun sum(a: Int, b: Int): Int {
    return a + b
}

fun main(args: Array<String>) {
  println(sum(args[0].toInt(), args[1].toInt()))
}

/*
$ kotlinc \
  -Werror \
  podany270895.kt \
  -include-runtime \
  -d output.jar
$ java -jar output.jar 5 6
11
*/
