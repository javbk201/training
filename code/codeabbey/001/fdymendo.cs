using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: AssemblyVersion ("1.0.0.0")]
[assembly: CLSCompliant (true)]
[assembly: ComVisible (false)]

namespace Fluid
{
    internal static class Program
    {
        static void Main()
        {
            Console.WriteLine("Por favor escriba el primer número:");
            String temp1 = Console.ReadLine();
            Console.WriteLine("Por favor escriba el segundo número");
            String temp2 = Console.ReadLine();
            int.TryParse(temp1, out int num1);
            int.TryParse(temp2, out int num2);
            int resultado = num1 + num2;
            Console.WriteLine("El resultado es: " + resultado.ToString());
            Console.ReadKey();
        }
    }
}
