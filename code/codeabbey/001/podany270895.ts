/*
 $ eslint podany270895.ts
*/

function sum(x: number, y: number): number {
  return x + y;
}

const argv = process.argv.slice(2);

console.log(sum(Number(argv[0]), Number(argv[1])));

/*
 $ tsc podany270895.ts
 $ node podany270895.ts 5 6
 11
*/
