/*
  cppcheck oscardjuribe
  Checking oscardjuribe.cpp ...
  g++ oscardjuribe.cpp -o oscardjuribe
*/
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cmath>

#define PI 3.1415926535897

using namespace std;


double function(double x, double y, double A, double B, double C){
  // method to calculate the value of the function at that poitn

  // calculate (x - A)^2
  double x_pow2_neg = (x - A) * (x - A);
  // calculate (y - A)^2
  double y_pow2_neg = (y - B) * (y - B);

  // calculate (x + A)^2
  double x_pow2_pos = (x + A) * (x + A);
  // calculate (y + A)^2
  double y_pow2_pos = (y + B) * (y + B);

  // return value of gradient
  return (x_pow2_neg) + (y_pow2_neg) + C * exp(- x_pow2_pos - y_pow2_pos);
}

double * get_gradient(double x, double y, double A, double B, double C){
  //method to calculate the gradient given two points

  // delta value
  double dt = 0.00001;

  // calculate f(x+dt, y)
  double function_x_dt = function(x + dt, y, A, B, C);
  // calculate f(x, y)
  double function_x_y = function(x, y, A, B, C);
  // calculate f(x, y+dt)
  double function_y_dt = function(x, y + dt, A, B, C);

  // array to store gradient values
  static double gradient[2];

  // calculate gradient of x
  gradient[0] = (function_x_dt - function_x_y) / dt;
  // calculate gradient of y
  gradient[1] = (function_y_dt - function_x_y) / dt;

  return gradient;
}

int direction(double gradient[2]){
  // method to calculate direction and round it

  return round((atan2(gradient[1],gradient[0]) * 180 / PI) + 180);
}


int main(int argc, char const *argv[]){

  // set precision for calculations
  setprecision(10);

  // object reader
  ifstream inFile;

  // open the file
  inFile.open("DATA.lst");

  // number of points
  int n;
  // constant a
  double A;
  // constant b
  double B;
  // constant c
  double C;

  // read n from file
  inFile>>n;
  // read constants from file
  inFile>>A;
  inFile>>B;
  inFile>>C;

  // var to store current x
  double x;
  // var to store current x
  double y;
  // store the gradient after calculation
  double * gradient;

  // store the direction after calculation
  int dir;

  // var to store answer
  string sol;

  // iterate over n points
  for (int i = 0; i < n; ++i){
    // read x
    inFile>>x;
    // read y
    inFile>>y;
    // calculate the gradient
    gradient = get_gradient(x, y, A, B, C);
    // calculate direction
    dir = direction(gradient);
    // concat current value to solution
    sol.append(to_string(dir)+" ");

  }

  cout<< sol<<endl;
  return 0;
}
/*
$ ./oscardjuribe
316 333 295 262 281 347 312 173 318 301 258 168 302 297 282
*/
