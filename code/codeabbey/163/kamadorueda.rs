/*
 * $ cargo build --release
 *   Finished release [optimized] target(s) in 0.01s
 */

#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use std::io::BufRead;

// problem constants
const dt: f64 = 0.005;
const w: f64 = 600.0;
const h: f64 = 300.0;
const r: f64 = 20.0;
const fr: f64 = 5.0;

struct ball {
  xc: f64,
  yc: f64,
  vx: f64,
  vy: f64,
  vt: f64,
}

fn main() {
  let file = std::fs::File::open("DATA.lst").unwrap();
  let buff = std::io::BufReader::new(file).lines();

  for line in buff {
    let crline: &str = &line.unwrap();
    let params: Vec<&str> = crline.split(' ').collect();

    if params.len() == 1 {
      continue;
    }

    // ball a
    let mut a: ball = ball {
      xc: params[0].parse().unwrap(),
      yc: params[1].parse().unwrap(),
      vx: params[4].parse().unwrap(),
      vy: params[5].parse().unwrap(),
      vt: 0.0,
    };

    // ball b
    let mut b: ball = ball {
      xc: params[2].parse().unwrap(),
      yc: params[3].parse().unwrap(),
      vx: params[6].parse().unwrap(),
      vy: params[7].parse().unwrap(),
      vt: 0.0,
    };

    let mut t: f64 = 0.0;
    while t < 10.0 {
      moveball(&mut a);
      moveball(&mut b);

      let dx: f64 = b.xc - a.xc;
      let dy: f64 = b.yc - a.yc;
      let d: f64 = dist(dx, dy);
      if d <= 2.0 * r {
        let vcx = (a.vx + b.vx) / 2.0;
        let vcy = (a.vy + b.vy) / 2.0;
        let ux = a.vx - vcx;
        let uy = a.vy - vcy;
        let ur = (ux * dx + uy * dy) / d;
        let urx = ur * dx / d;
        let ury = ur * dy / d;
        a.vx -= 2.0 * urx;
        a.vy -= 2.0 * ury;
        b.vx += 2.0 * urx;
        b.vy += 2.0 * ury;
      }

      t += dt;
    }
    a.xc = a.xc.round();
    a.yc = a.yc.round();
    b.xc = b.xc.round();
    b.yc = b.yc.round();

    print!("{} {} {} {} ", a.xc, a.yc, b.xc, b.yc);
  }
  println!("");
}

fn dist(x: f64, y: f64) -> f64 {
  return x.hypot(y);
}

fn moveball(o: &mut ball) -> &mut ball {
  o.xc += o.vx * dt;
  o.yc += o.vy * dt;
  if o.xc + r > w {
    o.xc -= 2.0 * (o.xc + r - w);
    o.vx *= -1.0;
  }
  if o.xc - r < 0.0 {
    o.xc -= 2.0 * (o.xc - r);
    o.vx *= -1.0;
  }
  if o.yc + r > h {
    o.yc -= 2.0 * (o.yc + r - h);
    o.vy *= -1.0;
  }
  if o.yc - r < 0.0 {
    o.yc -= 2.0 * (o.yc - r);
    o.vy *= -1.0;
  }
  o.vt = dist(o.vx, o.vy);
  if o.vt > 0.000001 {
    o.vx -= o.vx / o.vt * fr * dt;
    o.vy -= o.vy / o.vt * fr * dt;
  } else {
    o.vx = 0.0;
    o.vy = 0.0;
  }
  return o;
}

/*
There are two problems with this problem

- The checker at codeabbey is wrong for some inputs

Admin   2015-02-13 15:33:21
Graeme, thanks for your recomendations!

This problem really is bit to capricious,
but I have no idea how to make it better.
I believe that the checker does not use such a small steps,
so probably there is a small difference in implementation of calculations...

- The pseudocode provided sometimes makes balls virtually bounce some deltas
 eventhough this is not noticeable at sight it makes results differ

 demo:    http://codeabbey.github.io/simple-pool/
 source code: https://github.com/CodeAbbey/simple-pool/blob/gh-pages/pool.js
*/

/*
 * $ Cargo run
 * 398 104 107 62 425 258 277 164 24 127 416 124 572 249 87 127
 *
 */
