/**
  Feature: Solving the challenge #211 Information Entropy
  With Node.js
  From http://www.codeabbey.com/index/task_view/information-entropy

  Linting:   eslint jenniferagve.py
  --------------------------------------------------------------------
  (0 errors, 0 warnings)

  This code calculates the entropy for several messages.
*/

function dataconfig(sentences) {
  /* Calculate the number of times that a letter is repeat and
  save them in an array*/
  const INFO_DATA = sentences;
  const DATA_LEN = INFO_DATA.length;
  const UNI_LETTERS = Array.from(new Set(INFO_DATA));
  const LETTERS_COUNT = UNI_LETTERS.map((element) =>
    (INFO_DATA.match(new RegExp(element, 'gi'))).length);
  return [ LETTERS_COUNT, DATA_LEN ];
}

/* eslint no-magic-numbers: ["error", { "ignore": [11,1] }]*/
function entropycal(sentences) {
  /* Calculate the total entrophy for each sentence*/
  const [ LETTERS_COUNT, DATA_LEN ] = dataconfig(sentences);
  const CHARA_PROB = LETTERS_COUNT.map((element2) =>
    ((element2 / DATA_LEN) * (-(Math.log2(element2 / DATA_LEN)))));
  const sum = CHARA_PROB.reduce((element1, element2) => element1 + element2);
  const SUM_CONV = parseFloat((sum).toFixed(11));
  return SUM_CONV;
}

/* eslint no-sync: ["error", { allowAtRootLevel: true }]*/
const filesre = require('fs');
const contents = filesre.readFileSync('DATA.lst', 'utf8', '\n');
function fileprocess() {
  /* Read each line of the file and save it to be process*/
  const sentences = contents.toString().split('\n');
  const SENTENCES = sentences.slice(1);
  return SENTENCES;
}

function main() {
  /* Main where each function is called to calculate final entropy*/
  const sentences = fileprocess();
  const SUM_CONV = sentences.map((element) => entropycal(element));
  const output = SUM_CONV.join(' ');
  process.stdout.write(`${ output } \n`);
  return output;
}

main();


/**
    node jenniferagve.js
    input:5
          team eats cheese
          president plays with zooophthalmology
          helena plays with pigeons
          government loves beer
          irina dreams of clothes
    -------------------------------------------------------------------
    output: 2.77439747035 3.98285666332 3.7034651896 3.363286924 3.7950885864
*/
