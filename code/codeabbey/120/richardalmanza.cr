#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size - 1).each do |x|
  max = [args[0], 0]

  (0...args.size - x).each do |y|
    max = [args[y], y] if args[y] > max[0]
  end

  temp = args[-1 - x]
  args[-1 - x] = max[0]
  args[max[1]] = temp

  print "#{max[1]} "
end

puts

# $ ./richardalmanza.cr
# 111 119 19 31 39 58 52 51 89 11 85 38 69 92 28 61 32 4 4 12 94 49 92 21 42
# 74 15 62 79 85 38 68 28 40 77 50 16 36 5 79 22 48 75 2 9 6 5 34 51 14 33 48
# 27 2 66 42 6 55 43 19 27 20 21 18 34 27 31 15 19 39 7 27 13 17 7 32 41 5 40
# 11 3 25 21 23 12 35 12 10 12 29 11 21 16 17 26 10 4 17 3 20 15 17 1 10 3 8 4
# 2 10 10 0 5 2 7 0 1 2 4 2 0 1
