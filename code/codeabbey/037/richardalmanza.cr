#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args.map {|x| x.to_i}

x = (1 + args[1] / 1200)
p0 = args[0]
n = args[2]
xx = x ** n

puts "#{((p0 * xx * (1 - x)) / (1 - xx)).ceil.to_i}"

# $ ./richardalmanza.cr
# 12765
