/*
$jcdd
golint jcdd.go
*/
package main

import (
    "fmt"
    "strconv"
    "io/ioutil"
    "strings"
    "math"
)

func main() {
    reader, _ := ioutil.ReadFile( "DATA.lst" )
    num := string( reader )
    d := strings.Replace( num, "\r\n", "", -1 )
    element := strings.Split( d, " " )
    p, _ := strconv.ParseFloat( element[0], 64 )
    r, _ := strconv.ParseFloat( element[1], 64 )
    l, _ := strconv.ParseFloat( element[2], 64 )
    r = float64 ( r ) / float64( 1200 )
    // compound interest
    M := p * ( math.Pow ( ( 1 + r ), l ) * r )
    M /= ( math.Pow( ( 1 + r ), l ) - 1 )
    fmt.Printf ( "%.0f", M )
}
/*
  ./jcdd
  12764
*/
