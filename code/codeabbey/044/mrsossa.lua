--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local d = -1
local result  = {}
local case = 0
for line in data do
  if d == -1 then
    case = tonumber(line)
  else
    local n1
    local n2
    local a = 0
    for token in string.gmatch(line, "[^%s]+") do
      if a == 0 then
        n1 = tonumber(token)
      else
        n2 = tonumber(token)
      end
       a = a + 1
    end
      result[d] = n1%6+n2%6+2
  end
  d = d + 1
end
for i = 0, case-1 do
  print(result[i])
end

--[[
$ lua mrsossa.lua
5 7 10 4 9 7 6 4 6 8 3 11 10 9 4 9 5 8 4 8 5 5
]]
