#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def queen_move(q, king, h_step, v_step)
  queen = [q[0], q[1]]
  queen[0] += h_step
  queen[1] += v_step
  return false if 1 > queen[0] || queen[0] > 8
  return false if 1 > queen[1] || queen[1] > 8

  return true if queen == king

  false
end

FILES = " #{("a".."h").to_a.join}"

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..]

(0...args.size).step(2).each do |x|
  k = [FILES.index(args[x][0]) || 0, (args[x][1]).to_i]
  q = [FILES.index(args[x + 1][0]) || 0, (args[x + 1][1]).to_i]

  check = false

  (1..8).each do |step|
    #diagonal positive right
    check = queen_move(q, k, step, step)
    break if check

    #diagonal positive left
    check = queen_move(q, k, -step, -step)
    break if check

    #diagonal negative left
    check = queen_move(q, k, -step, step)
    break if check

    #diagonal negative right
    check = queen_move(q, k, step, -step)
    break if check

    #horizontal positive
    check = queen_move(q, k, step, 0)
    break if check

    #horizontal negative
    check = queen_move(q, k, -step, 0)
    break if check

    #vertical positive
    check = queen_move(q, k, 0, step)
    break if check

    #vertical negative
    check = queen_move(q, k, 0, -step)
    break if check
  end

  print "Y " if check
  print "N " unless check
end

puts

# $ ./richardalmanza.cr
# N N N N N N N N N N N N Y N Y N Y N N N Y N N N Y
