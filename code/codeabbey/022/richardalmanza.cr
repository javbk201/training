#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(3).each do |x|
  p1 = args[x]
  p2 = args[x + 1]
  pages = args[x + 2]

  a = (p1 / (p1 + p2) * pages).ceil * p2
  b = (p2 / (p1 + p2) * pages).ceil * p1

  a = b  if b < a

  print "#{a.to_i} "
end

puts

# $ ./richardalmanza.cr
# 318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
# 309055713 33295378 322580664 359769725 332892224 162592318 175123025
