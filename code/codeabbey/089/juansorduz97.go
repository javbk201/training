/*
$ golint juansorduz97.go
$ go run juansorduz97.go
*/
package main
import(
  "fmt"
  "bufio"
  "os"
  "strconv"
  "strings"
)
func main() {
  FrequencyStep := 1.059463094
  RO := 0
  RSO := 0
  LF := 0.0
  LSF := 0.0
  HSF := 0.0
  HF := 27.5
  FlagFrequencyFound := false
  FlagSpecificFrequencyFound := false
  var Notes = []string{"A","A#","B","C","C#","D","D#","E","F","F#","G","G#","A"}
  file,_ := os.Open("DATA.lst")
  defer file.Close()
  InputBuffer := bufio.NewScanner(file)
  InputBuffer.Scan()
  FirstInput,_ := strconv.ParseInt(InputBuffer.Text(),10,8)
  NumInputs := int(FirstInput)
  InputBuffer.Scan()
  FrequencyData := strings.Fields(InputBuffer.Text())
  for i := 0; i < NumInputs; i++ {
    Frecuency,_ := strconv.ParseFloat(FrequencyData[i],10)
    LF = 0.0
    HF = 27.5
    FlagFrequencyFound = false
    RO = 0
    for FlagFrequencyFound == false {
      if Frecuency > LF && Frecuency <= HF {
        FlagFrequencyFound = true
      }
      if FlagFrequencyFound == false {
        LF = HF
        HF = LF * 2
        RO = RO + 1
      }
    }
    if LF == 0 {
      FlagSpecificFrequencyFound = false
      LSF = LF
      HSF = HF
      RSO = 12
      for FlagSpecificFrequencyFound == false {
        LSF = HSF / FrequencyStep
        if Frecuency > LSF && Frecuency <= HSF {
          FlagSpecificFrequencyFound = true
        }
        if FlagSpecificFrequencyFound == false {
          HSF = LSF
          LSF = HSF / FrequencyStep
          RSO = RSO - 1
        }
      }
    }
    if LF > 0 {
      FlagSpecificFrequencyFound = false
      LSF = LF
      HSF = HF
      RSO = 0
      for FlagSpecificFrequencyFound == false {
        HSF = LSF * FrequencyStep
        if Frecuency > LSF && Frecuency <= HSF {
          FlagSpecificFrequencyFound = true
        }
        if FlagSpecificFrequencyFound == false {
          LSF = HSF
          HSF = LSF * FrequencyStep
          RSO = RSO + 1
        }
      }
      if Frecuency >= (LSF+HSF)/2 {
        RSO = RSO + 1
      }
      if Notes[RSO] == "A" || Notes[RSO] == "A#" || Notes[RSO] == "B" {
        RO = RO - 1
      }
      if RSO == 12 {
        RO = RO + 1
      }
    }
    fmt.Print(Notes[RSO])
    fmt.Print(RO)
    fmt.Print(" ")
  }
  fmt.Println("")
}
/*
$ go run juansorduz97.go
F#3 A#2 G#3 A1 G#1 F2 A#5 A#3 B1 C2 F#4 G5
*/
