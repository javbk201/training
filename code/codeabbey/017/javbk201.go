package main

import (
  "fmt"
)

const (
  SEED = 113
  LIMIT = 10000007
)


func main()  {
  var size int
  var resultado int64
  fmt.Println("Ingrese el tamaño del vector: \n")
  fmt.Scanln(&size)
  a := make([]int64, size)
  //Load the array
  fmt.Println("Ingrese los valores del array")
  for i := 0;i < size;i++ {
    fmt.Scanln(&a[i])
  }
  //
  for i := 0;i < size;i++ {
    resultado = (resultado + a[i]) * SEED
    if resultado > LIMIT {
      resultado = resultado % LIMIT
    }
  }
  fmt.Println(resultado)
}