/**
> golint slayfer1112.go
**/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {

  dictionary := [...]string{" ", "e", "t", "o", "n", "a", "s", "i", "r", "h",
    "d", "l", "!", "u", "c", "f", "m", "p", "g", "w", "b", "y", "v", "j", "k",
    "x", "q", "z"}

  bytes := [...]string{"11", "101", "1001", "10001", "10000", "011", "0101",
    "01001", "01000", "0011", "00101", "001001", "001000", "00011", "000101",
    "000100", "000011", "0000101", "0000100", "0000011", "0000010", "0000001",
    "00000001", "000000001", "0000000001", "00000000001", "000000000001",
    "000000000000"}
  var data []string
  var join string
  var array string
  var b2 []string

  dataIn, _ := os.Open("DATA.lst")

  dataScan := bufio.NewScanner(dataIn)

  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), "")
    data = line
  }

  for i := 0; i < len(data); i++ {
    b32, _ := strconv.ParseInt(data[i], 32, 64)
    b32conv := strings.ToUpper(strconv.FormatInt(b32, 2))
    b2 = append(b2, b32conv)
  }

  for i := 0; i < len(b2); i++ {
    if len(b2[i]) < 5 {
      b2[i] = "0" + b2[i]
      i--
    }
  }

  for i := 0; i < len(b2); i++ {
    join += b2[i]
  }

  data = strings.Split(join, "")

  for i := 0; i < len(data); i++ {
    array += data[i]
    for j := 0; j < len(bytes); j++ {
      if array == bytes[j] {
        fmt.Print(dictionary[j])
        array = ""
      }
    }
  }
}

/**
> go run .\slayfer1112.go
formidable to tyrants only !he has called together legislative bodies at
unacknowledged by our laws giving his !assent to their !acts of pretended
!legislation for that purpose obstructing the !laws for !naturalization of
!foreigners refusing
**/
