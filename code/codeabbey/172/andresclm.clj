;; $ clj-kondo --lint andresclm.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (vec (rest data'))]
    (vec (map (fn [line] (str/split line #" ")) data''))))

(defn calculate-d2 [D1 A B]
  (let [A (Math/toRadians A)
        B (Math/toRadians B)]
    (/ (* D1 (Math/tan A))
       (- (Math/tan B) (Math/tan A)))))

(defn calculate-h [D2 B]
  (let [B (Math/toRadians B)]
    (* (Math/tan B) D2)))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (map (fn [triplet] (let [[D1 A B] triplet
                                        D2 (calculate-d2 (edn/read-string D1)
                                                         (edn/read-string A)
                                                         (edn/read-string B))
                                        result (calculate-h
                                                D2
                                                (edn/read-string B))]
                                    (Math/round result)))
                    data)]
    (apply print result)))

;; $ clj -m andresclm
;; 1283 680 631 1854 1399 1121 1184 1276 1389 1536 750 1098 1938 874 1468 1912
;; 1520
