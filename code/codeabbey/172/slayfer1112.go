/*
$ golint slayfer1112.go
*/
package main

import (
  "bufio"
  "fmt"
  "math"
  "os"
  "strconv"
  "strings"
)

func main() {
  dataIn, _ := os.Open("Data.lst")
  dataScan := bufio.NewScanner(dataIn)
  data := [][]string{}
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      data = append(data, line)
    }
  }

  H := []float64{}
  for i := 0; i < len(data); i++ {
    D1, _ := strconv.ParseFloat(data[i][0], 64)
    A, _ := strconv.ParseFloat(data[i][1], 64)
    A = (A * math.Pi) / 180
    B, _ := strconv.ParseFloat(data[i][2], 64)
    B = (B * math.Pi) / 180
    R := math.Round(D1*(math.Tan(B)*math.Tan(A))/(math.Tan(B)-math.Tan(A)))

    H = append(H, R)
  }

  for i := 0; i < len(H); i++ {
    fmt.Print(H[i], " ")
  }
}

/*
1283 680 631 1854 1399 1121 1184 1276 1389
1536 750 1098 1938 874 1468 1912 1520
*/
