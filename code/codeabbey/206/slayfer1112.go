/**
> golint slayfer1112.go
**/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

var startdata = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
var startArray = strings.Split(startdata, "")
var dictionary = map[int]string{
  0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F", 6: "G",
  7: "H", 8: "I", 9: "J", 10: "K", 11: "L", 12: "M", 13: "N",
  14: "O", 15: "P", 16: "Q", 17: "R", 18: "S", 19: "T", 20: "U",
  21: "V", 22: "W", 23: "X", 24: "Y", 25: "Z", 26: "2", 27: "3",
  28: "4", 29: "5", 30: "6", 31: "7", 32: " "}
var dictionary2 = map[string]int{
  "2": 26, "3": 27, "4": 28, "5": 29, "6": 30, "7": 31, "A": 0,
  "B": 1, "C": 2, "D": 3, "E": 4, "F": 5, "G": 6, "H": 7, "I": 8,
  "J": 9, "K": 10, "L": 11, "M": 12, "N": 13, "O": 14, "P": 15,
  "Q": 16, "R": 17, "S": 18, "T": 19, "U": 20, "V": 21, "W": 22,
  "X": 23, "Y": 24, "Z": 25}

func main() {

  var data []string

  dataIn, _ := os.Open("DATA.lst")

  dataScan := bufio.NewScanner(dataIn)

  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), "\n")
    data = append(data, line[0])
  }

  for i := 1; i < len(data); i++ {
    if (i % 2) != 0 {
      encode(data[i])
    } else {
      decode(data[i])
    }
  }

}

func encode(odd string) {
  text := odd
  length := len(text)
  counter := 0
  var array []string
  var array5 []string

  if length%5 == 0 {
    text += "55555"
  }
  for length%5 != 0 {
    counter++
    length++
  }
  for i := 0; i < counter; i++ {
    text += strconv.Itoa(counter)
  }

  Byte := []byte(text)

  for i := 0; i < len(Byte); i++ {
    x := strconv.FormatInt(int64(Byte[i]), 2)
    for len(x)%8 != 0 {
      x = "0" + x
    }
    array = append(array, x)
  }

  text = strings.Join(array, "")

  array = strings.Split(text, "")

  text = ""

  for i := 1; i <= len(array); i++ {
    text += array[i-1]
    if len(text)%5 == 0 {
      array5 = append(array5, text)
      text = ""
    }
  }

  for i := 0; i < len(array5); i++ {
    x, _ := strconv.ParseInt(array5[i], 2, 32)
    fmt.Print(dictionary[int(x)])
  }

  fmt.Print(" ")
}

func decode(even string) {
  text := even
  split := strings.Split(text, "")
  var array []int
  var array2 []string

  for i := 0; i < len(split); i++ {
    array = append(array, dictionary2[split[i]])
    x := int64(array[i])
    array2 = append(array2, strconv.FormatInt(x, 2))
    for len(array2[i]) < 5 {
      array2[i] = "0" + array2[i]
    }
  }

  text = strings.Join(array2, "")

  array2 = strings.Split(text, "")

  text = ""

  for i := 0; i < len(array2); i++ {
    text += array2[i]
    if (i+1)%8 == 0 {
      text += " "
    }
  }

  array2 = strings.Split(text, " ")

  text = ""

  for i := 0; i < len(array2); i++ {
    var Rune rune
    Int, _ := strconv.ParseInt(array2[i], 2, 32)

    if int(Int) < 49 || int(Int) > 53 {
      Rune = rune(Int)
    } else {
      Rune = '0'
    }
    if string(Rune) != "0" {
      fmt.Print(string(Rune))
    }
  }
}

/**
> go run slayfer1112.go
MZSXE3LFNZ2GS3THEBQXE3LGOVWHGIDUOVRWWZLSOMQHK3TEMVZHI33OMUQG4ZLXONZGKZLMGU2TKNJ
V buttocks commercialization mosaic OJSWG33OON2GS5DVORSSA3LBNRUWO3TBNZRXSIDHPFW
XGIDSMVZWKYLSMNUGS3THEBUW4Z3SMVZXGMRS evoke NFZHEYLENFQXIZLTEBSGS43FNVRG6ZDZNFX
GOIDBMZTGKY3UNFXW4IDCMFZGW2LOM42DINBU histories limpidity swapped debacle ONRW6
5LOMRZGK3DTEBZGK4DPONUXI33SNFSXGMRS bookstore MNQWY3DPOVZWS3THEBQW4Y3FON2G64TFM
QQGM33SMV3WC4TOOMQHA33JNZ2GKZBR lord O5QWY3DPO5ZSAZDFMZQXK3DUMVZHGMRS gambling
vain contractile groggiest ONWHSZLTOQQHG5DSN53GKMRS speaks triplicating cloakin
g NBXXK4THNRQXG43FOMQGC3DPNZTXG2LEMU2DINBU population shrieking contralto ORSW4
ZDFOIQHMYLMNFQW45DMPEQHOYLONZQWEZLTEB2HE2LDNNWGKZBAMVZWGYLMMF2GS33OOM2DINBU bac
kpedalling smear spurred amputating NV2XIZLMPE2DINBU windy rosebush MRUW4Z3PMVZ
SA3LPORUW63TFMQQHAZLOMFXGGZJAOJXXI5DFNY2DINBU minuet toolbars OZXWGYLMEB2WYY3FO
JUW4ZZAMJSXOYLJNQQHAYLUNFSW45BR dodging joust overqualified dartboard NVQW42LQO
VWGC5DJOZSSA3DVMNVWYZLTOMQGEZLBORXGS2ZR ferrules toothsome oversteps
**/
