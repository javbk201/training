## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    PHP local File Inclusion
  Location:
    http://testphp.vulnweb.com/showimage.php
  CWE:
    CWE-098: PHP local File Inclusion
  Rule:
    REQ.050: https://fluidattacks.com/web/rules/050/
  Goal:
    Get source code of site doing local file inclusion
  Recommendation:
    Variables should be sanitized and must work within their scope

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | dirbuster       | 0.9.12      |
    | w3m             | 0.5.3       |
  TOE information:
    Given I have access to the url http://testphp.vulnweb.com/showimage.php
    And the server is running Ngxix version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/showimage.php?file=pictures/2.jpg
    Then I can see an image from the server.

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    When I use dirbuster to detect unprotected folders and links
    And I get these results of the url with parameters
    """
    showimage.php?file=pictures/2.jpg
    """
    And I change them for a filename
    """
    showimage.php?file=showimage.php
    """
    Then I get the source code file[evidence](get-source-code.png)
    And I can conclude that the site is vulnerable through this script.

  Scenario: Exploitation
    When I open the url with these params:
    """
    showimage.php?file=.showimage.php
    """
    Then I can download the source code file[evidence](get-source-code.png)
    Then I conclude that the site is vulnerable through this script

  Scenario Outline: Extraction
  Exposed files
    When I open the url with any name of a script on it.
    And I download and rename that file to the original extension.
    Then I can access <file> and read <output>
    | <file>               | <output> | <evidence>                            |
    | showimage.php        |   OUTPUT | [evidence](showimage.png)             |
    | database_connect.php |   OUTPUT | [evidence](get-source-code.png)       |
    Then I can conclude that any file can be extracted using this vulnerability

  Scenario: Remediation
    When file showimage.php checks for the file existance
    And the script forces to read only from an specific folder
    """
    vulnerable code:

    $name = $_GET["file"];
    $fp = fopen($name, 'rb');
    file[evidence](vulnerable-code.png)

    Fixed code:
    $photo_folder = '/path/to/folder'
    if(strpos( $photo_folder , strip_tags($name) ) !== false){
      $fp = fopen($name, 'rb');
    }else{
      header("HTTP/1.0 404 Not Found");
    }
    """
    Then the script is no longer vulnerable to this attack.
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/showimage.php?file=.showimage.php
    """
    Then I should not be able to download the source code.
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    1.0/10 (Critical) - CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:F/RL:W/RC:C/CR:H/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-06-12
