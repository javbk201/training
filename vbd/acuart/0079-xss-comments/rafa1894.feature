## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Category:
    Cross-Site Scripting
  Location:
    http://testphp.vulnweb.com/comment.php?aid=1 - Name field
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Inject malicious code in the Name field
  Recommendation:
    Encode and validate user input

  Background:
  Hacker's software:
    | Ubuntu          | 18.04.2 LTS |
    | Mozilla Firefox | 67.0        |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And entered one of its pages that accepts user input as comments

  Scenario: Normal use case
    When I access http://testphp.vulnweb.com/comment.php?aid=1
    And write "John" in the "Name" field
    And write "My comment" in the "Comment" field
    And click on "Submit"
    Then the page output is "John, thank you for your comment."

  Scenario: Static detection
    When I look at the source code of the page for commenting
    Then I can see that the vulnerability is being caused by line 25
    """
    25  <td valign="top"><input name="name" type="text" id="name"
    value="&lt;your name here&gt;" size="25"></td>
    """
    And I also look at the source code of the page after submitting
    Then I can see a vulnerability in line 20
    """
    <p class='story'>John, thank you for your comment.</p><p
    class='story'><i></p></i></body>
    """
    Then I can conclude that the input isn't encoded nor validated

  Scenario: Dynamic detection
    When I access http://testphp.vulnweb.com/comment.php?aid=1
    Then I can write the following script in the name field
    """
    <script>alert("Hacked")</script>
    """
    Then I get the output: [evidence](img1.png)
    And I can conclude that code can be injected in this field

  Scenario: Exploitation
    When I access http://testphp.vulnweb.com/comment.php?aid=1
    Then I can execute the following command:
    """
    <script>alert("Hacked")</script>
    """
    Then I get the output: [evidence](img1.png)
    And I can conclude that code can be injected in this field

  Scenario: Remediation
    When I have patched the code by adding HtmlEncode
    Then the line 25 would be the following
    """
    25  <td valign="top"><input name="name" type="text" id="name"
    value=<%= HtmlEncode(var) %> size="25"></td
    """
    And also modify considering the input context
    And perform data validation
    Then I can prevent most code injections

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.4/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.6/10 (Medium) - CR:H/MAC:H/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019/06/05
