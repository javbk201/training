## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
   Cleartext Storage
  Location:
    Kioptrix4
  CWE:
    CWE-312: Cleartext Storage of Sensitive Information
  Rule:
    REQ.127 Store hashed passwords
  Goal:
    Capture credentials on shared network
  Recommendation:
    Store passwords using hash algorithms.

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site 192.168.60.130/member.php?username=something

  Scenario: Normal use case:
    Given I access http://192.168.60.130/member.php?username=something
    Then I obtain the normal result from the page

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I search the url ../member.php?username=something
    And exploit the vulnerability 0089 SQL Injection
    And that I have usernames listed using dirbuster
    Then I have full access to the username password

  Scenario: Exploitation
    Given I have access to the database via SQL Injection
    And that I have usernames
    Then I use the vulnerability 0089 SQL Injection
    And see clear text credentials
    Then I can access to the user interface as another user

  Scenario: Remediation
    Store passwords using hash algorithms.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) -AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.5 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.5 (Medium) - CR:M/IR:M/AR:M/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-02
