## Version 1.4.1
## language: en

Feature:
  TOE:
    zixem
  Category:
    Cross Site Scripting
  Location:
    http://zixem.altervista.org/XSS/3.php - user (field)
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags
    in a Web Page (Basic XSS)
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute an alert with message 1337
  Recommendation:
    Encode the URL and use a filter function for HTML tags

  Background:
  Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE information:
    Given I am accessing the site
    And The page displays a message
    And I realize the URL has a parameter
    When I change that parameter
    Then The message changes with my own parameter
    Then A possible vulnerability with XSS could be exposed

  Scenario: Normal use case
    Given The page just displays a message
    When I check out all the page
    Then I realize displaying a message is its only functionality

  Scenario: Static detection
    Given There is not access to the source code
    When I try a static detection
    Then I realize this is not possible

  Scenario: Dynamic detection
  Detecting DOM based XSS
    When The data is passed by parameter in the URL
    Then The value is displayed as a message
    When I input HTML tags in the URL
    Then HTML tags are represented correctly
    Then I can conclude the site is vulnerable to Cross-site scripting [evidence](evidence.png)

  Scenario: Exploitation
    When I input the following string
    """
    %0A<img src="error" onerror="alert(1337)"/>
    """
    Then the site executes the JavaScript code [evidence](evidence1.png)

  Scenario: Remediation
    Given A PHP function called: strip_tags
    When A HTML tag is entered over the URL or any input
    Then The function will delete any HTML tag from the input, in this case would be:
    """
    $name = strip_tags($POST["name"])
    """
    Then If the past line is added to the source code, the vulnerability would be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:P/RL:U/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    4.3/10 (High) - CR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-01-09