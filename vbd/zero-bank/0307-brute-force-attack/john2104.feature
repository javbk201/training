## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
   Bruteforce attack
  Location:
    http://zero.webappsecurity.com/signin.html
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.0237. The system must guarantee that the person who performs the
    registration, authentication and password reset actions is human (using
    CAPTCHA or incremental delays)
  Goal:
    Detect and exploit a bruteforce attack
  Recommendation:
    Use CAPTCHA or incremental delays

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | NixOS                | 74.0 (64-bit)  |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 1.7.36         |
  TOE information:
    Given I am accessing the site http://zero.webappsecurity.com
    And Entered to site .../login.html
    Then I can see there is a login page
    And allows me to connect with given credentials

  Scenario: Normal use case
    Given I access zero.webappsecurity.com/login.html
    And I write an username in the login input
    And A password in the password input
    Then I push the Sign in button
    And I get access to the user pages

  Scenario: Static detection
    Given I do not have access to the source coude
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access zero.webappsecurity.com/signin.html
    Then I can intercept the request
    And send it to the repeater
    Then I can try different credentials
    And get access with one of them

  Scenario: Exploitation
    Given I access zero.webappsecurity.com/signin.html
    Then I can use the intruder in Burpsuite
    And change the positions to login and password
    And change the Attack type to Cluster bomb
    Then I can fill the payloads with usernames and passwords
    And start the attack
    Then I can check what credentials worked
    And have access to the user pages

  Scenario: Remediation
    Use CAPTCHA or incremental delays to deter attackers to perform bruteforce

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.3 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.1 (High) - CR:H/IR:H/AR:M/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-03-25
