## Version 1.4.2
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Security Features
  Location:
    /WebGoat/challenge/5 - exception, message, trace (Return values)
  CWE:
    CWE-255: Credentials Management
  Rule:
    REQ.125: https://fluidattacks.com/web/es/rules/125/
  Goal:
    Use a pwnd password to gain access
  Recommendation:
    Let the users change their password

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge5.lesson"
    Then I get a login page where I can submit credentials

  Scenario: Static detection
  No code available

  Scenario: Dynamic detection
  Cant change password
    Given I create an account
    Then when I need to change it I don't find the option
    And I have to stay with the same password forever

  Scenario: Exploitation
  using pwnd password
    Given I have access to a combolist from another hacked website
    And a user has an account in this site with the same password as there
    Given users have no way of changing their password
    Then I have access to those accounts

  Scenario: Remediation
  Implement a password change system
    Given I implement a password changing system
    Then users can change their passwords
    And the vuln is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-21
