## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Improper Session Management
  Location:
    http://testaspnet.vulnweb.com/PostNews.aspx - page (header)
  CWE:
    CWE-614: Sensitive Cookie in HTTPS Session Without 'Secure' Attribute
      https://cwe.mitre.org/data/definitions/614.html
  Rule:
    REQ.029 Cookies with security attributes
      https://fluidattacks.com/web/rules/029/
  Goal:
    Detect and exploit unsafe cookies management
  Recommendation:
    Set the secure attribute for sensitive cookies

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
    | Burpsuite     | 2.1.02        |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/payment_analysis/checkdata.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Main page with different options to test vulnerabilities
    Given I access http://webscantest.com/
    Then I can see multiple security test
    And a login button

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I enter the normal use case scenario
    When I navigate through multiple test eliminating cookies with Burpsuite
    And capture response headers
    Then I find how the cookies are being set
    """
    HTTP/1.1 200 OK
    Date: Mon, 04 May 2020 17:50:09 GMT
    Server: Apache/2.4.7 (Ubuntu)
    X-Powered-By: PHP/5.5.9-1ubuntu4.29
    Set-Cookie: TEST_SESSIONID=1bjq3vi9u3o3pv9pkp1k43i2u6; path=/
    Set-Cookie: NB_SRVID=srv140717; path=/
    """
    And I can see the site is generating cookies with default configuration
    And I can conclude webpage does not manage samesite attribute

  Scenario: Exploitation
    Given Website does not use "secure" or "strict" SameSite attribute
    When I access the website
    """
    http://webscantest.com/crosstraining/sitereviews.php
    """
    When I submit a product review with a malicious link
    And someone click on it
    Then The webpage can send cookies in certain cross-site request or CSRF

  Scenario: Remediation
    Given the cookies are being set in default
    When I generate a cookie with the parameter
    """
    Set-Cookie: CookieName=CookieValue; SameSite=Strict;
    or
    Set-Cookie: CookieName=CookieValue; SameSite=Secure;
    """
    And limit the cookie lifespan
    And consider the use of the HTTPOnly attribute too
    Then the webpage can prevent CSRF and cross-site requests

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    3.9/10 (Low) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    3.9/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
  vbd/web-scan-test-site/0352-csrf-token
    Given I know webpage is vulnerable to CSRF
    Then I can perform the same exploitation
