## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
    SQL Injection
  Location:
    http://webscantest.com/datastore/search_by_name.php - name (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Exploit SQL Injection vulnerability using single quotes
  Recommendation:
    Sanitize inputs
    Use of Prepared Statements
    Use of Stored Procedures
    Escaping all User Supplied Input

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/datastore/search_by_name.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Users can use the webapp to search names of tools to see description or price
    When I input a tool name
    Then I get the respective tool ID, name, description, price and picture

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
    When I access http://webscantest.com/datastore/search_by_name.php
    Then I begin testing the input "name" field checking for errors
    And I see that the parameter "id=1 AND 1=2" return error
    """
    Invalid Product
    """
    And Based on this result I conclude there exists a SQLi
    And I can conclude that the service does not sanitize inputs

  Scenario: Exploitation
    When I access to Normal use case
    Then I test the type of injection by injecting a boolean expression
    """
    Rake' AND 1='1
    """
    And the query isn't generating errors
    And injection should work on this website
    When I use the PayLoad
    """
    Rake' OR 1='1
    """
    Then I can see all database content [evidence](img1.png)
    And the website is vulnerable to SQL injection

  Scenario: Remediation
    Given The site is susceptible to SQL injection attacks
    When I make use of Prepared Statements or Stored Procedures
    Then the webpage can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
  No correlations have been found to this date 2020-04-15
