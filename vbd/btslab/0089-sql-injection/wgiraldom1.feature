## Version 1.4.1
## language: en

Feature:
  TOE:
    BTSLab - SQL Injection
  Location:
    http://127.0.0.1/btslab/vulnerability/forum.php - ID field
  Category:
    Web
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in a SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.105: https://fluidattacks.com/web/rules/105/
  Goal:
    Prove existence of a SQL Injection vulnerability
  Recommendation:
    Sanitize user input.
    Limit the amount of successive wrong queries made by an user.
    Prevent confirmation/error messages when possible.

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
    | Apache Web Server |    2.4.1   |
    | PHP               |    7.1.1   |
    | MySQL             |   10.4.6   |
  TOE information:
    Given I'm accessing the site through http://localhost/btslab
    When the Web and MySQL Servers are running,
    Then analysis can be started.

  Scenario: Normal use case
    Given I got to the forum page,
    And I don't need any sort of credentials to post,
    When I try to create a new post.
    Then it appears as the last one made.

  Scenario: Static detection
    Given source code clearly is prone to SQL Injection,
    Given its core algorithm in "/vulnerability/forum.php":
    """
    ...
    01: if(isset($_SESSION['isLoggedIn']))
    02: {
    03:     $user=$_SESSION['username'];
    04: }
    05: else
    06: {
    07:     $user="Anonymous";
    08: }
    09: mysql_query($con, "INSERT into posts(content,title,user) values " .
    10:      "('$content','$title','$user')")
    11:     or die("Failed to post ".mysql_error());
    12: }

    ...
    """
    When we poison either $content, $title or $user,
    Then we could INSERT
    But also SELECT information

  Scenario: Dynamic detection
    Given the site exposes the fields for the new post
    When I poison "title" with 'abcde
    Then an error should be produced
    Given the unbalanced single quotes.
    And the error got produced when the form got sent (evidence)[img1.png].

  Scenario: Exploitation
    Given we can manipulate the INSERT statement content,
    When we, for example, escape the title param with
    """
      title = desired_title', 'fakeuser') --
    """
    Then we will be able to post to the forum
    And use a diferent user for it (evidence)[img2.png]

  Scenario: Extraction
    Given we are able to INSERT the output of a MySQL functions,
    When we put on the title param:
    """
      title = desired_title', LOAD_FILE("C:/xampp/htdocs/index.php"))--
    """
    Then we are able to read the contents of all files on the DBMS.
    And we did (evidence)[img3.png]

  Scenario: Remediation
    Given we escape all user input:
    """
    46:   $content = mysql_real_escape_string($content);
    47:   $title = mysql_real_escape_string($title);
    48:   $user = mysql_real_escape_string($user);
    49:   mysql_query($con, "INSERT into posts(content,title,user) values ".
          "('$content','$title','$user')")
          or die("Failed to post ".mysql_error($con));
    """
    When we post again poisoned data,
    Then the application is not vulnerable to the attacks described above.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.5/10 (Medium) - E:F/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - CR:H/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-02
