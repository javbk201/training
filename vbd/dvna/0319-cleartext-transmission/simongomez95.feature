## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Insecure Communication
  Location:
    /login - username, password (Fields)
  CWE:
    CWE-0319: Cleartext Transmission of Sensitive Information -variant-
      https://cwe.mitre.org/data/definitions/319.html
    CWE-0326: Inadequate Encryption Strength -class-
      https://cwe.mitre.org/data/definitions/326.html
    CWE-1013: Encrypt Data -category-
      https://cwe.mitre.org/data/definitions/1013.html
  CAPEC:
    CAPEC-158: Sniffing Network Traffic -detailed-
      http://capec.mitre.org/data/definitions/158.html
    CAPEC-157: Sniffing Attacks -standard-
      http://capec.mitre.org/data/definitions/157.html
    CAPEC-117: Interception -meta-
      http://capec.mitre.org/data/definitions/117.html
  Rule:
    REQ.185: https://fluidattacks.com/web/es/rules/185/
  Goal:
    Sniff victim's credentials
  Recommendation:
    Always encrypt sensitive data

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running DVNA on http://localhost:8000

  Scenario: Normal use case
    Given I go to http://localhost:8000
    Then I can navigate the site

  Scenario: Static detection
  No code available for this vulnerability

  Scenario: Dynamic detection
  Detecting http communication
    Given I go to http://localhost:8000
    And submit my credentials
    Then I notice the whole process is going through http
    Then I know all the data is traveling in plaintext

  Scenario: Exploitation
  Sniff another user's credentials
    Given I credentials are being sent in plaintext
    Then I sniff the network with Wireshark
    And wait until the victim logs in to the site
    Then I capture the http packet with his creds (evidence)[wireshark.png]
    Then I can login as the victim

  Scenario: Remediation
  Implement HTTPS
    Given I set the server up to only accept encrypted connections via HTTPS
    Then all the data is encrypted
    And an attacker can't sniff credentials anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-29