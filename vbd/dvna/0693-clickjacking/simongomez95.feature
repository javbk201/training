## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Other Bugs
  Location:
    app/useredit - Headers
  CWE:
    CWE-1021: Improper Restriction of Rendered UI Layers or Frames -base-
      https://cwe.mitre.org/data/definitions/1021.html
    CWE-0441: Unintended Proxy or Intermediary ('Confused Deputy') -class-
      https://cwe.mitre.org/data/definitions/441.html
    CWE-0442: Web Problems -category-
      https://cwe.mitre.org/data/definitions/442.html
  CAPEC:
    CAPEC-222: iFrame Overlay -detailed-
      https://capec.mitre.org/data/definitions/222.html
    CAPEC-103: Clickjacking -standard-
      https://capec.mitre.org/data/definitions/103.html
    CAPEC-173: Action Spoofing -meta-
      https://capec.mitre.org/data/definitions/173.html
  Rule:
    REQ.175: https://fluidattacks.com/web/es/rules/175/
  Goal:
    Make a user click on a site action without knowing
  Recommendation:
    Implement a Content Security Policy

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/app/useredit
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000
    Then I can navigate te site normally

  Scenario: Static detection
  No security headers
    When I look at the code at "/dvna/routes/app.js"
    """
    ...
    05  module.exports = function () {
    06      router.get('/', authHandler.isAuthenticated, function (req, res) {
    07          res.redirect('/learn')
    08      })
    09
    10      router.get('/usersearch', authHandler.isAuthenticated, function (req
    , res) {
    11          res.render('app/usersearch', {
    12              output: null
    13          })
    14      })
    15
    16      router.get('/ping', authHandler.isAuthenticated, function (req, res)
     {
    17          res.render('app/ping', {
    18              output: null
    19          })
    20      })
    ...
    """
    Then I see it doesn't have a middleware to add security headers

  Scenario: Dynamic detection
  Request Analysis
    Given I use Burp to intercept a request to the password reset page
    And the response doesn't contain a CSP or X-Frame-Options header
    """
    POST /app/useredit HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/app/useredit
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 40
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AbkE-ktw_GSO1pdPj7wsbVgd
    dUZKH5tf_.4hjFmNywNQEjr1mNN8MrylaQ52JWXOliji4kssQpxrs
    Upgrade-Insecure-Requests: 1

    id=2&name=b&email=b&password=&cpassword=
    """
    Then I know it's injectable in an iFrame and I can clickjack the page

  Scenario: Exploitation
  Clickjacking to get a victim to change their password
    Given I know the page is vulnerable to clickjacking
    Then I create a fake page that promises prizes for entering "bunnies" twice
    And overlap it on an iFrame of the password change page
    Then I send it to an user
    And they unsuspectingly input "bunnies" and click submit
    Then I have their new password and can take over their account

  Scenario: Remediation
  Adding security headers
    Given I add this to the "app.js" controller
    """
    ...
    app.use(function(req, res, next) {
        res.header('X-Frame-Options', 'DENY');
        next();
    });
    """
    Then the "X-Frame-Options: DENY" header is added
    And the pages can't be injected into iFrames anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.9/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.5/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.5/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
