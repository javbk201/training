## Version 1.4.1
## language: en

Feature:
  TOE:
    Badstore
  Location:
    http://www.badstore.com/cgi-bin/test.cgi
  CWE:
    CWE-522: Insufficiently Protected Credentials
  Rule:
    REQ.100 Seal windows containing assets
  Goal:
    Acces to credentials and accesin to system like Master System Administrator
  Recommendation:
    Delete or ban the path test.cgi

  Background:
  Hacker's software:
    |    <Software name>          |    <Version>          |
    |    VMWare                   | 14.1.3 build-9474260  |
    |    Firefox Quantum          | 60.4.0esr(64-bit)     |
    |    Kali GNU/Linux Rolling   | 4.18.0-kali2-amd64    |
    |    Nmap                     | Nmap 7.70             |
  TOE information:
    Given I am accessing the site www.badstore.com
    And Apache httpd 1.3.28 ((Unix) mod_ssl/2.8.15 OpenSSL/0.9.7c)
    And ssl/https Apache/1.3.28 (Unix) mod_ssl/2.8.15 OpenSSL/0.9.7c
    And MySQL 4.1.7-standard

  Scenario: Normal use case
    Given I access www.badstore.com
    And You can add items to purchases car

  Scenario: Static detection
    Given There are no session checks
    When I look at the code in the server in the path:
    """
    /usr/local/apache/cgi-bin/test.cgi
    """
    Then the code is this:
    """
    001 #!/usr/bin/perl -w
    002 # Test Script to verify execution of CGI scripts
    003 use CGI::Carp qw(fatalsToBrowser);
    004 use CGI qw(:standard :html3);
    005 use Digest::MD5 qw(md5_hex);
    006 use MIME::Base64;
    007 print "Content-Type: text/html\n\n";
    008 $time=time;
    009 print "<html><head>\n";
    010 print "<title>CGI ScriptTest.CGI Execution Successful</title></head>\n";
    011 print "Session ID Test:  ",$time,p,
    012 "Base64 Encoding:  ",encode_base64("secret"),p,
    013 "MD5 Hash:  ",md5_hex("secret"),p,p,
    014 "<body><p>This concludes our test....</body></html>\n";
    """
    Then I can conclude It is necessary to add validations

  Scenario: Dynamic detection
    Given Show credentials in path of web Aplication
    When run gobuster for found new paths
    And gobuster found 2 new paths en the server apache:
    """
    www.badstore.com/cgi-bin/test.cgi
    http://www.badstore.com/cgi-bin/switch.cgi
    """
    Then I can see sensible data in the first path test.cgi
    """
    Session ID Test: 1553599148
    Base64 Encoding: c2VjcmV0
    MD5 Hash: 5ebe2294ecd0e0f08eab7690d2a6ee69
    This concludes our test....
    """
    Then I can conclude that there many bad practices of programming

  Scenario: Exploitation
    Given Once get password
    Then Input password admin
    And Log in success
    Then the evidence [evidence](image1.png)

  Scenario: Remediation
    Given I have patched the code by doing
    """
    016  ### Read CartID Cookie ###
    017  $ctemp=cookie('CartID');
    018  @c_cookievalue=split(":", ("$ctemp"));
    019  $id=shift(@c_cookievalue);
    020  $items=shift(@c_cookievalue);
    021  $cost=shift(@c_cookievalue);
    022  $price='$' . sprintf("%.2f", $cost);
    023
    024  ### Read SSOid Cookie ###
    025  $stemp=cookie('SSOid');
    026  $stemp=decode_base64($stemp);
    027  @s_cookievalue=split(":", ("$stemp"));
    028  $email=shift(@s_cookievalue);
    029  $passwd=shift(@s_cookievalue);
    030  $fullname=shift(@s_cookievalue);
    031  if ($fullname eq 'Master System Administrator') {
    032    print "Content-Type: text/html\n\n";
    033    $time=time;
    034    print "<html><head>\n";
    035    print "<title>CGI Script Test Execution Successful</title></head>\n";
    036    print "Session ID Test:  ",$time,p,
    037    "Base64 Encoding:  ",encode_base64("secret"),p,
    038    "MD5 Hash:  ",md5_hex("secret"),p,p,
    039    "<body><p>This concludes our test....</body></html>\n";
    040  }
    041  else{
    042    print "There are no permits";
    043  }
    """
    Then If I re-run with curl
    """
    $ curl http://www.badstore.com/cgi-bin/test.cgi
    """
    Then I get:
    """
    $ There are no permits
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
    8.2/10 AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 RC:R/
    Environmental: Unique and relevant attributes to a specific user environment
    4.6/10 (CR:L/IR:L/AR:L/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    Given No correlations have been found to this date 2019-03-31