## Version 1.4.1
## language: en

Feature: An accessible file contains the admin credential
   TOE:
    It's October: 1
  Category:
    Exposure of Sensitive Information to an Unauthorized Actor
  Location:
    http://toe-ip:8080/mynote.txt
  CWE:
    CWE-538: Insertion of Sensitive Information into Externally-Accessible
             File or Directory
  Rule:
    REQ.176: https://fluidattacks.com/web/rules/176/
  Goal:
    Get the root flag of the target.
  Recommendation:
    Don't allow external access to sensitive data

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
    | Dirb            | 2.22              |
    | Nmap            | 7.60              |
  TOE information:
    Given VirtualBox is running the VM
    And it gave me its ip

  Scenario: Normal use case
    Given the web [page](page.png)
    Then I checked all page looking for something extrange
    But It's just a normal example for a landing page

  Scenario: Static detection
    Given there isn't a source code
    Then there isn't a static detection scenario either

  Scenario: Dynamic detection
    Given the ip of the TOE
    When I scanned it with the following command
    """
    $ nmap -v -p- toe-ip
    """
    And it returned me the information below
    """
    Starting Nmap 7.60 ( https://nmap.org ) at 2020-04-08 19:39 -05
    Initiating Ping Scan at 19:39
    Scanning toe-ip [2 ports]
    Completed Ping Scan at 19:39, 0.00s elapsed (1 total hosts)
    Initiating Parallel DNS resolution of 1 host. at 19:39
    Completed Parallel DNS resolution of 1 host. at 19:39, 0.38s elapsed
    Initiating Connect Scan at 19:39
    Scanning toe-ip [65535 ports]
    Discovered open port 8080/tcp on toe-ip
    Discovered open port 80/tcp on toe-ip
    Discovered open port 22/tcp on toe-ip
    Discovered open port 3306/tcp on toe-ip
    Completed Connect Scan at 19:39, 1.00s elapsed (65535 total ports)
    Nmap scan report for toe-ip
    Host is up (0.00012s latency).
    Not shown: 65531 closed ports
    PORT     STATE SERVICE
    22/tcp   open  ssh
    80/tcp   open  http
    3306/tcp open  mysql
    8080/tcp open  http-proxy

    Read data files from: /usr/bin/../share/nmap
    Nmap done: 1 IP address (1 host up) scanned in 1.46 seconds
    """
    Then I checked the other port in Chrome
    """
    http://toe-ip:8080/
    """
    And it showed me a [image](notes.png)
    But it isn't interesting at all
    And I checked the source code looking for some hints
    """
    <!DOCTYPE html>
    <html>
    <head>
      <title>My Note</title>
    </head>
    <body>
      <img src="mynote.jpg" width="100%">
      <!--
        <a href="mynote.txt">My Note</a>
      -->
    </body>
    </html>
    """
    When I tried to see mynote.txt
    """
    http://toe-ip:8080/mynote.txt
    """
    And it contains
    """
    user      - admin
    password  - adminadmin2
    """
    Then it looks like I found something useful

  Scenario: Exploitation
    Given the admin credential
    Then I tried to connect with TOE using ssh
    But it rejected the connection
    And it happend again when I tried to connect MySQL SERVICE
    When I use the command below to get more information of this web page
    """
    $ dirb http://toe-ip/
    """
    And I got this
    """
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Wed Apr  8 17:25:21 2020
    URL_BASE: http://toe-ip/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://toe-ip/ ----
    + http://toe-ip/0 (CODE:200|SIZE:9908)
    + http://toe-ip/backend (CODE:302|SIZE:410)
    ==> DIRECTORY: http://toe-ip/config/
    + http://toe-ip/index.php (CODE:200|SIZE:9908)
    ==> DIRECTORY: http://toe-ip/modules/
    ==> DIRECTORY: http://toe-ip/plugins/
    + http://toe-ip/server-status (CODE:403|SIZE:278)
    ==> DIRECTORY: http://toe-ip/storage/
    ==> DIRECTORY: http://toe-ip/themes/
    ==> DIRECTORY: http://toe-ip/vendor/

    ---- Entering directory: http://toe-ip/config/ ----

    ---- Entering directory: http://toe-ip/modules/ ----
    ==> DIRECTORY: http://toe-ip/modules/backend/
    ==> DIRECTORY: http://toe-ip/modules/cms/
    ==> DIRECTORY: http://toe-ip/modules/system/

    ___THE OUTPUT CONTINUES___
    """
    Then I checked each link that gave me a response
    And "http://toe-ip/0" was just the same normal page
    But "http://toe-ip/backend" redirected me to
    """
    http://toe-ip/backend/backend/auth/signin
    """
    And I tried the credential I got from mynote.txt
    And I entered to an [admin panel](admin.png)

  Scenario: Remediation
    Given I moved the file out of the service accessible zone
    Then it's not accessible anymore for any external visitor

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.8/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.6/10 (High)     - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-08
