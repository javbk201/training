## Version 1.4.1
## language: en

Feature:
  TOE:
    Security Tweets
  Category:
    Cross Site Scripting
  Location:
    http://testhtml5.vulnweb.com/#/popular - Username (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
      https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross site scripting
  Recommendation:
    Validate input data from users

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10              |
    | Mozilla Firefox   | 69.0.3 (64 bit) |
  TOE information:
    Given I access the main page
    And entered a HTML5 website with some news from twitter
    And the page uses Angular 1.0.6
    And Twitter Bootstrap 2.3.1

  Scenario: Normal use case
    When I click on the login button
    Then a login form is displayed
    And Username and Password are displayed as fields

  Scenario: Static detection
    When I check the source code
    Then I can see the Username field is inside of a form
    """
    <form class="modal-body" action="/login" method="POST" id="loginForm">
    """
    And I try to access the "/login" page
    """
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
    <title>405 Method Not Allowed</title>
    <h1>Method Not Allowed</h1>
    <p>The method GET is not allowed for the requested URL.</p>
    """
    And I can't check the actual code for "/login" page

  Scenario: Dynamic detection
    Given I access the login form from the website
    Then I write a simple HTML message in the Username field
    """
    <b>This is a test</b>
    """
    When I submit the login form
    Then I can see the message as my username [evidence](evidence.png)
    And the site is not validating user input

  Scenario: Exploitation
    Given I access the login form from the website
    When I write a javascript code in the user field
    """
    <script>alert("This is an XSS Attack")</script>
    """
    When I submit the login form
    Then the page displays the above message [evidence](evidence2.jpg)
    And the page displays the same alert when the page is reloaded
    And It is possible to exploit the page via stored XSS

  Scenario: Remediation
    Given the login form at line 112
     """
    <form class="modal-body" action="/login" method="POST" id="loginForm">
    """
    When the action method is implemented
    Then it must sanitize inputs for login and password fields
    And the inputs should be escaped
    Then the XSS attacks can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-10-25
