## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Time-Based-Double-Quotes
  Location:
    http://localhost/sqlilabs/Less-10/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 7               |
    | Chrome            | 78.0.3904.70    |
    | Wamp server       | 3.1.9 32 bit    |
    | Burp Suite        | 2.1.04          |
    | Crunch            | 1.0 Win Version |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    When I access the page
    Then a message is displayed at the center of the screen
    """
    Please input the ID as parameter with numeric value
    """
    When I enter the ID parameter as "?id=1"
    Then a new message appears with a login name and a password
    """
    You are in...........
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    sqlilabs\Less-10\index.php
    """
    When I check the source code
    Then I find a query related to the id parameter
    """
    $id = '"'.$id.'"';
    $sql="SELECT * FROM users WHERE id=$id LIMIT 0,1";
    $result=mysqli_query($con, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    """
    And the "$id" variable is enclosed between double quotes
    And the variable "$sql" is storing a query string
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given the page encodes URL special characters
    When I test double quotes (") injections for the id in the URL
    Then I cannot see any changes in the website
    When I check the website using Burp Suite
    Then I enter a valid id in the address
    And I intercept the request to the server
    When I send the request to the burp repeater
    Then I enter double quotes (") as the id parameter in the text area
    And I can see a slight change in the html response
    And I realize that this change may be related to an error
    When I enter a simple time based injection in the id
    """
    ?id=1"%20and%20sleep(10)--+
    """
    Then I find out a delay in the server response
    And the site is vulnerable to SQL Injection

  Scenario: Exploitation
    Given I use Burp Suite to make injections on the website
    When I am guessing the database's name length
    Then I use a MySQL time command as the true route of a conditional
    """
    if(length(database())='1',sleep(10),1)
    """
    When I want to automate the db name guessing
    Then I send the original request to burp intruder
    And I write a SQL injection as the id value
    """
    ?id=1"%20and%20if(length(database())=<value>,sleep(10),1)--+
    """
    When I add the database length as the payload variable
    Then I load a .txt file with 50 numbers as the payload option
    When I start an attack using the above configuration
    Then a new window shows the results for every payload
    And I find out that the value "8" has a delay in the response
    And the length parameter is different [evidence](img.png)
    When I test the same sql injection in the burp repeater
    Then the response is taking too much time
    When I check the response time in the bottom of the window
    Then I can see that the response took 10 seconds [evidence](img2.png)
    And I conclude that the database name has 8 characters
    When I want to guess every character for the database name
    Then I use a substring function as the condition in the payload
    """
    if(substring(database(),1,1)=<value>,sleep(10),1)
    """
    When I add the first character value as the payload
    Then I use crunch to generate every alphabet character
    And I generate a new .txt file with the result
    When I start the attack using the new wordlist
    Then I get "S" as the first database character [evidence](img3.png)
    When I make the same steps for every character
    Then I find out the database name
    """
    Security
    """
    And the website is vulnerable to Blind SQL Injection

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-12-10
