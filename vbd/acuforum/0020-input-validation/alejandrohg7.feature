## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper input validation
  Location:
    https://testasp.vulnweb.com/showforum.asp?id=0 - title (field)
  CWE:
    CWE-20: Improper input validation
    https://cwe.mitre.org/data/definitions/20.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit improper input validation in the forum form
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com
    And I enter an "ASP.NET" site that allows me to login
    And I can submit a post through a form
    And the server is running Microsoft SQL Server version 8.5
    And "ASP.NET"

  Scenario: Normal use case
    When I enter some values in the title and body field
    Then I can submit those values
    And I can post

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I use special characters in the title field
    Then I can post those characters without client-side validation
    """
    <></>
    """
    And I can conclude that it can be affected by improper input validation

  Scenario: Exploitation
    When I use some HTML tags in the title field
    Then I can post an HTML injection
    """
    <h1>Hello, "+user+"</h1>
    """
    And I can see my post in "h1" tag [evidence](img.png)
    And I can conclude that it can be affected by improper input validation

  Scenario: Remediation
    Given the site is using an insecure form against cross site scripting
    When I input a value it must be inserted only if it is trusted Data
    And It must escape all special characters
    And It must escape all untrusted data based on the body,js,CSS and url
    And It must use White-List input validation
    And It must use OWASP auto-sanitization library
    And It must use OWASP content security policy
    And It must use output validation

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    vbd/acuforum/0352-stored-csrf-flaws
      Given I can create an HTML tag in the forum page
      And I can create an HTML tag provided by 0352-stored-csrf-flaws
      When someone visits the forum page
      Then I can log out everyone who visits that page
