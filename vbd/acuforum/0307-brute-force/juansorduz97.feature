## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Brute force
  Location:
    http://testasp.vulnweb.com/Login.asp
  CWE:
    CWE-307: improper restriction of excessive authentication attempts
  Rule:
    REQ.141: https://fluidattacks.com/web/rules/141/
  Goal:
    Get username and password by brute force
  Recommendation:
    Implement restriction to multiple login attempts

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 16.04.6 LTS     |
    | Firefox           | 71.04           |
    | Burp Suite        | 2.1.07          |
  TOE information:
    Given I am accessing the site
    And I check the site with https://builtwith.com
    When I examine the site technologies
    Then I see that framework is ASP.NET
    And Web server is Microsoft IIS Server solution

  Scenario: Normal use case
    Given the site has username and password fields
    When I try to login with a random username and password
    Then I get "Invalid login!" message [evidence](invalidlogin.png)

  Scenario: Static detection
    Given I do not have access to the source code
    When I try a static detection
    Then I realize a static detection is not possible

  Scenario: Dynamic detection
  Detecting brute force vulnerability
    Given I only get "Invalid login!" for a failed login attempt
    When I login into the site with random information multiple times
    Then I receive "Invalid login!" message every time
    Given There is not multiple login attempts restriction
    When A user introduce invalid information multiple times
    Then the site is susceptible to brute force attack

  Scenario: Exploitation
    Given I can login multiple times
    When I capture the login request with Burp Suite
    Then I configure a cluster bomb attack [evidence](request.png)
    And I use a basic list of common username and password
    """
    root
    admin
    test
    guest
    info
    adm
    mysql
    user
    administrator
    oracle
    ftp
    pi
    puppet
    ansible
    ec2-user
    vagrant
    azureuser
    """
    When I see the attack results
    Then I identify that "test"-"test" combination length is different
    When I use the combination "test"-"test" in the login page
    Then I can login [evidence](loginsuccess.png)

  Scenario: Remediation
    Given The site is susceptible to brute force attack
    When I look for a solution for the specific framework on the web
    Then I find that a solution for ASP.net framework already exist
    When I search in https://docs.microsoft.com/en-us/dotnet/
    Then I find a specific configuration in the "system.web" section
    """
    <membership defaultProvider="SqlProvider"
      userIsOnlineTimeWindow = "20>
      <providers>
        <add
          name="SqlProvider"
          type="System.Web.Security.SqlMembershipProvider"
          connectionStringName="SqlServices"
          requiresQuestionAndAnswer="true"
          maxInvalidPasswordAttempts="5"
          passwordAttemptWindow="30"
          applicationName="MyApplication" />
      </providers>
    </membership>
    """
    When The field "maxInvalidPasswordAttempts" is set with an integer
    Then An attacker can not login with invalid information multiple times
    And The vulnerability should be patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.1/10 (High) - E:H/RL:O/RC:X
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2020-01-13
