## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    Authorize Actors
  Location:
    http://localhost:3000/users/10/work_info - URL
  CWE:
    CWE-285: Improper Authorization
  Rule:
    REQ.096: https://fluidattacks.com/web/rules/096/
    REQ.097: https://fluidattacks.com/web/rules/097/
    REQ.300: https://fluidattacks.com/web/rules/300/
  Goal:
    Access other user's work info.
  Recommendation:
    Restrict access by validating current user's ID.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 52.9.0      |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I log in
    Then I can click on Work Info
    And I can see my information.

  Scenario: Static detection
    When I look at the work info controller in "app/controllers/"
    Then I can see that it only validates if the ID exists
    And it can be found in this file from line 1 to line 10.
    """
    1 class WorkInfoController < ApplicationController
    2   def index
    3     @user = User.find_by_user_id(params[:user_id])
    4     if !(@user) || @user.admin
    5       flash[:error] = "Sorry, no user with that user id exists"
    6       redirect_to home_dashboard_index_path
    7     end
    8   end
    9 end
    """
    Then I can conclude that I can change the ID in the URL

  Scenario: Dynamic detection
    Given I access http://localhost:3000
    And I log in with any user
    When I change the ID in the URL
    Then I see it redirects to the new URL
    And I can conclude that the app does not validate IDs.

  Scenario: Exploitation
    Given the app does not validate the URL ID
    When I change http://localhost:3000/users/10/work_info from /10 to /11
    Then it redirects me to user 11's work info [evidence](redir-usr-11.png)
    And I can conclude that work info can be accessed by unauthorized users.

  Scenario: Remediation
    Given I have patched the code by adding an aditional validation
    And added the following to the work_info_controller.rb from line 7 to 13
    """
    7  else
    8    unless current_user.id == @user.id
    9      flash[:error] = "Sorry, you are not authorized"
    10     redirect_to home_dashboard_index_path
    11     return
    12   end
    13 end
    """
    When I try to access user 11's work information
    Then I get an error message [evidence](no-redir.png)
    And I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:H/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
