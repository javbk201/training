## Version 2.0
## language: en

Feature: SQL Injection
  TOE:
    Rails-Goat
  Category:
    SQL Injection
  Location:
    http://192.168.1.5:3000/users/5/account_settings - user[user_id] (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in
    an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | BurpSuite       | 1.7.36-56   |
    | Sqlmap          | 1.3.4       |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running on Rails 6.0

  Scenario: Normal use case
    Given The form
    When An user changes the fields
    Then The server updates the user information
    And It is stored in the database

  Scenario: Static detection
    Given The source code
    When I see query to the database
    """
    user = User.where("user_id = '#{params[:user][:user_id]}'")[0]
    """
    Then I see a direct concatenation of user input
    And I have an SQLi vulnerability

  Scenario: Dynamic detection
    Given The form
    When I use "sqlmap" with one of my request
    Then I get a message saying that one of my parameters is vulnerable
    """
    POST parameter 'user[user_id]' is vulnerable.
    Do you want to keep testing the others (if any)? [y/N] N
    """
    And I can continue with my exploitation

  Scenario: Exploitation
    Given The vulnerability
    When I intercept one of my queries with BurpSuite
    Then I can save the request inside a file "request.req"
    When I use "sqlmap"
    """
    $ sqlmap -r request.req --current-db
    """
    Then I get the current database
    When I run another sqlmap command
    """
    $ sqlmap -r request.req -D None --tables
    """
    Then I can get all tables inside the database [evidence](image1.png)

  Scenario: Remediation
    Given The source code
    When I find that the problem is the direct concatenation
    Then I update the query to use parametized queries
    """
    user = User.find(:first, :conditions => ["user_id = ?",
    params[:user][:user_id]])
    """
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-30
