# Version 1.4.1
## language: en

Feature:
  TOE:
    xvwa
  Location:
    http://localhost/xvwa/vulnerabilities/crypto/
  CWE:
    CWE-261: Weak Cryptography for Passwords
  Rule:
    REQ.134: Store passwords with salt
  Goal:
    Generate proper an input to get a crytographic hashed password
  Recommendation:
    Store passwords using password hashing function
    instead of cryptographic hash functions like md5
    SHA256

  Background:
  Hacker's software:
    | <software name>      | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 74.0.3729.131  |
  TOE information:
    Given I'm entering the site http://localhost/xvwa
    And I access a site which has been build in php and uses sql

  Scenario: Normal use case
    Given I enter http://localhost/xvwa/vulnerabilities/crypto/
    And I write something in the Enter your text input
    Then I saw a encoded text

  Scenario: Static detection
    Given I enter http://localhost/xvwa/vulnerabilities/crypto/
    And I have access to the machine
    Then I comprehend that the path is at URL
    And I go the path and see home.php in the crypto directory
    Then I saw the vunerable code part that display the data
    """
    37 <?php
    38
    39   $str = $_GET['item'];
    40      if($str){
    41
    42       echo "<table style=\"border-collapse:collapse; table-layout:fixed;
            width:700px;\">";
    43       echo "<tr><th width>Crypto Used</th><th>Value</th></tr>";
    44       # --- ENCODING ---
    45       echo "<tr><td style=\"word-wrap:break-word;\">Base64 Encode</td>";
    46       echo "<td style=\"word-wrap:break-word;\">".
             base64_encode($str)."</td></tr>";
    47       echo "<tr><td colspan=2>&nbsp;&nbsp;</td></tr>";
    48
    49       # --- ENCRYPTION ---
    50       echo "<tr><td style=\"word-wrap:break-word;\">AES Encryption<br>";
    51
    52       $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5
             e1d417e2ffb2a00a3");
    53
    54       $key_size =  strlen($key);
    55       echo "Key Size : " . $key_size. "</td>";
    56
    """
    And I see the way the password is handled is not adequate

  Scenario: Dynamic detection
    Given I enter http://localhost/xvwa/vulnerabilities/crypto/
    And I write a text admin
    Then it show me a page with resuls
    """
    Crypto Used Value
    Base64 Encode YWRtaW4=

    AES Encryption
    Key Size : 32
    """
    Then I came to the conclusion that encoding is base64
    And is not apropiate for handling passwords
    Then it can lead to data disclosure

  Scenario: Exploitation
    Retrieve the information in very easy manner
    Given the base64 enconding
    Then we can decode the password
    And with php base64_decode(YWRtaW4=)
    Then see the string in this case admin

  Scenario: Remediation
    When the page was develop it should handle the data in a proper manner
    And it must hide this variables
    And if the purpose was to show it
    Then it must make very hard to get data by the hacker
    And the way to solve the issue is with this code
    """
    $str = password_hash($str,PASSWORD_BCRYPT);
    """
    Then it solves the issue because the hacker can't guess the password easily
    And because it is a password hashing function introduces a random salt
    And that makes harder to guest since can withstand brute force attacks
    And also resist loop and rainbow table attacks

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.9/10 (High) - AV:L/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.7/10 (Medium) - E:F/RL:O/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    6.2/10 (Medium) - CR:H/IR:M/AR:Me

  Scenario: Correlations
    No correlations have been discovered to this date 2019-05-13
