## Version 1.4.1
## language: en

Feature:
  TOE:
    Firing Range
  Category:
    Injection Flaws
  Location:
    firing-Range/angular/angular_form_parse/1.6.0 - Form
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute arbitrary JS
  Recommendation:
    Sanitize user input, don't use $parse

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am accessing Firing Range at
    """
    https://public-firing-range.appspot.com
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to https://firing-range/angular/angular_form_parse/1.6.0
    Then I send a query
    And I see it reflected

  Scenario: Static detection
  No input sanitization
    When I look at the page source
    """
    01  <!DOCTYPE html>
    02  <title>Angular Form Parse</title>
    03  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.js"
    ></script>
    04  <script>
    05    angular.module('test', [])
    06        .controller('VulnerableController', ['$scope', '$parse', function(
      $scope, $parse) {
    07          $scope.submit = function() {
    08            var expression = $scope.expression.trim();
    09            if (expression) {
    10              $parse(expression)({});
    11            }
    12          };
    13        }]);
    14  </script>
    15  <form ng-app="test" ng-submit="submit()" ng-controller="VulnerableContro
    ller">
    16    <input ng-model="expression" placeholder="Angular Payload goes in here
    ...">
    17    <input type="submit" value="Parse">
    18  </form>
    """
    Then I see it's parsing whatever value I input with angular and executing it

  Scenario: Dynamic detection
  Angular fuzzing
    Given I send a query with an Angular XSS Payload
    """
    constructor.constructor('alert(1)')()
    """
    Then I get an alert
    Then the page is vulnerable to Angular code injection

  Scenario: Exploitation
  There is no way to meaningfully exploit vulnerabilities in this ToE

  Scenario: Remediation
  Sanitize input
    Given I don't use $parse ever
    Then code can't be injected anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.7/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.5/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-17
