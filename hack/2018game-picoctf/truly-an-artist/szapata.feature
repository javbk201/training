## Version 2.0
## language: en

Feature: truly-an-artist - Forensics - 2018game-picoctf
  """
  Can you help us find the flag in this Meta-Material? You
   can also find the file in
   /problems/truly-an-artist_2_61a3ed7216130ab1c2b2872eeda81348.
  """
  Site:
    2018game-picoctf
  Category:
    Forensics
  User:
    chalimbu
  Goal:
    get the flag in the image

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Google Chrome   | 76.0.3809   |
    | GNU strings     | 2.30        |
    | exiftool        | 10.30       |
    | grep(GNU grep)  | 3.1         |

  Machine information:
    Given I Downloaded the image from the Site
    """
    https://2018shell.picoctf.com
    /static/a386ed9a7534702173762cf536dce121/2018.png
    """
    Then I have the image in my /Downloads folder
    And the console allows me to execute commands over the image.

  Scenario: Success: Strings in file
  """
  In order to see non-encrypted strings in the image, first i test strings
  """
    Given I am logged in the machine
    When I am in the Downloads/ where the image is
    And The image is called 2018.png
    And The flag has the format picoCTF{anyString}
    Then I can run the command
    """
    strings 2018.png | grep pico
    """
    And I did work
    And I capture the flag

  Scenario: Success: Metadata in file
    Given The challenge says meta-material
    And Meta could refer to the metadata in the image
    Then I look a the metadata in the image with the command
    """
    exiftool 2018.png
    """
    Then I look for the flag
    And I found it in the attribute artist
    And I test the flag in the field on the website
    And it works
