## Version 2.0
## language: en

Feature: flaskcards-skeleton-key -web-explotation -2018game-picoctf
  Site:
    2018game-picoctf
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get admin access

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: login form injection
    Given there is a login form in the challenge page
    When I try a sample injection "{{1+1}}" in the login form
    Then there is not result
    Then I can not get the flag

  Scenario: Fail: name cookie manipulation
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can see the name of the cookies stored in my browser
    And I change the name of the cookie called "session" to "admin"
    And I can not get the flag

  Scenario: Fail: value cookie manipulation
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can see the value of the cookies stored in my browser
    """
    eyJfZnJlc2giOmZhbHNlLCJjc3JmX3Rva2VuIjoiNmU0NDdjMzExYmI0NTZlZDJiNTE4O
    DQzOGEyOTAzZGZmM2E1OTg0NiJ9.EFgtmw.QRswDt8-rTv8ynbvvq0LUeUW_Bw
    """
    And I try to decode using "https://md5hashing.net/hash" getting nothing
    And I can not get the flag

  Scenario: Fail: flask value cookie manipulation ussing admin
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can see the values of the cookies stored in my browser
    """
    eyJfZnJlc2giOmZhbHNlLCJjc3JmX3Rva2VuIjoiNmU0NDdjMzExYmI0NTZlZDJiNTE4
    ODQzOGEyOTAzZGZmM2E1OTg0NiJ9.EFgtmw.QRswDt8-rTv8ynbvvq0LUeUW_Bw
    """
    And I try to decode using "www.kirsle.net/wizards/flask-session.cgi"
    """
    {
    "_fresh": true,
    "_id": "f695cae486df3c06262fa75e18872b587448f6c8bcb9566869cfd8dc9adb63
    5a2cd515dcd90823617ca153a06708a9a796991fe90ae5ae4b5e1247d0c97f9e8a",
    "csrf_token": "6e447c311bb456ed2b5188438a2903dff3a59846",
    "user_id": "22"
    }
    """
    And I change the "user_id" to "admin"
    """
    SECRET_KEY = 'a155eb4e1743baef085ff6ecfed943f2'
    COOKIES_TO_ENCODE = {
    "_fresh": True,
    "_id": "f695cae486df3c06262fa75e18872b587448f6c8bcb9566869cfd8dc9adb63
    5a2cd515dcd90823617ca153a06708a9a796991fe90ae5ae4b5e1247d0c97f9e8a",
    "csrf_token": "434995661d22d4bd5ef8872bd7022374944f2365",
    "user_id": "admin"
    }
    """
    And I encode it using "SECRET_KEY" and custom code (evidence)[encoder.py]
    """
    .eJwlj8ttAzEMBXvR2QeJEn9uZkGRImwEcYBd-xSk92ziAt7MvO-y5b6OW7k-99e6lO0e5
    VqSFN3WEIrsXgkI0hhXE2GYKDyGJLlMn4pEQuoZEq4WkzoaeGDD8NAq0KmxW8NulbiKqbG
    Sasul1RaemnmSYXBUV05dYuVS_Nhze359rMfZM_rQP1ELgBgzcOV_SXAF6Dx0jDw9eO5ex
    9rfJyw-74_y8wuZFj_y.XXa_FA.YSt27oVUXnpxDday6zkkYDaBUDE
    """
    And I change the value of the cookie in the challenge page
    And I can not get the flag

  Scenario: Success: flask value cookie manipulation ussing 01
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can see the values of the cookies stored in my browser
    """
    eyJfZnJlc2giOmZhbHNlLCJjc3JmX3Rva2VuIjoiNmU0NDdjMzExYmI0NTZlZDJiNTE4OD
    QzOGEyOTAzZGZmM2E1OTg0NiJ9.EFgtmw.QRswDt8-rTv8ynbvvq0LUeUW_Bw
    """
    And I try to decode using "www.kirsle.net/wizards/flask-session.cgi"
    """
    {
    "_fresh": true,
    "_id": "f695cae486df3c06262fa75e18872b587448f6c8bcb9566869cfd8dc9adb63
    5a2cd515dcd90823617ca153a06708a9a796991fe90ae5ae4b5e1247d0c97f9e8a",
    "csrf_token": "6e447c311bb456ed2b5188438a2903dff3a59846",
    "user_id": "22"
    }
    """
    And I change the "user_id" to "01" guessing it could be the admin
    """
    SECRET_KEY = 'a155eb4e1743baef085ff6ecfed943f2'
    COOKIES_TO_ENCODE = {
    "_fresh": True,
    "_id": "f695cae486df3c06262fa75e18872b587448f6c8bcb9566869cfd8dc9adb63
    5a2cd515dcd90823617ca153a06708a9a796991fe90ae5ae4b5e1247d0c97f9e8a",
    "csrf_token": "434995661d22d4bd5ef8872bd7022374944f2365",
    "user_id": "01"
    }
    """
    And I encode it using "SECRET_KEY" and custom code (evidence)[encoder.py]
    """
    .eJwlj0tOBDEMBe-S9SwSJ_7NZVqOHQuEBFL3zApx9wlwgFdV77scea7rrdwf53PdyvEe5
    V6SFN3WEIrsXgkI0hhXE2GYKDyGJLlMn4pEQuoZEq4WkzoaeGDD8NAq0KmxW8NulbiKqbG
    Sasul1RZuzdxkGBzVlVOXWLkVv848Hl8f63P3jD70V9QCIMYMXPlXElwBOg8dI7cH9-55r
    fP_RG3l5wXYbj5K.XXbF-Q.t1S78j2b9ufqVDW1H_W4sE8iqdY
    """
    And I change the value of the cookie in the challenge page
    And I get the flag
