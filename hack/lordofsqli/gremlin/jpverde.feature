## Version 2.0
## language: en

Feature: gremlin-sqli-lord-of-sqlinjection
  Site:
    lord-of-sqlinjection
  User:
    jpverde
  Goal:
    Modify the main query

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given A webpage
    """
    https://los.rubiya.kr/chall/gremlin_280c5552de8b681110e9287421b834fd.php
    """
    And The webpage shows the php code with the query function

  Scenario: Success:sql-injection
    Given I am looking at the code shown
    And I can see the function recieves the 'id' and 'pw' values by Get method
    When I type this in the url trying o inject sql "?id=' or 1=1--+"
    Then I get this response
    """
    GREMLIN Clear!
    """
    And And I completed the challenge
