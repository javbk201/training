## Version 2.0
## language: en

Feature: giant-sql-injection-lordofsqli
  Site:
    LORD OF SQLINJECTION
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | Windows         | 10 Pro              |
    | Firefox         | 74.0 (64-bit)       |
  Machine information:
    Given the challenge URL
    """
      https://los.rubiya.kr/chall/giant_18a08c3be1d1753de0cb157703f75a5e.php
    """
    When I open the URL with Chrome
    Then I see the PHP code that validates the user id
    And It shows the query made on the screen
    """
      select 1234 fromprob_giant where 1
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      GIANT Clear!
    """
    When I try with payload:
    """
      ?shit=1
    """
    Then I get nothing
    And I din't solve it

  Scenario: Success:Sqli-asciichar-technique
    Given I inspect the code
    And I see that they validate certain characters
    And the length of the input
    """
      if(strlen($_GET[shit])>1) exit("No Hack ~_~");
      if(preg_match('/ |\n|\r|\t/i', $_GET[shit])) exit("HeHe");
    """
    When I use the vertical tab ascii char
    """
      ?shit=%0B
    """
    Then I solve the challenge
