## Version 2.0
## language: en

Feature: Morse-Code - Cryptography - ctflearn
  Site:
    https://ctflearn.com/problems/309
  User:
    adrianfcn (ctflearn)
  Goal:
    Generate a secret code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have the message
    """
    ..-. .-.. .- --. ... .- -- ..- . .-.. -- --- .-. ... . .. ... -.-. --- ---
    .-.. -... -.-- - .... . .-- .- -.-- .. .-.. .. -.- . -.-. .... . . ...
    """

  Scenario: Success: code morse decode
    Given I need to decode the message
    """
    ..-. .-.. .- --. ... .- -- ..- . .-.. -- --- .-. ... . .. ... -.-. --- ---
    .-.. -... -.-- - .... . .-- .- -.-- .. .-.. .. -.- . -.-. .... . . ...
    """
    And I know that this use code morse
    And I know a tool online to decode morse code
    """
    https://www.dcode.fr/morse-code
    """
    When I use the tool onlie
    Then I decode the message <flag>
    And I solve the challenge
