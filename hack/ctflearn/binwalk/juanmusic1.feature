## Version 2.0
## language: en

Feature: Binwalk - Forensics - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Forensics
  User:
    JuanMusic1
  Goal:
    Find the flag

  Background:
  Hacker's software:
    |   Ubuntu    |  18.04.3 |
    |   Firefox   |  70.0.1  |
    |   Binwalk   |  2.2.0   |
  Machine information:
    Given A link in the challenge page
    """
    https://ctflearn.com/challenge/108
    """
    And the challenge has an image with a file inside
    And the challenge's title is Binwalk

  Scenario: Fail: Get strings inside the image
    When I check image file
    Then I can see it can't be opened like a normal image
    And I think this should have strings inside with the flag
    When I use the strings command in terminal
    """
    $ strings PurpleThing.jpeg
    """
    Then I get some strings
    """
    A$emB
    A$emB
    IEND
    """
    And I couldn't get the flag

  Scenario: Success: Analysing the image and get the file inside
    When I check the challenge's title
    Then I find out that Binwalk means a forensics software
    And I think the flag can be extracted by this software
    When I want to examine the image
    Then I execute the file command with the image
    """
    $ file PurpleThing.jpeg

    juanmusic1/PurpleThing.jpeg: PNG image data, 780 x 720, 8-bit/color RGBA,
    non-interlaced
    """
    And I change the file extension with mv command to PNG
    When I try to open the image
    Then I see a purple ghost
    When I check with the Binwalk software
    Then I find out another PNG image inside the file
    """
    $ binwalk PurpleThing.png

    DECIMAL   HEXADECIMAL    DESCRIPTION
    ---------------------------------------------------------------
    0         0x0            PNG image, 780 x 720, 8-bit/color RGBA,
    non-interlaced
    41        0x29           Zlib compressed data, best compression
    153493    0x25795        PNG image, 802 x 118, 8-bit/color RGBA,
    non-interlaced
    """
    When I try to extract the other image with the software
    """
    binwalk -dd='.*' <file.png>
    """
    Then I use "ls" in the console
    And I can see a new folder named "_PurpleThing.png.extracted"
    """
    $ ls
    PurpleThing.jpeg  PurpleThing.png  _PurpleThing.png.extracted
    """
    When I open the explorer in this folder
    Then I see a file as an image
    And I open this file
    Then I see the flag "ABCTF{<FLAG>}"
    When I put the flag in the flag format
    """
    CTFlearn{<FLAG>}
    """
    Then I solve the challenge
