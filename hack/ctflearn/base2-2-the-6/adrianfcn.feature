## Version 2.0
## language: en

Feature: base-2-2-The-6 - Cryptography - ctflearn
  Site:
    https://ctflearn.com/problems/192
  User:
    adrianfcn (ctflearn)
  Goal:
    Decode the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
    | base64          | 8.30        |
  Machine information:
    Given I have the message "Q1RGe0ZsYWdneVdhZ2d5UmFnZ3l9"

  Scenario: Success: base64 decode
    Given I need to decode the message "Q1RGe0ZsYWdneVdhZ2d5U"
    And the name of challenge is "base 2 2 the 6"
    When I do "2^6 = 2*2*2*2*2*2 = 64"
    Then I think the message is encoded in base64
    When I use the command
    """
    $ message=Q1RGe0ZsYWdneVdhZ2d5U
    $ echo $message|base64 -d
    """
    Then I decode the message <flag>
    And I solve the challenge
