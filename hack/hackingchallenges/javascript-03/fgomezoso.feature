## Version 2.0
## language: en

Feature: javascript-03 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    fgomezoso
  Goal:
    Enter the correct password

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.hacking-challenges.de/index.php?page=
    hackits&kategorie=javascript&id=1
    """
    And the website is redirected right away to the homepage

  Scenario: Success:Blocking redirections
    Given the challenge page is displayed
    When I use the tamper chrome extension
    And the "Block/Rerout Requests" option is activated
    Then I block the first request
    And I check the source code of the first displayed website
    And I can see there is important info at line 159
    """
    </script>

    <!-- Das Passwort lautet: Source -->

    Herzlichen Glückwunsch! Du hast die Aufgabe schon fast geschafft!
    Jetzt musst Du nur noch versuchen das Passwort bei Lösung einzutragen!
    """
    And I find out that the password is "Source"
    And the next line is telling me to use the password in the website
    When I visit the challenge's link again
    And the tamper is still active
    Then I begin to allow safe requests until a redirection request appears
    And the URL is pointing to the homepage
    """
    http://www.hacking-challenges.de/index.php?seite=home
    """
    When I enter the password in the textbox and submit the form
    And I block the homepage redirection in first place
    Then the tamper asks me again if I want to allow the password request
    And I give access to this request [evidence](img.png)
    When a new page is displayed with a confirmation message
    """
    Das Passwort ist Richtig!
    """
    Then I solved the challenge
