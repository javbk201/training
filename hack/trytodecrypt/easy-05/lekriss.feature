## Version 1.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    Cristian Guatuzmal
  Goal:
    Discover the decrypted text

  Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=5
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    For the beginning here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    90DE633F425148DE51546CDE725466DE3F2A6936DE4263CCDEAB362A3372DE3954
    5DDE633F36DE51366F63DE545136D8
    """
    And an input format which allows me to cipher text
    And uses the same cipher pattern as the text

  Scenario: Success:finding the cipher method
    Given the web page has an input field and an Encrypt Button
    Then I start to put some values in the input field
    And I start to encrypt them
    Then I notice that each character has a hexadecimal value
    And 0 has the value "0C", which is the lower value
    And I can determine the hexadecimal of every charater
    When I sum three to the previous value
    Then I can build a table based on that order
    Then when I replace every character based on that table
    Then I obtain the following text
    """
    I think now you have it. Ready for the next one?
    """
    And I submit it as a solution
    And it is the solution
    Then I solved the challenge
