## Version 2.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    edr3mo
  Goal:
    Discover the decrypted text

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Mozilla Firefox | 66.0.2      |
    | Linux Mint      | Tessa 19.1  |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=1
    """
    Then I opened the URL with Mozilla
    And I see the challenge statement
    """
    For the beginnint here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    131017171A48221A1D170F
    """
    And I see a new statement
    """
    To give you a real chance you can here encrypt any text you want.
    This is your chance to find out how the algorythm works.
    """
    And I see an input field and one button to encrypt
    Then I put some values in the field
    And I see as result the encrypted value of the input
    Then I conclude that the input helps to find out the cypher pattern

  Scenario: Success:method-trying-values-in-the-input
    Given the web page has an input field and an Encrypt Button
    Then I try differents values to find out how the algorythm works
    And I start encrypting numbers
    Then I see a pattern in the encryption
    And I see the result for the number 0
    """
    02
    """
    Then I try with the number 3
    And I see the pattern in the result
    """
    05
    """
    Then I try with letters
    And I see the result for the letter a
    """
    0C
    """
    Then I conclude that the pattern has two digits
    And I see the first digits start in 0 and the second in 2
    And I see the second digit iterate from 0 to F
    Then the first digit changes from 0 to 1 and goes on
    And I build a table with two columns for the value and cypher
    Then I compare two digits of the crypted text with the table
    And I see that the cypher text correspond to word hello world
    Then I write hello world in the input field
    And I click the this is the solution button
    Then I solved the challenge
