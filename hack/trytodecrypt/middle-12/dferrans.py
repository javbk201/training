"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting..

"""
from __future__ import print_function
import time
from selenium import webdriver

ENCRYPTEDSTR = input()
GROUPS = []
READY = []
DRIVER = webdriver.Firefox()
DRIVER.get('https://www.trytodecrypt.com/decrypt.php?id=12#headline')
CHARMAP = [" ", "Y", "a", "e", "i", "o", "u", "e", "t", "a", "o", "i",
           "n", "s", "r", "h", "l", "d", "c", "u", "v", "m", "f", "p",
           "g", "w", "y", "b", "k", "x", "j", "q", "z", ".", ",", ";",
           ":", "!", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
           "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p",
           "q", "r", "s", "t", "v", "w", "x", "y", "z", "A", "B", "C",
           "D", "E", "F", "G", "H", "I", "J", "L", "K", "L", "M", "N",
           "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Z"]

DRIVER.get('https://www.trytodecrypt.com/decrypt.php?id=12#headline')
INPUTFIELD = DRIVER.find_elements_by_id("comment")
INPUTFIELD[0].clear()
INPUTFIELD[0].send_keys("a")
BTNCRYPT = DRIVER.find_element_by_name("encrypt")
BTNCRYPT.click()
RST = DRIVER.find_elements_by_class_name("panel-body")
CHALL_LEN = len(RST[1].text)
FIRST_CHAR = ""
RST1 = ""

for char in CHARMAP:

    time.sleep(2)
    INPUTFIELD = DRIVER.find_elements_by_id("comment")
    INPUTFIELD[0].clear()
    INPUTFIELD[0].send_keys(char)
    BTNCRYPT = DRIVER.find_element_by_name("encrypt")
    BTNCRYPT.click()
    RESULTN = DRIVER.find_elements_by_class_name("panel-body")
    STRCOMPARE = RESULTN[1].text
    if STRCOMPARE == str(ENCRYPTEDSTR[0:CHALL_LEN]):
        FIRST_CHAR += char
        break

for pairs_str in range(len(ENCRYPTEDSTR)):
    GROUPS.append(ENCRYPTEDSTR[pairs_str:pairs_str+CHALL_LEN])

for str_to_decrypt in GROUPS[0::CHALL_LEN]:
    READY.append(str_to_decrypt)

ANSWERSTR = FIRST_CHAR
COUNTER = 0

for str1 in READY[1:]:

    INPUTFIELD = DRIVER.find_elements_by_id("comment")
    INPUTFIELD[0].clear()
    INPUTFIELD[0].send_keys(ANSWERSTR + ' ')
    BTNCRYPT = DRIVER.find_element_by_name("encrypt")
    BTNCRYPT.click()
    RST1 = ""

    while str1 != RST1:

        for char in CHARMAP:
            time.sleep(2)
            INPUTFIELD = DRIVER.find_elements_by_id("comment")
            INPUTFIELD[0].clear()
            INPUTFIELD[0].send_keys(ANSWERSTR + char)
            BTNCRYPT = DRIVER.find_element_by_name("encrypt")
            BTNCRYPT.click()
            RESULTN = DRIVER.find_elements_by_class_name("panel-body")
            STRCOMPARE = RESULTN[1].text
            if STRCOMPARE[-CHALL_LEN:] == str1:
                RST1 = STRCOMPARE[-CHALL_LEN:]
                ANSWERSTR += char
                break

SOLUTION = DRIVER.find_element_by_name("solutionButton")
SOLUTION.click()
DRIVER.quit()
print(SOLUTION.text)

# $python dferrans.py < DATA.lst
# cookie monster
