## Version 2.0
## language: en

Feature: 76-programming-wixxerd
  Code:
    76
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
    Insert your Pseudo into the table named "hackers"
    The column is also named "hacker"

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |

  Scenario: fail:common-query
    Given I can run select and insert queries
    When I use the next query
    """
    SELECT * FROM hackers;
    """
    Then got the table columns
    And try to use the next queries
    """
    insert into hackers(HACKER,ID) values("my-alias",randomNum);
    insert into hackers values("my-alias",sameRandomNum);
    """
    And show the next message on both
    """
    ERROR: column "my-alias" does not exist
    """
    And I could not capture the flag

  Scenario: Success:using-subquery-to-insert
    Given exists another ways to insert values
    When I use subquery as the next
    """
    insert into hackers select randomNum  a, 'my-alias' b;
    """
    Then I run the query and show the next
    """
    Element COLUMNLIST is undefined in RESULTS.
    """
    Then I insert my-alias successful
    And I run the next query to search my-alias
    """
    select HACKER from hackers where id = sameRandomNum
    """
    Then I get the next
    """
    You got it!      You now have my permission to do a little dance.
    """
    And I get the flag
