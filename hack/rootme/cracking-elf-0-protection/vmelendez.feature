# Version 1.4.0
# language: en

  Feature:
    TOE:
      Rootme
    Category:
      Cracking
    Challenge name:
      ELF - 0 protection
    CWE:
      CWE-200: Information Exposure
    Goals:
      Get the right password
    Recommendation:
      Use a binary protection mechanism

    Background:
    Hacker's software:
      | <Name>       | <Version> |
      | Gdb-peda     | 8.1.1     |
      | GNU Binutils | 2.31.1    |
    TOE:
      Given an executable linux file (ELF) who asks for a password
      And the binary is running on low security_level

    Scenario: Normal use case
    Enters the password
      Given I'm with the elf
      And I see shows a banner:
      """
      ############################################################
      ##        Bienvennue dans ce challenge de cracking        ##
      ############################################################

      Veuillez entrer le mot de passe :
      """
      Then I see a welcome message and a text to enter the password
      Given I write <password> in the console
      When I press enter key
      Then I get <results>
      | <password> | <results>                         |
      | password   | Dommage, essaye encore une fois.  |
      | test       | Dommage, essaye encore une fois.  |
      | 123456     | Dommage, essaye encore une fois.  |

    Scenario: Static detection
    The password will be compared in memory
      When I look at the disassembly code
      """
      gdb-peda$ disass main
      Dump of assembler code for function main:
         0x0804869d <+0>:     lea    ecx,[esp+0x4]
         0x080486a1 <+4>:     and    esp,0xfffffff0
         0x080486a4 <+7>:     push   DWORD PTR [ecx-0x4]
         0x080486a7 <+10>:    push   ebp
         0x080486a8 <+11>:    mov    ebp,esp
         0x080486aa <+13>:    push   ecx
         0x080486ab <+14>:    sub    esp,0x24
         0x080486ae <+17>:    mov    DWORD PTR [ebp-0x8],0x8048841
         0x080486b5 <+24>:    mov    DWORD PTR [esp],0x804884c
         0x080486bc <+31>:    call   0x80484c8 <puts@plt>
         0x080486c1 <+36>:    mov    DWORD PTR [esp],0x804888c
         0x080486c8 <+43>:    call   0x80484c8 <puts@plt>
         0x080486cd <+48>:    mov    DWORD PTR [esp],0x80488cc
         0x080486d4 <+55>:    call   0x80484c8 <puts@plt>
         0x080486d9 <+60>:    mov    DWORD PTR [esp],0x804890c
         0x080486e0 <+67>:    call   0x8048498 <printf@plt>
         0x080486e5 <+72>:    mov    eax,DWORD PTR [ebp-0xc]
         0x080486e8 <+75>:    mov    DWORD PTR [esp],eax
         0x080486eb <+78>:    call   0x80485fe <getString>
         0x080486f0 <+83>:    mov    DWORD PTR [ebp-0xc],eax
         0x080486f3 <+86>:    mov    eax,DWORD PTR [ebp-0x8]
         0x080486f6 <+89>:    mov    DWORD PTR [esp+0x4],eax
         0x080486fa <+93>:    mov    eax,DWORD PTR [ebp-0xc]
         0x080486fd <+96>:    mov    DWORD PTR [esp],eax
         0x08048700 <+99>:    call   0x80484d8 <strcmp@plt>
         0x08048705 <+104>:   test   eax,eax
         0x08048707 <+106>:   jne    0x804871e <main+129>
         0x08048709 <+108>:   mov    eax,DWORD PTR [ebp-0x8]
         0x0804870c <+111>:   mov    DWORD PTR [esp+0x4],eax
         0x08048710 <+115>:   mov    DWORD PTR [esp],0x8048930
         0x08048717 <+122>:   call   0x8048498 <printf@plt>
         0x0804871c <+127>:   jmp    0x804872a <main+141>
         0x0804871e <+129>:   mov    DWORD PTR [esp],0x8048970
         0x08048725 <+136>:   call   0x80484c8 <puts@plt>
         0x0804872a <+141>:   mov    eax,0x0
         0x0804872f <+146>:   add    esp,0x24
         0x08048732 <+149>:   pop    ecx
         0x08048733 <+150>:   pop    ebp
         0x08048734 <+151>:   lea    esp,[ecx-0x4]
         0x08048737 <+154>:   ret
      End of assembler dump.
      """
      And firstly I see that the pointer value 0x8048841 is saved in [ebp - 0x8]
      And then I see 3 "puts" functions that are used to print the banner
      And a "printf" function for the text requesting the password
      """
      gdb-peda$ x/s 0x804884c
      0x804884c:      '#' <repeats 60 times>
      gdb-peda$ x/s 0x804888c
      0x804888c: "##        Bienvennue dans ce challenge de cracking        ##"
      gdb-peda$ x/s 0x80488cc
      0x80488cc:      '#' <repeats 60 times>, "\n"
      gdb-peda$ x/s 0x804890c
      0x804890c:      "Veuillez entrer le mot de passe : "
      """
      Then I see some interesting functions like "getString" and "strcmp"
      Then I see a piece of the getString function
      And I notice that it receives a parameter associated to a variable
      And the variable is [ebp - 0xc]
      """
      0x080486e5 <+72>:    mov    eax,DWORD PTR [ebp-0xc]
      0x080486e8 <+75>:    mov    DWORD PTR [esp],eax
      0x080486eb <+78>:    call   0x80485fe <getString>
      """
      And noticing by name I suppose that from there it captures the password
      And saves it in the variable
      Then I see a strcmp function
      And which compares the string I entered with the variable [ebp - 0x8]
      """
      0x080486f3 <+86>:    mov    eax,DWORD PTR [ebp-0x8]
      0x080486f6 <+89>:    mov    DWORD PTR [esp+0x4],eax
      0x080486fa <+93>:    mov    eax,DWORD PTR [ebp-0xc]
      0x080486fd <+96>:    mov    DWORD PTR [esp],eax
      0x08048700 <+99>:    call   0x80484d8 <strcmp@plt>
      """
      And if I see what the key to which it compares
      Then I see:
      """
      gdb-peda$ x/s 0x8048841
      0x8048841:      "123456789"
      """

    Scenario: Exploitation
    Enters the correct password
      Given I write <password> in the console
      When I press enter key
      Then I get <results>
      | <password> | <results>                           |
      | 123456789  | Bien joue, vous pouvez valider      |
      |            | l'epreuve avec le pass : 123456789! |

      Then I see that I have validated the password correctly
