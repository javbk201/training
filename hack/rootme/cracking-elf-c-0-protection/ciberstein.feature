## Version 2.0
## language: en

Feature: Web server-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Find the validation password.

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | IDA             | 5.0              |
    | Python          | 2.7              |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    ELF C++ - 0 protection

    Find the validation password.
    Start the challenge
    """
    Then I press the start button
    And I download the ch25.bin file

  Scenario: Fail:Exploring inside of the file
    Given the .bin file
    Then I proceed to open file with PowerISO version 7.2
    And I saw only files encrypted
    Then I conclude that I can't use PowerISO for my purpose.

  Scenario: Success:Disassembling the file
    Given I have the IDA version 5.0 on my system
    Then I proceed to open the file with this software
    Then I saw two construction of String
    And the content of the string are in offset "unk_8048DC4" and "unk_8048DCC"
    And there is a function "plouf" which take two string [evidence](off.png)
    And i conclude that the pseudocode looks like that
    """
    string s1 = [unk_8048DC4];
    string s2 = [unk_8048DCC];
    plouf(s3,s2,s1);
    """
    Then we try to reverse the function plouf
    And the function plouf looks like that
    """
    plouf(string s3, string s2, string s1) {
      int i=0;

      while (s2[i] != 0) {
        s3[i] = s2[i] xor s1[i % len(s1)];
        i++;
      }
    }
    """
    Then I saw finally that the program use s3 to compare with the input
    Then I used s1 to decode s2, to get the right result with the python script
    """
    https://ghostbin.com/paste/xxwpu/raw
    """
    And I receive this information
    """
    Here_you_have_to_understand_a_little_C++_stuffs
    """
    Then I put as answer Here_you_have_to_understand_a_little_C++_stuffs
    And the answer is correct.
    Then I solved the challenge.
