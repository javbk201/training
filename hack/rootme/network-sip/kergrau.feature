## Version 2.0
## lenguage: en

Feature: network-sip
  Site:
    Root-me
  Category:
    Network
  User:
    synapkg

  Background:
  Hacker's software:
  | <Software name> | <Version>     |
  | Ubuntu          | 16.04 (64-bit)|
  | Mozilla Firefox | 63.0 (64-bit) |

  Scenario: Success: Simple deduction
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/SIP-authentication-103
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    Find the password used to authenticate on the SIP infrastructure.
    """
    Then I started challenge opening the follow link
    """
    http://challenge01.root-me.org/reseau/ch4/ch4.txt
    """
    Then I saw three records
    """
    172.25.105.3"172.25.105.40"555"asterisk"REGISTER"
    sip:172.25.105.40"4787f7ce""""PLAIN"1234

    172.25.105.3"172.25.105.40"555"asterisk"INVITE"
    sip:1000@172.25.105.40"70fbfdae""""MD5"aa533f6efa2b2abac675c1ee6cbde327

    172.25.105.3"172.25.105.40"555"asterisk"BYE"
    sip:1000@172.25.105.40"70fbfdae""""MD5"0b306e9db1f819dd824acf3227b60e07
    """
    And in the last part of them looks like as a type password and password.
    Then the first record have {PLAIN} as password type
    And {1234} as password.
    Then I put {1234} as answer and I was rigth.
