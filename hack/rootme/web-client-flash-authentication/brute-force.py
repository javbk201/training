"""
    Script for generating all possible combinations
    given the restrictions.
    > pylint bruteForce.py.spaghetticode
    -------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 9.31/10, +0.69)
"""
import hashlib
import replication


def get_combinations():
    """
        Get all 12 or 13 byte-length combinations
    """
    arr = {"A": "B", "1": "4", "5": "9"}
    res = ["A", "1", "5"]
    for i in range(11):
        n_arr = []
        for var_s in res:
            l_digit = var_s[-1:]
            if l_digit in arr:
                if i != 10:
                    n_arr.append(var_s + "A")
                    n_arr.append(var_s + "1")
                    n_arr.append(var_s + "5")
                n_arr.append(var_s + arr[l_digit])
            else:
                if i == 10:
                    n_arr.append(var_s + "AB")
                    n_arr.append(var_s + "14")
                    n_arr.append(var_s + "59")
                else:
                    n_arr.append(var_s + "A")
                    n_arr.append(var_s + "1")
                    n_arr.append(var_s + "5")

        res = n_arr
    return res


for l in get_combinations():
    h = hashlib.md5(replication.reverse(l).encode()).hexdigest()
    if h == "dbbcd6ee441aa6d2889e6e3cae6adebe":
        print l
        exit()
