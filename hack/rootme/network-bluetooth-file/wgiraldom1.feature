## Version 2.0
## language: en

Feature: Bluetooth-Unknown-file
  Site:
    root-me.org
  User:
    william73
  Goal:
    Find the SHA1 hash of the concatenation of a MAC address and phone name
    Given a set of captured packets

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |

  Scenario: Fail:open-directly
    When I start the challenge
    And it prompts
    """
    Your friend working at NSA recovered an unreadable file in a hacker’s
    computer. The only thing he knows is that it comes from a communication
    between a computer and a phone.
    The answer is the sha-1 hash of the concatenation of the MAC address
    (uppercase) and the name of the phone.
    """
    Then there're two files to download
    But one is a binary file, so I can not read it with a code editor
    And the second one is a PDF, describing a file format called btsnoop

  Scenario: Success:parse-bytes
    When the file is parsed,
    And it shows to be a set of Bluetooth PDUs
    Then I need to manage each separate packet.
    But I used a Python module called btsnoop in order to parse the file,
    And I discovered the module could gather connection information,
    Then I used it.
    When I got that information,
    Then I SHA1-ed the concatenation of the phone's MAC address and name.
    And I entered the result into the field at root-me.org
    And it worked.
