## Version 2.0
## language: en

Feature: rsa-common-modulus-rootme
  Code:
    RSA - Common modulus
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    kedavamaru
  Goal:
    Break RSA encryption and decrypt the message and capture the flag.

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    |    GNU bash     |     4.4.19    |
    |     Python      |   2.7.15 rc1  |

  Machine information:
    Given I am accessing the challenge page
    And the title is RSA - Common modulus
    And file "key1_pub.pem" is provided
    """
    -----BEGIN PUBLIC KEY-----
    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtbdQAzdaO7GHXxUsVZ+FmcddA
    Hrugq+azkVdfgnHu6teK3hDQlk0BdNz9LlQT3BoHXg5/g9FDv3bBwaulpQEQPlGM
    UXEUnQAJ69KSVaLxHb5Wmb0vqX/qySKc8Hseqt5wbXklOrnZeHJ3Hm3mUeIplpWP
    f19C6goN3bUGrrniwwIDAQAB
    -----END PUBLIC KEY-----
    """
    And file "key2_pub.pem" is provided
    """
    -----BEGIN PUBLIC KEY-----
    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtbdQAzdaO7GHXxUsVZ+FmcddA
    Hrugq+azkVdfgnHu6teK3hDQlk0BdNz9LlQT3BoHXg5/g9FDv3bBwaulpQEQPlGM
    UXEUnQAJ69KSVaLxHb5Wmb0vqX/qySKc8Hseqt5wbXklOrnZeHJ3Hm3mUeIplpWP
    f19C6goN3bUGrrniwwIDBTy3
    -----END PUBLIC KEY-----
    """
    And file "message1" is provided
    """
    BzFd4riBUZdFuPCkB3LOh+5iyMImeQ/saFLVD+
    ca2L8VKSz0+wtTaL55RRpHBAQdl24Fb3XyVg2N
    9UDcx3slT+vZs7tr03W7oJZxVp3M0ihoCwer3x
    ZNieem8WZQvQvyNP5s5gMT+K6pjB9hDFWWmHzs
    n7eOYxRJZTIDgxA4k2w=
    """
    And file "message2" is provided
    """
    jmVRiKyVPy1CHiYLl8fvpsDAhz8rDa/Ug87ZUX
    Z//rMBKfcJ5MqZnQbyTJZwSNASnQfgel3J/xJs
    jlnf8LoChzhgT28qSppjMfWtQvR6mar1GA0Ya1
    VRHkhggX1RUFA4uzL56X5voi0wZEpJITUXubbu
    jDXHjlAfdLC7BvL/5+w=
    """

  Scenario: Success:discovering the vulnerability
    Given I inspect the PEM certificates with encoding X.509
    Then I get the following results for "key1_pub.pem"
    """
    Algo RSA
    Format X.509
    RSA Public Key
        [b4:da:98:66:88:09:21:fe:da:0d:a2:09:75:53:53:c3:8e:c3:8e:5f]
    modulus:
      0xad6dd400cdd68eec61d7c54b1567e16671d7401ebba0abe6b391575f8271eeea
        d78ade10d0964d0174dcfd2e5413dc1a075e0e7f83d143bf76c1c1aba5a50110
        3e518c5171149d0009ebd29255a2f11dbe5699bd2fa97feac9229cf07b1eaade
        706d79253ab9d97872771e6de651e22996958f7f5f42ea0a0dddb506aeb9e2c3
    public exponent:
      0x10001
    """
    And I get the following results for "key2_pub.pem"
    """
    Algo RSA
    Format X.509
    RSA Public Key
        [b4:da:98:66:88:09:21:fe:da:0d:a2:09:75:53:53:c3:8e:c3:8e:5f]
    modulus:
      0xad6dd400cdd68eec61d7c54b1567e16671d7401ebba0abe6b391575f8271eeea
        d78ade10d0964d0174dcfd2e5413dc1a075e0e7f83d143bf76c1c1aba5a50110
        3e518c5171149d0009ebd29255a2f11dbe5699bd2fa97feac9229cf07b1eaade
        706d79253ab9d97872771e6de651e22996958f7f5f42ea0a0dddb506aeb9e2c3
    public exponent:
      0x53cb7
    """

  Scenario: Success:bezout's identity
    Given I currently know the modulus are the same for the public keys
    Then I prepare data to exploit the vulnerability
    When I turn the base64 encoded encrypted messages to integers
    """
    $ python
    >>> import base64
    >>> exp1 = int(base64.b64decode(message1).encode('hex'), 16)
    ... some really big number ...
    >>> exp2 = int(base64.b64decode(message2).encode('hex'), 16)
    ... some really big number ...
    """
    Then I can run a common modulus attack
    """
    $ # modinv(a, n) is a function to return the modular inverse of a under n
    $ # gcd(a, b) a function to return the greatest common divisor between a, b
    $ python
    >>> s1 = modinv(exp1, exp2)
    >>> s2 = (gcd(exp1, exp2) - exp1 * s1) / exp2
    >>> m1 = pow(message1, s1, modulus)
    >>> m2 = pow(modinv(message1, modulus), -1 * s2, N)
    >>> m  = (m1 * m2) % N
    >>> format(m, 'x').decode('hex')
    Yeah man, you got the message.

    The flag is W311D0n3! and this is a padding to have a long text,
      else it will be easy to decrypt.
    """
    When I enter "W311D0n3" as flag
    Then I solve the challenge
