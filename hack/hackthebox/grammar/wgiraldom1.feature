## Version 2.0
## language: en

Feature: iknowmag1c - HackTheBox
  Site:
    www.hackthebox.eu
  User:
    williamgiraldo
  Goal:
    Get to the admin userpage

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | PHP               |    7.1.1   |
  Machine Information:
    Given this is a web-based challenge,
    When I get to the challenge page
    Then I will inspect its source and resources

  Scenario: Fail:load-challenge
    Given I start the challenge
    And it prompts
    """
    When we access this page we get a Forbidden error.
    However we believe that something strange lies behind...
    Can you find a way in and retrieve the flag?
    """
    When I load the challenge site,
    Then the site effectively responds with a 403 - Forbidden.

  Scenario: Fail:post-request
    Given I tried GoBuster directory listing,
    And no information is returned,
    When I wonder what GoBuster does
    Then I recall it only makes HTTP GET requests,
    Then I make a POST request to the default URL
    And I get an interesting page [evidence](img1.png)
    Then I notice it is running PHP.

  Scenario: Fail:what-is-in-the-cookie
    Given I inspect the source code and find nothing is worth noting,
    When I examine the request
    Then I notice a custom Cookie:
    """
    ses=eyJVc2VyIjoiYWJjZGUiLCJBZG1pbiI6IkZhbHNlIiwiTUFDI
     joiNWM0ZGUzZGU3NGU3MGNlNmEyNjQwMjVhOTQwYWMzNTIifQ%3D%3
    """
    And I believe this Cookie is URL-encoded
    Then I decode it and find
    """
    eyJVc2VyIjoiYWJjZGUiLCJBZG1pbiI6IkZhb
    HNlIiwiTUFDIjoiNWM0ZGUzZGU3NGU3MGNlNmEyNjQwMjVhOTQwYWMzNTIifQ==
    """
    And it is clearly a base64-encoded string.
    Then after decoding I get
    """
    {"User":"abcde","Admin":"False","MAC":"5c4de3de74e70ce6a264025a940ac352"}
    """
    And it is clearly a JSON resource.

  Scenario: Fail:naive
    When I try modifying and encoding the Cookie with the content
    """
    {"User":"abcde","Admin":"True","MAC":"5c4de3de74e70ce6a264025a940ac352"}
    """
    Then nothing happens.
    And I try with
    """
    {"User":"abcde","Admin":"True"}
    """
    And still nothing happens.
    Then I notice the content in the input made its way into the "User" field.

  Scenario: Fail:JSON-injection
    Given I now know user input is embedded into the JSON,
    When I try to introduce something like
    """
    User","Admin":"True
    """
    Then the input gets sanitized, and the webpage tells me only lowercase
    And letters-digits are allowed.
    Then I think this is not a worth approach.

  Scenario: Success:typejuggling
    When the Cookie reachs the server, lets think what would happen
    """
    Lets define H as a hash function over "User" and "Admin" fields.
    We can do this because we played with the "User" field and
    the hash in the "MAC" field changed.
    Also, we could not think H is a function of the "MAC" field,
    because this would turn into a contradiction, or an infinite recursion
    {H(U, A, H(U, A, H(U, A, H(U, A, ...))))}
    """
    Then the server might have a validation of the form
    """
    01: if(H($user, $admin) == $MAC){
    02:  // Validate $admin permissions
    03: }
    """
    And the validation would be prone to typejuggling.
    When PHP compares a string with a number,
    Then it converts said string to a number, and perform numerical comparison.
    Given we are handling a JSON,
    Then we are able to fix the type of the MAC field to a number.
    And as the range of H is the set of HEX strings
    Then its output, converted to a number, must be zero
    Given the result starts with a non-digit,
    And any number between 1 and 10^n-1
    Given the first n chars of the output are digits.
    But we will rely on our luck and say we can't be too damned
    And have n>2.
    Then we devise a PHP script to make a bruteforce attack on the MAC field
    And encode the result as the server expects
    And we run it from 0 to 100.
    Then we get the flag (evidence)[img2.png].
