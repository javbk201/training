## Version 2.0
## language: en

Feature: Pusheen_Loves_Graps-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Stego
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | IDA Free        | 7.0         |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given a file
    """
    Pusheen
    """
    And A clue
    """
    Pusheen just loves graphs, Graphs and IDA.
    Did you know cats are weirdly controlling
    about their reverse engineering tools?
    Pusheen just won't use anything except IDA.
    """
    And A flag format of
    """
    HTB{$FLAG}
    """
    And a disassembler program in IDA
    And its used to generate the assembly code of an executable
    And Its running on Mint 19.3 with  kernel 18.04.3

  Scenario: Fail: Did not find the flag
    Given the file "Pusheen"
    When I give the file permissions to execute
    Then I execute the file regularly
    And I get a printout of pusheen [evidence] (pusheenCat.png)
    When I copy the input to a text editor
    Then I realize the image is just plain text characters
    And The flag is not there

  Scenario: Success: Found the flag
    Given the file 'Pusheen'
    When I Load the file into IDA Free [evidence] (pusheenOpen.png)
    Then I open the file as an ELF type [evidence] (pusheenELF.png)
    And I get an error on the method "main"
    """
    The graph is too big (more than 100 nodes) to be displayed on the screen.
    Switching to text mode.
    (You can change this limit in the graph options dialogue)
    """
    When I go to the graph options dialogue
    Then I set the maximum nodes to 9999 using the GUI [evidence] (Nodes.png)
    When I go back to the "main" method view in IDA
    Then I am able to see a graph representation of the 'main' method
    And I start looking through the graph nodes
    But They are too many
    And I can't find an explicit flag within any nodes
    When I look at the graph overview image
    Then I can see some text is formed by the blank spaces between the nodes
    And I realize I might have found the flag [evidence] (pusheenFlag.png)
    When I format the text found with the flag format
    Then I input the formated text as a solution
    And I solve the challenge
