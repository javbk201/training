#lenguage: en

Feature: Solve Stop us challenge
  From the Wechall site
  Of the category Exploit and PHP
  As the registered user disassembly

  Background:
    Given a test site to purchase domains
    And the source code of this site

  Scenario Outline: Purchase a domain without paying for it

  Scenario: Make test purchases
    Given two options (upload money and purchase domain) in the home page
    Given I try to purchase a domain without money
    Then the site tells me that I have insufficient founds
    Given I upload money ($10.00)
    And try again to purchase a domain
    Then the site shows me that I have purchased the domain

  Scenario: Examine the source code
    Given the highlighted version of the source code
    Then I found some interesting lines
    """
    89   # +1 domain
    90   if (false === noothtable::purchaseDomain($sid))
    91   {
    92     die('Hacking attempt!');
    93   }
    105  # Something weird? Oo
    106  if (noothtable::getFundings($sid) < noothtable::getDomains($sid))
    107  {
    108    GWF_Module::loadModuleDB('Forum', true, true);
    109    # Get here, hacker!
    110    $chall->onChallengeSolved(GWF_Session::getUserID());
    111  }
    """
    Given the function purchaseDomain($sid) will be false when I have no founds
    And the number of fundings have to be smaller than the number of domains
    Then I acknowledge the conditions to solve the challenge

  Scenario: Find where the vulnerability is
    Given the site have some delays when buying a domain
    And the order of steps to successfully make a purchase
    Then I see that there's a hole in the script between making a purchase
    And charge the purchase
    """
    97   # -$10.00
    98   nooth_message('Reducing your balance ...');
    99   noothtable::reduceMoney($sid, $price);
    100  nooth_message('Thank you for your purchase!');
    """

  Scenario: Expolit the PHP script
    Given I add founds (money) on the site
    And purchase a domain
    But cancel the operation right before the purchase be charged
    Then I have make a purchase without paying for it
    And Solve the challenge
