# language: en

Feature: Solve AUTH me challenge
  From the WeChall site
  Of the category HTTP and Training
  As the registered user disassembly

    Background:
      Given an apache.conf file with the VHost configuration
      And a URL to access the box
      And client certs and key
      Given I have the library cURL with TLS support:
      """
      curl 7.54.0 (x86_64-apple-darwin17.0) libcurl/7.54.0 LibreSSL/2.0.20
      zlib/1.2.11 nghttp2/1.24.0
      """

    Scenario Outline: Connect to the URL hosted on a VHost with TLS enabled

    Scenario: Failed attempt to connect
      When I type the command:
      """
      curl -v --header "Cookie: WC=10329184-39802-qZX1sJWtFrW01cJE"
      https://authme.wechall.net/challenge/space/auth_me/www/index.php
      """
      Then the interpreter show me that:
      """
      curl: (60) SSL certificate problem: self signed certificate in
      certificate chain
      """
      Then I know im dealing with a self signed certificate

    Scenario: Failed attempt to connect
      When I try to connect ignoring the CACert:
      """
      curl -kv --header "Cookie: WC=10329184-39802-qZX1sJWtFrW01cJE"
      https://authme.wechall.net/challenge/space/auth_me/www/index.php
      """
      Then I get the ouput:
      """
      curl: (35) error:14094410:SSL routines:SSL3_READ_BYTES:sslv3 alert
      handshake failure
      """
      Then I realize that im not passing the client cert and key

    Scenario: Successful connection
      When I try to connect adding the client cert and key:
      """
      curl -kv --header "Cookie: WC=10329184-39802-qZX1sJWtFrW01cJE"
      --cert ~/Desktop/client.crt --key ~/Desktop/client.key
      https://authme.wechall.net/challenge/space/auth_me/www/index.php
      """
      Then I successfully connect to the URL
