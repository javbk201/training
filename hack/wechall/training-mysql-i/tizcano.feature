# language: en

Feature: Training: MySQL I
  From the WeChall site
  Of the category MySQL, Exploit and Training
  As the registered user tizcano

  Background:
    Given the source of a PHP file that create and run the sql query

  Scenario: A sql injection
    When I realize that you can comment a part of the query
    Then I put the corresponding username a comment the rest of the query
    """
    username field: admin'#
    executed query: SELECT * FROM users WHERE username='admin'#' AND ..
    """
    And Solve the challenge
