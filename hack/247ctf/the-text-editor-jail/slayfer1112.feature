## Version 2.0
## language: en

Feature: MISCELLANEUS-247ctf
  Site:
    247ctf
  Category:
    MISCELLANEOUS
  User:
    slayfer1112
  Goal:
    Found the flag using the text editor

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    And the website show a form for authenticating

  Scenario: Fail:type-:quit-to-exit
    Given I am on a page that simulates vim editorr
    When I try to write something
    And I see that only can write
    Then I press ctr + c to try to stop vim
    And I see a message that say "Type :quit to exit vim"
    When I try typing ":quit"
    Then I see a message that say "E138: Can't write viminfo file"
    And I fail getting the flag

  Scenario: Success:command-line-with-!
    Given I can type commands with a key combination
    When I press ctr + c to try to stop vim
    Then I see again the message "Type :quit to exit vim"
    And I try to type any command with ":!"
    When I see the command run
    And I try to type ":! ls" to see the files
    When I see the file "run_for_flag"
    Then I try to type "./run_for_flag"
    And I get a message with the flag
    When I put the flag in the validation field
    Then I receive a popup asking a calification for the challenge
    And I capture the flag
