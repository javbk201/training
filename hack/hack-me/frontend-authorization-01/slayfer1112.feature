## Version 2.0
## language: en

Feature: CHALLENGE-Hac.me
  Site:
    Hac.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag to pass to the next stage

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a username and password to continue

  Scenario: Fail:Type-a-generic-username-and-password
    Given I'm on the web with a login form
    When I try to use a generic username and password like "admin","root"
    Then I receive a message that says "Error: Username is incorrect!"
    And I fail to get the flag

  Scenario: Success:Inspect-code-to-see-the-script
    Given I complete the second stage
    Given I'm on the field that ask me the name of the doctor
    When I Inspect the page to see the HTML
    Then I see a function in the submit button "checkForm();"
    And I try to find the script to see the function
    When I found the script I see the function
    """
    function checkForm() {
      // Lets check the username
      var username = document.getElementById('username');
      if( username.value != '<username>' ){
        return createErrorMessage('Error: Username is incorrect!', username);
      }

      // Lets check the password
      var password = document.getElementById('password');
      if(password.value.length < 8) {
        return createErrorMessage('Error: Password must contain at least
                                  eight characters!', password);
      }
      if(password.value.length >= 10) {
        return createErrorMessage('Error: Password must contain less than
                                  ten characters!', password);
      }
      re = /[0-9]/;
      if(!re.test(password.value)) {
        return createErrorMessage('Error: password must contain at least one
                                  number (0-9)!', password);
      }
      re = /[a-z]/;
      if(!re.test(password.value)) {
        return createErrorMessage('Error: password must contain at least one
                                  lowercase letter (a-z)!', password);
      }
      re = /[A-Z]/;
      if(!re.test(password.value)) {
        return createErrorMessage('Error: password must contain at least one
                                  uppercase letter (A-Z)!', password);
      }
      if(password.value == '<password>') {
        $('#myModal').modal('show');
      } else {
        return createErrorMessage('You entered a valid password, but not the
                      correct one! You entered: ' + password.value, password);
      }
    }
    """
    Then I inspect that to find any clue for the username and password
    And I found some hints
    When I read "if( username.value != '<username>' )"
    Then I get the username of the log in form
    And I try to find the password
    When I read this line "if(password.value == '<password>')"
    Then I see the password
    And I try to use this parameters in the login form
    When I submit the login form with the username and password
    Then I see a popup with the a message that says "Good Job"[evidence](img1)
    And I click on the button that says "Next Problem"
    When I'm redirected to the second test [evidence](img2)
    Then I complete the first test
    And I found the flag
