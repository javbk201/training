# language: en

Feature: Solve challenge 23
  From site W3Challs
  From Hacking Category
  With my username Skhorn
 
Background:
  Given the fact i have an account on W3Challs site
  And i have Debian 9 as Operating System
  And i have internet access
  And i have open the resources 
        
Scenario: Succesful attempt
  Given the link to the challenge
  And a web page that seems to be in construction
  And have a Home section
  And a Forum section
  And an Administration section
  And while looking the Administration section it displays a denied access text
  And i check the cookies
  And i see there is one called authz
  And it seems like this is a hash of 128bits long
  And i use a SHA256 online decryptor tool
  And i see the decrypted text is a readable word
  And the word is related with user management context
  And i try to get the hash of another related word with this context
  And i use SHA256 as the hast
  When i use the output 
  And put it in the authz value
  Then i end the challenge
