## Version 2.0
## language: en

Feature: is-it-a-secure-strings
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version> |
  | Ubuntu (Bionic) | 18.04.1   |
  | Python3         | 3.6.9     |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/55
    """
    Then I see an encrypted text and a key
    And the challenge is to recover the flag

  Scenario: Success: Python-AES-Decipher
    Given the challenge information
    When I have the idea to put the key in a Python array
    Then I notice it has a size of 256 bits
    And I decide to look for encryption algorithms with that key length
    And I come across AES256
    When I investigate about Python implementations of AES256
    """
    https://stackoverflow.com/questions/12524994/encrypt-decrypt-using-
    pycrypto-aes-256
    """
    Then I adapt them to this challenge
    And build a script to decipher the string [exploit.py]
    When I execute the script
    Then I get the flag
