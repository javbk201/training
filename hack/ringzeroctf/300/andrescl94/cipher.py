#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This script implements a substitution algorithm that shuffles the key
after every iteration durint the encryption process
"""

import argparse
import re


def shuffle(key, letter):
    """
    Reorganizes an array around the position of 'letter'. All characters on its
    right are placed on its left and viceversa.
    """
    tmp_key = []
    letter_index = key.index(letter)
    if letter_index == 0:
        tmp_key.append(key[1:])
        tmp_key.append(letter)
    elif letter_index == len(key)-1:
        tmp_key.append(letter)
        tmp_key.append(key[:letter_index])
    else:
        tmp_key.append(key[letter_index+1:])
        tmp_key.append(letter)
        tmp_key.append(key[:letter_index])
    return ''.join(tmp_key)


def encrypt(message, key):
    """
    Encrypts a message using the specified key
    """
    encrypted_message = []
    for letter in message:
        alpha_index = ALPHABET.index(letter)
        key_letter = key[alpha_index]
        encrypted_message.append(key_letter)
        key = shuffle(key, letter)
    encrypted_message = ''.join(encrypted_message)
    return encrypted_message


def decrypt(message, key):
    """
    Decrypts a message using the specified key
    """
    decrypted_message = []
    for letter in message:
        key_index = key.index(letter)
        alpha_letter = ALPHABET[key_index]
        decrypted_message.append(alpha_letter)
        key = shuffle(key, alpha_letter)
    decrypted_message = ''.join(decrypted_message)
    return decrypted_message


def create_key(keyword):
    """
    It uses the provided keyword to shuffle the alphabet and generate the key
    that will be used for the encryption algorithm
    """
    key = ALPHABET
    for letter in keyword:
        key = shuffle(key, letter)
    return key


if __name__ == "__main__":
    ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    MESSAGE = 'SENDHELP'
    CIPHERTEXT = 'TQSBAODTTABMRUHDKNVUORAKATOZLFBFDWPHQLANSZIKOSEDESXZLDYEUB'\
        'JRROAVZRBSLWESCEGGOCEMLFMAHAYSRNMCXATHGNZQBCLSCEMKIVELCRXCJTBBTXGBR'\
        'NDQTLJMLUOEQWTHWVBAZHAABXPZELKBNWSNCZLNSBELFFKDLVFWOWNDQWMLFXEQWAQO'\
        'QRIAAVSXAADYEUUAMTHYLSCVILMNE'

    AP = argparse.ArgumentParser()
    AP.add_argument('-f', '--file', required=False, help='Keywords file')
    AP.add_argument('-k', '--keyword', required=False, help='Keyword')

    ARGS = vars(AP.parse_args())
    if ARGS.get('file', ''):
        with open(ARGS['file'], 'r') as f:
            for line in f.readlines():
                if re.match(r'^[a-zA-Z]+$', line.strip()):
                    ENC_KEY = create_key(line.strip().upper())
                    RESULT = decrypt(CIPHERTEXT, ENC_KEY)
                    if re.search(r'FLAG[A-Z]*', RESULT):
                        print(line)
                        print(RESULT)
    elif ARGS.get('keyword', ''):
        ENC_KEY = create_key(ARGS['keyword'].strip().upper())
        RESULT = decrypt(CIPHERTEXT, ENC_KEY)
        if re.search(r'FLAGH[A-Z]*', RESULT):
            print(RESULT)
