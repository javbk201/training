## Version 2.0
## language: en

Feature: headache-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/43
  Category:
    Web
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/43
    """
    When I open the url with Chrome
    Then I see a text "Answer is closer than you think!"
  Scenario: Fail:Inspect
    Given the url
    Then I try to see the page content
    And I don't see anything
    Then I don't solve the challenge

   Scenario: Success:header
    Given that I inspected the page content
    When I tried to get the headers
    Then I get the flag
    Then I solve the challenge
