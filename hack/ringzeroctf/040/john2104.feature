## Version 2.0
## language: en

Feature: whats-the-definition-of-null-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/40
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Bypass the login form

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/40
    """
    When I open the url with Chrome
    Then I see a login form
    And a text string with the query
    """
    Hint WHERE (id IS NOT NULL) AND (ID = ? AND display = 1)
    """
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    Then I see that the input is in base64 "1 eq MQ=="
    When I try with a well known payload
    """
    0) or 1=1 --
    MCkgb3IgMT0xIC0t
    """
    Then I get nothing
    And I don't solve the challenge

   Scenario: Success:Sql-injection
    Given that I inspected the hint
    Then I used the following payload
    """
    0) or (ID IS NULL)  --
    MCkgb3IgKElEIElTIE5VTEwpICAtLQ==
    """
    And I get the flag
    Then I solve the challenge
