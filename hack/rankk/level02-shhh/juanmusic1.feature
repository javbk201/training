## Version 2.0
## language: en

Feature: shhh - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/shhh.py
  User:
    JuanMusic1 (wechall)
  Goal:
    Find the key in the page

  Background:
  Hacker's software:
    | <Software name> | <Version>  |
    | Ubuntu          | 18.04.3    |
    | Firefox         | 72.0.2     |
  Machine information:
    Given A link in the challenge page
    When I access it
    Then I can see a message
    """
    Shhhh. Don't tell them.
    """

  Scenario: Fail: Decode the message in decimal to ASCII
    Given The page
    When I search in the source code
    Then I find a comment
    """
    <!-- ASCII 73 61 47 47 61 72 61 //-->
    """
    When I try to decode the numbers in decimal to ASCII with the following tool
    """
    https://www.rapidtables.com/convert/number/ascii-hex-bin-dec-converter.html
    """
    Then I obtain the following string
    """
    I=//=H=
    """
    And I try to put it as flag
    Then The challenge says it is wrong

  Scenario: Success: Decode the message in hex to ASCII
    Given The page
    And The comment
    When I try to decode the numbers in hex to ASCII with the following tool
    """
    https://www.rapidtables.com/convert/number/ascii-hex-bin-dec-converter.html
    """
    Then I obtain a string
    When I try to put it as flag
    Then I solve the challenge
