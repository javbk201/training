Feature: Solve The nth term challenge
  from site rankk
  With my username EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Successful solution
  Given The current challenge.
  When I realize the series presented are fibonacci's series
  Then I write a script in Java
  And I solve the challenge.

