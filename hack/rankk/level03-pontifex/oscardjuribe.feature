## Version 2.0
## language: en

Feature: level03-pontifex
  Site:
    https://www.rankk.org/challenges/pontifex.py
  User:
    alestorm980 (wechall)
  Goal:
    Decrypt the hidden message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7.15+     |
  Machine information:
    Given An image containing some cards [evidence](image1.png)
    And A random text
    Then I have to decryp the text

  Scenario: Fail: Brute force string
    Given A random text
    When I try to bruteforce the string using a web page
    """
    https://www.dcode.fr/caesar-cipher
    """
    Then I get random information without sense
    And I decide to change my method

  Scenario: Success: Create a Python script
    Given A random text
    When I read the challenge description
    Then I see the Pontifex word
    When I search it on Google
    Then I find a brief description of the method
    """
    https://www.revolvy.com/page/Solitaire-%28cipher%29
    """
    When I understan the method
    Then I decide to create a Python script "pontifex.py"
    When I create an array containing the suit's card and his value
    Then I run my script
    """
    The solution is <flag>
    """
    And I get the solution
