## Version 2.0
## language: en

Feature: Unix 101 - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/unix-101.py
  User:
    juansorduz97
  Goal:
    Change the permission of the file

  Background:
  Hacker's software:
    |  Ubuntu         |  16.04.6    |
    |  Mozilla        |  71.0       |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see the following message
    """
    Using the numeric representation, help Genius change the permission of the
    file as follows:
      Owner: full access
      Group: read access
      Other: no access
    $ ls -l restricted
     -rwxr-xr-x   1   Genius   220   Feb 07 15:36   restricted
    """

  Scenario:  Fail:Put the permission number
    Given The permissions for owner, group and other
    Then I open the following tool https://chmod-calculator.com/
    And I introduce the permissions and obtain the number 740
    Then I introduce the number in the challenges
    And The number did not work

  Scenario:  Fail:Put the command
    Given The number did not work by its own
    Then I think that the challenge request the full command
    And I introduce sudo chmod 740 restricted
    And The command did not work

  Scenario:  Success:Put the command without root user
    Given The command did not work
    And Genius is the file owner
    Then I use the previous command without sudo
    And I solved the challenge
