# $ pylint decode.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
code to decode the password of the challenge colourfull-life
"""

import re

# content of web page
HTML_TEXT = """<!DOCTYPE html>
<html>
<head>
<title>Level 3/30</title>
<link href="/css/rankk.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="challenge">
<h1>Level 3/30</h1>
<h3>Colourful life</h3>
<form name="challenge" method="post">
<p>
<span style="color: #FE4F27">T</span>
<span style="color: #FCFBC8">h</span>
<span style="color: #0FF8EA">e</span>
<span style="color: #FD2DF1">s</span>
<span style="color: #2EFB3D">o</span>
<span style="color: #9375F3">l</span>
<span style="color: #F91807">u</span>
<span style="color: #FCF7F7">t</span>
<span style="color: #2CF9B3">i</span>
<span style="color: #034FFA">o</span>
<span style="color: #F7B72E">n</span>
<span style="color: #9829F6">i</span>
<span style="color: #BEC4BF">s</span>
<span style="color: #FFFF63">c</span>
<span style="color: #FFFF6F">o</span>
<span style="color: #FFFF6C">d</span>
<span style="color: #FFFF6F">e</span>
<span style="color: #FFFF75">b</span>
<span style="color: #FFFF72">r</span>
<span style="color: #FFFF63">e</span>
<span style="color: #FFFF6F">a</span>
<span style="color: #FFFF64">k</span>
<span style="color: #FFFF65">e</span>
<span style="color: #FFFF64">r</span>
<span style="color: #FA2F0E">!</span></p>
<p><input type="text" size="30"name="solution"
class="solution" maxlength="1500" /></p>
<input type="submit" name="submit" value="submit" class="button" />
<script>document.challenge.solution.focus()</script>
</form>
</div>
</body>
</html>
"""

# variable to concat the char after convertion
SOL = ""

# iterate over all values matching the regex
for hex_color in re.findall(r"#[A-Z|0-9]{6}", HTML_TEXT):
    # only take value starting with #FFFF
    if hex_color[:5] == "#FFFF":
        # convert hex to int and later to character
        SOL += chr(int(hex_color[5:], base=16))

# print the solution
print "The solution is: " + SOL

# $ python decode.py
# The solution is: <flag>
