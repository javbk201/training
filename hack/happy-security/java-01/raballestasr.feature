# language: en

Feature: Solve the challenge Java 1001
  From the happy-security.de website
  From the Java category
  With my username raballestas

  Background:
    Given a java applet

  Scenario: Succesful solution
    When I download the applet's .class file
    And decompile it with jad
    And I look at the applet's code
    Then I see that it uses another class called "fenster"
    Then I download, decompile and examine its source
    And I find a method that checks the password
    And I solve the challenge
