## Version 2.0
## language: en

Feature: 47-H4CK1NG-enigmes-a-thematiques
  Code:
    47
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the website

  Scenario: Fail:convert-to-string
    Given I am on the main page
    And I can see a form with a field and a "Valider" button
    When I do not fill the field
    And I press the "valider" button
    Then I can see an alert with a message
    """
    Ce n'est pas le bon mot de passe...
    """
    When I inspect into the source Code
    Then I see the related script
    """
    function password()
    {
      var mdp=unescape("%68%65%78%61%73%70%65%72");
      if (document.toto.ep47.value == mdp)
        alert("Mot de passe correct ! Tu peux entrer
        ce mot pour valider l\'énigme.");
      else
        alert ("Ce n\'est pas le bon mot de passe...");
    }
    """
    When I convert the value inside the unescape function to string
    Then I can see the string
    """
    eapr
    """
    When I fill the field of the form with the string
    Then I can not capture the flag

  Scenario: Success:use-unescape-function
    Given I am on the main page
    When I search for the unescape function on the internet
    Then I find the description of the function
    And I find an interpreter to try the function
    When I try the escape function with the string
    """
    %68%65%78%61%73%70%65%72
    """
    Then I can see the a possible "<FLAG>"
    When I fill the field with the "<FLAG>"
    Then A new alert appears with a message
    """
    Mot de passe correct ! Tu peux entrer ce mot pour valider l'énigme.
    """
    When I enter the "<FLAG>" into the challenge form
    And I press the "Valider" button
    Then I can see the message "Bravo"
    And I capture the flag
