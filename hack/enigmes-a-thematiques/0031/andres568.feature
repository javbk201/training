## Version 2.0
## language: en

Feature: 31-H4CK1NG-enigmes-a-thematiques
  Code:
    31
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Decrypt code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing to the website
    And the challenge show an encrypted message

  Scenario: Success:decrypt-hash-code
    Given I am on the challenge page
    And I can see an encrypted message that looks like a hash code
    When I use the hash analyzer from "www.tunnelsup.com/"
    Then A chart show "Hash type MD5 or MD4"
    When I use a md5 decripter from "www.md5online.org/"
    Then I can see the message "<FLAG>"
    When I enter the message into the challage form
    And I press the "Valider" button
    Then I can see the message "Bravo"
    And I could capture the flag
