# language: en

Feature: Solve the challenge 11
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a batch script authentication system
    And I have to find the passsword that access the system
    And I am running the linux distribution, Ubuntu 16.04 LTS

  Scenario: Successful Solution
    When I analyze the batch script
    Then I find an integer comparation so I assume this is a hint of how to find the password
    Then I keep looking in the script 
    Then I Discover that the password have to be equivalent to the numbers that multiply between them reach the value of given integer
    Then I write and execute a short program that multiply all the posibles multiples until reach integer value
    Then I convert the number in letter with the conversion table given in the batch script
    And I solve the challenge 

