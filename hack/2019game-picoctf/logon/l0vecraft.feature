## Version 2.0
## language: en

Feature: logon - web - picoCTF
  Code:
    logon
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04           |
    | FireFox         | 72.0.1(64 bits) |
    | BurpSuite Community| 2.1.07 (64 bits) |
  Machine information:
    Given I access to the challenge via browser
    When I enter to the page I see a form
    And on the top right corner i see tow tabs "home" and "sign out"
    Then I can assume the challenge probably is bypass the form security

  Scenario: Fail: Default credentials
    When I try to enter the default credentials "admin" and "admin"
    Then I get redirect to another page
    And I see a message
    """
    Success: You logged in! Not sure you'll be able to see the flag though.
    """
    When I try to enter another default credentials like "admin" and "1234"
    Then I get redirected to the same page
    And I see the same message
    And I could not capture the flag

  Scenario: Success: Intercepting requests
    When I inspect the source code, I see the form makes a POST requests
    Then I configure BurpSuite to intercept my requests
    When I enter a random credentials like "admin" and "pass"
    Then I can see the body of the POST request
    And I see also the params:
    """
    user=admin&password=pass
    """
    When I get the redirection I see a cookie param "admin" set "False"
    Then I modify that param to "True" and I forward it the request
    """
    password=pass; username=admin; admin=True
    """
    And now I can capture the flag
