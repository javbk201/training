{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  inputs = [
    pkgs.nodejs
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            srcEslintConfig = ../../../configs/eslint-javascript.json;

            builder = ./builder.sh;

            nodeJsModuleYargs = builders.nodePackage "yargs@15.3.1";

            nodeJsModuleEslint = builders.nodePackage "eslint@7.1.0";
            nodeJsModuleEslintstrict = builders.nodePackage "eslint-config-strict@14.0.1";
            nodeJsModuleEslintpluginfs = builders.nodePackage "eslint-plugin-fp@2.3.0";
          })
    )
