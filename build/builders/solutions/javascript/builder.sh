source "${srcGeneric}"

function lint {
  local solution="${1}"

      eslint \
        -c "${srcEslintConfig}" \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_node_modules \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
