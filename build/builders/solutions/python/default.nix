{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.pythonPackage = import ../../../builders/python-package pkgs;
  inputs = [];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          srcProspectorConfig = ../../../configs/prospector.yaml;

          builder = ./builder.sh;

          pyPkgProspector = builders.pythonPackage "prospector[with_everything]==1.2.0";
          pyPkgMypy = builders.pythonPackage "mypy==0.770";
        })
  )
