source "${srcGeneric}"

function lint {
  local solution="${1}"

      prospector \
        --profile "${srcProspectorConfig}" \
        "${solution}" \
  &&  mypy \
        --strict \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_python_packages \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
