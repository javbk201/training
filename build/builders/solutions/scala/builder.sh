source "${srcGeneric}"

function lint {
  local solution="${1}"

  java \
    -jar "${srcScalastyle}" \
    --config "${srcScalastyleConfig}" \
    --warnings true \
    "${solution}"
}

function compile {
  local solution="${1}"

  scalac \
    -Werror \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
