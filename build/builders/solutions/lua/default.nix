{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.lua5_3
    pkgs.lua53Packages.luacheck
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;
          })
    )
