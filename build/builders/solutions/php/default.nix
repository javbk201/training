{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.composerPackage = import ../../../builders/composer-module pkgs;
  inputs = [
    pkgs.php74
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;

            composerModulePhplint = builders.composerPackage "overtrue/phplint:2.0.2";
          })
    )
