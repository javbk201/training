source "${srcGeneric}"

function lint {
  local solution="${1}"

      php -l \
        "${solution}" \
  &&  phplint \
        --no-configuration \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_composer_modules \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
