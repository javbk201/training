source "${srcGeneric}"

function lint {
  local solution="${1}"

      perltidy  \
        -i=2 \
        -nt \
        -ple \
        "${solution}" \
  &&  diff "${solution}" "${solution}.tdy" \
  &&  perlcritic \
        --noprofile \
        --brutal \
        --exclude 'NamingConventions::Capitalization' \
        --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
        --verbose 11 \
        "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
