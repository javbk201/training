source "${srcGeneric}"

function lint {
  local solution="${1}"
  local min_confidence='0.5'

  golint \
    -min_confidence "${min_confidence}" \
    -set_exit_status \
    "${solution}"
}

function compile {
  local solution="${1}"
  local HOME='.'

  go build \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
