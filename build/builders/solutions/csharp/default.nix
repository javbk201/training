{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.mono
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            binGendarme = pkgs.fetchzip {
              url = "https://github.com/downloads/spouliot/gendarme/gendarme-2.10-bin.zip";
              sha256 = "1yg04rfzfk8kc4bqq5qv31ic2jwfgk8f7hd2j99saazhcxhax507";
              stripRoot=false;
            };
            builder = ./builder.sh;
          })
    )
