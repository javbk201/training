source "${srcGeneric}"

function compile {
  local solution="${1}"

  mcs "${solution}"
}

function lint {
  local solution="${1}"

  mono \
    "${binGendarme}/gendarme.exe" \
    --severity all \
    --set self-test \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  compile "root/src/${solutionFileName}" \
  &&  lint "root/src/${solutionFileName%.*}.exe"
}

build || exit 1
echo > "${out}"
