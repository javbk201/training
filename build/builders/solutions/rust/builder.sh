source "${srcGeneric}"

function lint {
  local solution="${1}"
  local config="max_width=80,tab_spaces=2"

  rustfmt \
    --check \
    --config "${config}" \
    "${solution}"
}

function compile {
  local solution="${1}"

  rustc \
    -D warnings \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
