import ./fetch-src.nix {
  repo = "https://github.com/NixOS/nixpkgs";
  commit = "e0777cba1913491e8cb90c4692664907bbe7841f";
  digest = "1bhqbs4iqngxf82f5600hy8xmhjky3b2k1g97lcz2jp3fh4lsrrh";
}
