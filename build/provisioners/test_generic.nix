let
  pkgs = import ../pkgs/stable.nix;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (import ../src/external.nix pkgs)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.cacert
            pkgs.file
            pkgs.yq
            pkgs.pre-commit
            pkgs.python37Packages.yamllint
          ];
        })
  )
