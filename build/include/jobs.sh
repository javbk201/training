# shellcheck shell=bash

source "${srcIncludeHelpers}"
source "${srcEnv}"

function job_build_nix_caches {
  local provisioners
  local dockerfile
  local context='.'
  local dockerfile_generic='build/Dockerfile.generic'
  local dockerfile_solutions='build/Dockerfile.solutions'

      helper_use_pristine_workdir \
  &&  provisioners=(./build/provisioners/*) \
  &&  helper_build_nix_caches_parallel \
  &&  for (( i="${lower_limit}";i<="${upper_limit}";i++ ))
      do
            provisioner=$(basename "${provisioners[${i}]}") \
        &&  provisioner="${provisioner%.*}" \
        &&  if echo "${provisioner}" | grep -q 'build_solutions_'
            then
              dockerfile="${dockerfile_solutions}"
            else
              dockerfile="${dockerfile_generic}"
            fi \
        &&  helper_docker_build_and_push \
              "${CI_REGISTRY_IMAGE}/nix:${provisioner}" \
              "${context}" \
              "${dockerfile}" \
              'PROVISIONER' "${provisioner}" \
        ||  return 1
      done
}

function job_build_solutions_gherkin {
  local files
  local builder='build/builders/solutions/gherkin'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ hack/ -type f -name '*.feature')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_java {
  local files
  local builder='build/builders/solutions/java'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.java')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_shell {
  local files
  local builder='build/builders/solutions/shell'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.sh')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_nix {
  local files
  local builder='build/builders/solutions/nix'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.nix')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_python {
  local files
  local builder='build/builders/solutions/python'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.py')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_csharp {
  local files
  local builder='build/builders/solutions/csharp'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.cs')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_javascript {
  local files
  local builder='build/builders/solutions/javascript'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.js')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_ruby {
  local files
  local builder='build/builders/solutions/ruby'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.rb')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_clojure {
  local files
  local builder='build/builders/solutions/clojure'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.clj')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_cpp {
  local files
  local builder='build/builders/solutions/cpp'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.cpp')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_scala {
  local files
  local builder='build/builders/solutions/scala'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.scala')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_php {
  local files
  local builder='build/builders/solutions/php'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.php')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_rust {
  local files
  local builder='build/builders/solutions/rust'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.rs')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_d {
  local files
  local builder='build/builders/solutions/d'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.d')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_crystal {
  local files
  local builder='build/builders/solutions/crystal'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.cr')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_ocaml {
  local files
  local builder='build/builders/solutions/ocaml'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.ml')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_haskell {
  local files
  local builder='build/builders/solutions/haskell'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.hs')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_go {
  local files
  local builder='build/builders/solutions/go'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.go')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_c {
  local files
  local builder='build/builders/solutions/c'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.c')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_lua {
  local files
  local builder='build/builders/solutions/lua'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.lua')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_r {
  local files
  local builder='build/builders/solutions/r'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.r')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_erlang {
  local files
  local builder='build/builders/solutions/erlang'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.erl')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_dart {
  local files
  local builder='build/builders/solutions/dart'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.dart')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_coffeescript {
  local files
  local builder='build/builders/solutions/coffeescript'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.coffee')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_elixir {
  local files
  local builder='build/builders/solutions/elixir'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.exs')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_fsharp {
  local files
  local builder='build/builders/solutions/fsharp'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.fs')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_haxe {
  local files
  local builder='build/builders/solutions/haxe'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.hx')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_octave {
  local files
  local builder='build/builders/solutions/octave'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.m')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_perl {
  local files
  local builder='build/builders/solutions/perl'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.pl')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_pascal {
  local files
  local builder='build/builders/solutions/pascal'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.pas')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_awk {
  local files
  local builder='build/builders/solutions/awk'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.awk')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_cobol {
  local files
  local builder='build/builders/solutions/cobol'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.cbl')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_fortran {
  local files
  local builder='build/builders/solutions/fortran'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.f90')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_prolog {
  local files
  local builder='build/builders/solutions/prolog'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.pro')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_kotlin {
  local files
  local builder='build/builders/solutions/kotlin'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.kt')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_typescript {
  local files
  local builder='build/builders/solutions/typescript'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.ts')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_build_solutions_lisp {
  local files
  local builder='build/builders/solutions/lisp'
  local solution_path_var_name='solutionPath'
  export -f helper_build_solution

      helper_use_pristine_workdir \
  &&  files="$(find code/ -type f -name '*.lsp')" \
  &&  echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder} ${solution_path_var_name}"
}

function job_test_schemas {
  local lang_data_supported='code/lang-data-supported.yml'
  local lang_schema_supported='code/lang-schema-supported.yml'
  local lang_data_dropped='code/lang-data-dropped.yml'
  local lang_schema_dropped='code/lang-schema-dropped.yml'

      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  helper_test_pykwalify \
        "${lang_data_supported}" \
        "${lang_schema_supported}" \
  &&  helper_test_pykwalify \
        "${lang_data_dropped}" \
        "${lang_schema_dropped}" \
  &&  helper_test_schemas_site_data
}

function job_test_generic {
      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  helper_test_generic_dir_depth \
  &&  helper_test_generic_only_png_evidences \
  &&  helper_test_generic_misplaced_evidences \
  &&  helper_test_generic_allowed_mimes \
  &&  helper_test_generic_atfluid_account \
  &&  helper_test_generic_short_filenames \
  &&  helper_test_generic_raw_github_urls \
  &&  helper_test_generic_no_asc_extension \
  &&  helper_test_generic_no_tabs \
  &&  helper_test_generic_only_allowed_characters_in_paths \
  &&  helper_test_generic_80_columns \
  &&  helper_test_generic_valid_language_solution \
  &&  helper_test_generic_pre_commit
}

function job_test_others {
      helper_use_pristine_workdir \
  &&  helper_test_generic_others_duplicates \
  &&  helper_test_generic_others_code_has_unique_ext \
  &&  helper_test_generic_others_code_raw_only \
  &&  helper_test_generic_others_code_sort_by_ext \
  &&  helper_test_others_urls_status_code 'LINK.lst' \
  &&  helper_test_others_urls_status_code 'OTHERS.lst'
}

function job_test_commit_msg {
      helper_use_pristine_workdir \
  &&  env_prepare_python_packages \
  &&  helper_test_commit_msg_commitlint \
  &&  pushd build/modules/parse-commit-msg \
  &&  pushd tests \
  &&  echo '[INFO] Testing the parser' \
  &&  ./test-commit-msg-parser.sh \
  &&  popd || return 1 \
  &&  echo '[INFO] Using the parser to check your commit message' \
  &&  ./commit_msg_parser.py \
  &&  popd || return 1
}

function job_deploy_public {
      echo '[INFO] Triggering pipeline' \
  &&  curl \
        --request 'POST' \
        --form 'ref=master' \
        --form "token=${SITE_TRIGGER_TOKEN}" \
        'https://gitlab.com/api/v4/projects/10466586/trigger/pipeline'
}
